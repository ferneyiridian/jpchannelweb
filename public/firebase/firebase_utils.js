function firebaseImages(){
    var imgs = ['necesidad_imagen','promocion_imagen','promocion_imagenvertical','faq_imagen', 'producto_imagen',
        'producto_thumbnail','contenido_material','contenido_imagen','evento_logo','evento_portada','jptop_portada', 'liveshow_portada', 'video_imagen_image'
    ];
    for(var i = 0; i < imgs.length; i++){
        var id = imgs[i];
        var el = $('#'+id);
        if(el.length > 0){
            el.after('<div class="fire_cont" data-id="'+id+'"><input type="file" class="fire"></div>');
            if(el.val() != null)
                el.next().append('<img src="'+el.val()+'" style="max-width: 200px; max-height: 200px; border: 10px solid #ffffff; margin-top: 10px"/>');
            el.attr('type','hidden');
        }
    }
    const ref = firebase.storage().ref('app_images/');
    var padre;
    $( ".fire" ).change(function () {
        padre = $(this).parent();
        const file = $(this).get(0).files[0]
        const name = (+new Date()) + '-' + file.name;
        const metadata = {
            contentType: file.type
        };
        const task = ref.child(name).put(file, metadata);
        task.then((snapshot) => {
            var meta = snapshot.metadata;
            console.log(snapshot);
            meta.fullPath = meta.fullPath.replace(/\//g,'%2F')
            let url = 'https://firebasestorage.googleapis.com/v0/b/'+meta.bucket+'/o/'+meta.fullPath+'?alt=media';
            //url = url.replace(/\//g,'%2F');
            console.log(url);
            padre.find('img').remove();
            padre.append('<img style="max-width: 200px; max-height: 200px; border: 10px solid #ffffff; margin-top: 10px" src="'+url+'"/>');
            $('#'+padre.data('id')).val(url);
        }).catch((error) => {
            console.error(error);
        });
    });
}
