<?php

namespace App\Form;

use App\Entity\Producto;
use App\Entity\Subcategoria;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('productos', EntityType::class, [
            'class' => Producto::class,
            'query_builder' => function (EntityRepository $er) use ($options) {
                return $er->createQueryBuilder('p')
                    ->where('p.subcategoria = :subcategoria')
                    ->setParameter('subcategoria', $options['subcategoria'])
                    ->orderBy('p.orden', 'ASC');

            },
            'choice_label' => 'nombre',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Producto::class,
            'subcategoria' => Subcategoria::class,
        ]);
        $resolver->setAllowedTypes('subcategoria', 'relation');
    }
}
