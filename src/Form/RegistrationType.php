<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre', null, array('label'=>'Nombres','required'=>true));
        $builder->add('apellido', null, array('label'=>'Apellidos','required'=>true));
        $builder->add('email', RepeatedType::class, array(
            'type' => EmailType::class,
            'first_options' => array('label' => 'Correo electrónico'),
            'second_options' => array('label' => 'Confirmar correo electrónico'),
            'invalid_message' => 'Los correos electrónicos no coinciden',
        ));
        $builder->add('username', HiddenType::class);
        //$builder->add('celular');
        $builder->add('terminos', HiddenType::class);
        //$builder->add('mensajes', null, array('label'=>'Quiero recibir mensajes de texto y correo'));
        //$builder->add('whatsapp', null, array('label'=>'Quiero recibir mensajes de whatsapp (Solo para cortesias y mensajes importantes)'));
        $builder->add('plainPassword', RepeatedType::class, array(
            'type' => PasswordType::class,
            'options' => array(
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'autocomplete' => 'new-password',
                ),
            ),
            'first_options' => array('label' => 'Contraseña'),
            'second_options' => array('label' => 'Confirmar contraseña'),
            'invalid_message' => 'Las contraseñas no coinciden',
        ));
    }
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }
    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
