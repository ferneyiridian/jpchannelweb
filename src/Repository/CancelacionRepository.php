<?php

namespace App\Repository;

use App\Entity\Cancelacion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Cancelacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cancelacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cancelacion[]    findAll()
 * @method Cancelacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CancelacionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cancelacion::class);
    }

    // /**
    //  * @return Cancelacion[] Returns an array of Cancelacion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cancelacion
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
