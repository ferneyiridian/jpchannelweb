<?php

namespace App\Repository;

use App\Entity\POSAdmin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method POSAdmin|null find($id, $lockMode = null, $lockVersion = null)
 * @method POSAdmin|null findOneBy(array $criteria, array $orderBy = null)
 * @method POSAdmin[]    findAll()
 * @method POSAdmin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class POSAdminRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, POSAdmin::class);
    }

    // /**
    //  * @return POSAdmin[] Returns an array of POSAdmin objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?POSAdmin
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
