<?php


namespace App\EventSubscriber;

use App\DTO\UserSession;
use App\Service\QI;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class RegistrationInitializeListener  implements EventSubscriberInterface
{


    private $userManager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }


    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInit',
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationSuccess',
        );
    }

    public function onRegistrationInit(UserEvent $userEvent)
    {
        $user = $userEvent->getUser();

        $user->setUsername(uniqid());
    }
    public function onRegistrationSuccess(UserEvent $event)
    {
        $user = $event->getUser();

        $user->setUsername($user->getEmailCanonical());

        $this->userManager->updateUser($user);
    }
}