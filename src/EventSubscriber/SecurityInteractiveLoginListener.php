<?php


namespace App\EventSubscriber;

use App\DTO\UserSession;
use App\Service\QI;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SecurityInteractiveLoginListener
{
    private $userManager;
    private $qi;

    public function __construct(UserManagerInterface $userManager, QI $qi)
    {
        $this->userManager = $userManager;
        $this->qi = $qi;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $request = $event->getRequest();
        $session = $request->getSession();
        $session->has('id'); // Just to fix a bug on Remember Me
        $user = $event->getAuthenticationToken()->getUser();

        // Set the session ID on user and save it in database
        $user->setSessionId($session->getId());
        $this->qi->saveFire($user);
        $this->userManager->updateUser($user);
    }
}