<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 22/05/2019
 * Time: 5:11 PM
 */

namespace App\Service;


use App\Entity\Categoria;
use App\Entity\CategoriaBlog;
use App\Entity\Galeria;
use App\Entity\Image;
use App\Entity\Journey;
use App\Entity\PagoSuscripcion;
use App\Entity\Plan;
use App\Entity\Post;
use App\Entity\Producto;
use App\Entity\Seo;
use App\Entity\Subcategoria;
use App\Entity\Suscripcion;
use App\Entity\Tarjeta;
use App\Entity\Usuario;
use App\Entity\Video;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use function GuzzleHttp\Psr7\uri_for;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Symfony\Component\PropertyAccess\PropertyAccess;


define('MULTIPART_BOUNDARY', '----' . md5(time()));
define('EOL', "\r\n");

class QI
{
    protected $textosBig = null;
    protected $textos = null;
    protected $imagenes = null;
    protected $settings = null;
    private $session;
    protected $aws = null;


    public function __construct(
        EntityManagerInterface $em,
        RequestStack $request_stack,
        ContainerInterface $container,
        SessionInterface $session,
        AWSHelper $AWSHelper){
        $this->em = $em;
        $this->request_stack = $request_stack;
        $this->container = $container;
        $this->session = $session;
        //$this->locale = $request_stack->getCurrentRequest()->getLocale();

        $this->aws = $AWSHelper;
    }


    public function crearTarjetaUsuario($user, $tarjeta)
    {

        $planId = $tarjeta['plan_ref'];//$this->getSetting('plan_id_payu');
        $plan = $this->em->getRepository(Plan::class)->findOneBy(array('id_payu' => $planId));

        unset($tarjeta['plan_ref']);
        $u = $this->em->getRepository(Usuario::class)->find($user->getId());
        if ($u->getIdPayu()) {
            $cliente_id = $u->getIdPayu();
        } else {
            //crear cliente payu
            $cliente = $this->crearClientePayu(array('fullName' => $user->getNombre() . ' ' . $user->getApellido(), 'email' => $user->getEmail()));
            $cliente_id = $cliente->id;
            //actualizar user
            $u->setIdPayu($cliente->id);
            $user->setIdPayu($cliente->id);
            $this->em->persist($u);
        }

        //crear tarjeta a ese cliente
        $card = $this->crearTarjetaPayu($tarjeta, $cliente_id);
        if (property_exists($card, 'type')) {
            return $card;
        }
        if (!$card->token) {
            return $card;
        }
        //crear tarjeta en nuestro sistema
        $newT = new Tarjeta();
        $newT->setToken($card->token);
        $newT->setUsuario($u);
        $this->em->persist($newT);
        //crear suscripcion a ese cliente y a esa tarjeta
        $cuotas = 1;

        $suscripcion = array('clienteId' => $cliente_id, 'tokenTarjeta' => $card->token, 'installments' => $cuotas, 'planId' => $planId);
        $suscripcion = $this->crearSuscripcionPayu($suscripcion);

        //crear suscripcion en nuestro sistema
        $newS = new Suscripcion();

        $newS->setDuracion(1);
        $valida = new \DateTime();
        $valida = new \DateTime(date('Y-m-d', strtotime("+2 days", strtotime("+" . $newS->getDuracion() . " months", $valida->getTimestamp()))));
        //dd($valida);
        $newS->setValidaHasta($valida);

        $newS->setUsuario($u);
        $newS->setEmail($user->getEmail());
        $newS->setPayerId($cliente_id);
        $newS->setPlanId($planId);
        $newS->setStatus('creada_payu');
        $newS->setIdPayu($suscripcion->id);
        $newS->setActiva(false);
        $newS->setPlanSuscripcion($plan);
        $this->em->persist($newS);
        $this->em->flush();


        //crear pagos de esa suscripcion
        /*
        $resp = $this->getPagoSuscripcionPayu($suscripcion->id);

        $activa = false;
        foreach ($resp->recurringBillList as $p) {
            $pe = $this->em->getRepository(PagoSuscripcion::class)->findOneBy(array('id_payu' => $p->id));
            if (!$pe) {
                $pago = new PagoSuscripcion();
                $pago->setIdPayu($p->id);
                $pago->setOrderId($p->subscriptionId);
                $pago->setMonto($p->amount);
                $pago->setMoneda($p->currency);
                $fecha = new \DateTime();
                $fecha->setTimestamp($p->dateCharge);
                $pago->setFecha($fecha);
                $pago->setState($p->state);
                $pago->setSuscripcion($newS);

                $this->em->persist($pago);
                $this->em->flush();
                $this->saveFire($pago);
                if ($p->state == 'completed' || $p->state == 'PAID') {
                    $activa = true;
                }
            }
        }
        if ($activa) {
            $newS->setActiva($activa);
            $this->em->persist($newS);
            $this->em->flush();
        }
        */
        $newS->setEmail("");
        $newS->setDuracion(0);
        $newS->setIdPayu("");
        $newS->setPayerId("");
        $newS->setIdPaypal("");
        $newS->setPlanId("");
        $newS->setUsuario(null);
        $newS->setValidaHasta(null);
        $this->saveFire($newS);
        //retornar
        return $newS->getId();
    }


    /*
     * params mixed $cliente (fullname,email)
     * */
    public function crearClientePayu($cliente)
    {
        $postdata = json_encode($cliente);
        $url = $this->getSetting('urlpayu') . 'customers/';
        return $this->sendDataPayu($postdata, $url);

    }

    /*
     * params mixed $tarjeta "name": "Sample User Name",
       "document": "1020304050",
       "number": "4242424242424242",
       "expMonth": "01",
       "expYear": "2018",
       "type": "VISA",
       "address": {
          "line1": "Address Name",
          "line2": "17 25",
          "line3": "Of 301",
          "postalCode": "00000",
          "city": "City Name",
          "state": "State Name",
          "country": "CO",
          "phone": "300300300"
       }
     * */
    public function crearTarjetaPayu($tarjeta, $clienteId)
    {
        $postdata = json_encode($tarjeta);
        $url = $this->getSetting('urlpayu') . 'customers/' . $clienteId . '/creditCards';
        return $this->sendDataPayu($postdata, $url);

    }

    /*
     * params mixed $suscripcion ($clienteId,$tokenTarjeta,$installments,$plan)
     * */
    public function crearSuscripcionPayu($suscripcion)
    {

        $postdata = array(
            "quantity" => "1",
            "installments" => $suscripcion["installments"],
            "trialDays" => "0",
            "immediatePayment" => true,
            "customer" => array(
                "id" => $suscripcion["clienteId"],
                "creditCards" => array(
                    array(
                        "token" => $suscripcion["tokenTarjeta"]
                    )
                )
            ),
            'notifyUrl' => $this->getSetting('urlRespuestaPayu'),
            "plan" => array(
                "planCode" => $suscripcion["planId"]
            ));
        $postdata = json_encode($postdata);
        $url = $this->getSetting('urlpayu') . 'subscriptions/';
        return $this->sendDataPayu($postdata, $url);

    }

    public function getCustomerAndSubscriptionPayPal($subscriptionId){

        $basicToken = base64_encode($this->getSetting('paypal_clientid') . ":" . $this->getSetting('paypla_secret'));
        $url = $this->getSetting('url_paypal') . 'billing/subscriptions/' . $subscriptionId;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Basic {$basicToken}",
                "Accept: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        curl_close($curl);

        if(!$response) return null;

        return $response;
    }

    public function getCustomerAndSubscriptionPayU($subscriptionId){

        $url = "{$this->getSetting('urlpayu') }subscriptions/{$subscriptionId}";
        $basicToken = base64_encode($this->getSetting('payu_apiLogin') . ":" . $this->getSetting('payu_apiKey'));
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic {$basicToken}",
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        curl_close($curl);

        $data = array("subscription"=>$response);

        if(!$response) return null;

        $customerId = $response->customer->id;

        $url = "{$this->getSetting('urlpayu') }customers/{$customerId}";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic {$basicToken}",
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        curl_close($curl);
        $data["customer"] = $response;

        return $data;

    }


    private function getRecurringBillsPayPalFromURL($url) {

        $basicToken = base64_encode($this->getSetting('paypal_clientid') . ":" . $this->getSetting('paypla_secret'));
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Basic {$basicToken}",
                'Accept: application/json',
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);

        curl_close($curl);

        return $response;
    }

    public function getRecurringBillsPayPal($dateBegin, $dateFinal){
        $url = $this->getSetting('url_paypal') . 'reporting/transactions?start_date=' . $dateBegin . 'T00:00:00-0700&end_date=' . $dateFinal . 'T23:59:59-0700&transaction_status=S';
        $response = $this->getRecurringBillsPayPalFromURL($url);
        if(!$response || !isset($response->transaction_details)) return null;
        $recurringBills = $response->transaction_details;
        $hasNext = isset($response->links);
        while($hasNext){
            $nextLink = null;
            foreach ($response->links as $link) {
                if($link->rel == "next") {
                    $nextLink = $link->href;
                }
            }
            if($nextLink) {
                $response = $this->getRecurringBillsPayPalFromURL($nextLink);
                if(!$response) break;
                $recurringBills = array_merge($recurringBills, $response->transaction_details);
                $hasNext = isset($response->links);
            } else {
                break;
            }
        }
        return $recurringBills;
    }

    public function getRecurringBillsPayU($dateBegin, $dateFinal) {
        $url = "{$this->getSetting('urlpayu') }recurringBill?dateBegin={$dateBegin}&dateFinal={$dateFinal}";
        $basicToken = base64_encode($this->getSetting('payu_apiLogin') . ":" . $this->getSetting('payu_apiKey'));

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic {$basicToken}",
                "Content-Type: application/json",
                'Accept: application/json',
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        $information = curl_getinfo($curl);

        curl_close($curl);
        return $response;
    }

    public function cancelarSuscripcion(Suscripcion $suscripcion)
    {
        if ($suscripcion->getIdPayu()) {

            $url = $this->getSetting('urlpayu') . 'subscriptions/' . $suscripcion->getIdPayu();
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            //curl_setopt($ch, CURLOPT_HEADER, 1);
            $additional_headers = array(
                'Accept: application/json',
                'Content-Type: application/json; charset=utf-8',
                "Authorization: Basic " . base64_encode($this->getSetting('payu_apiLogin') . ":" . $this->getSetting('payu_apiKey')) . "\""
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);


            $data = curl_exec($ch);

            $data = json_decode($data);
            $information = curl_getinfo($ch);
            if(!$data) {
                $data = $information;
            }
            curl_close($ch);
            return $data;
        } else {

            $url = $this->getSetting('url_paypal') . 'billing/subscriptions/' . $suscripcion->getIdPaypal() . '/cancel';
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            //curl_setopt($ch, CURLOPT_HEADER, 1);
            $additional_headers = array(
                'Accept: application/json',
                'Content-Type: application/json; charset=utf-8',
                "Authorization: Basic  " . base64_encode($this->getSetting('paypal_clientid') . ":" . $this->getSetting('paypla_secret')) . "\""
            );

            curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, '{"reason": "Not satisfied with the service"}');


            $data = curl_exec($ch);

            $data = json_decode($data);
            $information = curl_getinfo($ch);
            if(!$data) {
                $data = $information;
            }
            curl_close($ch);
            return $data;
        }
    }

    public function checkPagosPSE(Suscripcion $suscripcion)
    {
        if ($suscripcion->getIdPayu()) {

            $url = 'https://api.payulatam.com/reports-api/4.0/service.cgi';
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            //curl_setopt($ch, CURLOPT_HEADER, 1);
            $additional_headers = array(
                'Accept: application/json',
                'Content-Type: application/json; charset=utf-8'
            );
            $postdata = array("test" => false,
                "language" => "en",
                "command" => "ORDER_DETAIL_BY_REFERENCE_CODE",
                "merchant" => array(
                    "apiLogin" => $this->getSetting('payu_apiLogin'),
                    "apiKey" => $this->getSetting('payu_apiKey')
                ),
                "details" => array(
                    "referenceCode" => $suscripcion->getIdPayu()
                ));
            $postdata = json_encode($postdata);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

            $data = curl_exec($ch);

            $data = json_decode($data);
            $information = curl_getinfo($ch);
            curl_close($ch);
            return $data;
        }
        $pay = array('result' => array('payload' => 1));

        return json_encode($pay);
    }

    public function makeRefundPayu(PagoSuscripcion $pago, Suscripcion $suscripcion)
    {

        $pagos = $this->getPagoSuscripcionPayu($suscripcion->getIdPayu());
        $orderid = '';
        $transactionid = '';
        foreach ($pagos->recurringBillList as $p) {

            if (strpos($pago->getOrderId(), $p->id) !== FALSE) {
                $orderid = $p->orderId;
                $transactionid = $pago->getIdPayu();
            }
        }

        if ($orderid != '') {
            $postdata = array(
                "language" => "es",
                "command" => "SUBMIT_TRANSACTION",
                "merchant" => array(
                    "apiKey" => $this->getSetting('payu_apiKey'),
                    "apiLogin" => $this->getSetting('payu_apiLogin')
                ),
                "transaction" => array(
                    "order" => array(
                        "id" => $orderid
                    ),
                    "type" => "REFUND",
                    "reason" => $this->getSetting('razon_cancelacion_duplicado_payu'),
                    "parentTransactionId" => $transactionid
                )
            ,
                "test" => false);
            $postdata = json_encode($postdata);
            $url = $this->getSetting('urlpayu_refund');
            $this->cancelarSuscripcion($suscripcion);
            return $this->sendDataPayu($postdata, $url);
        } else {
            return false;
        }


    }

    /*
    * params $suscripcionId
    * */
    public function getPagoSuscripcionPayu($suscripcionId)
    {

        $url = $this->getSetting('urlpayu') . 'recurringBill?subscriptionId=' . $suscripcionId;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            "Authorization: Basic " . base64_encode($this->getSetting('payu_apiLogin') . ":" . $this->getSetting('payu_apiKey')) . "\""
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);


        $data = curl_exec($ch);
        $data = json_decode($data);
        $information = curl_getinfo($ch);

        curl_close($ch);
        return $data;


        //return $this->sendDataPayu($postdata,$url);

    }

    public function getPagosUsuarioPayu($clienteId)
    {

        $url = $this->getSetting('urlpayu') . 'recurringBill?customerId=' . $clienteId;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            "Authorization: Basic " . base64_encode($this->getSetting('payu_apiLogin') . ":" . $this->getSetting('payu_apiKey')) . "\""
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);


        $data = curl_exec($ch);
        $data = json_decode($data);
        $information = curl_getinfo($ch);

        curl_close($ch);
        return $data;


    }

    private function sendDataPayu($postdata, $url)
    {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            "Authorization: Basic " . base64_encode($this->getSetting('payu_apiLogin') . ":" . $this->getSetting('payu_apiKey')) . "\""
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);


        $data = curl_exec($ch);
        $data = json_decode($data);
        $information = curl_getinfo($ch);

        curl_close($ch);
        return $data;
    }


    public function crearSuscripcionPaypal(Plan $plan, Usuario $user, $urlretorno)
    {

        $urlretorno = str_replace('http', 'https', $urlretorno);
        $fecha = new \DateTime();
        $fecha = new \DateTime(date('Y-m-d', strtotime("+1 days", $fecha->getTimestamp())));
        $timestamp = $fecha->getTimestamp();
        $url = $this->getSetting('url_paypal') . 'billing/subscriptions';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type: application/json; charset=utf-8',
            'Prefer: return=representation',
            'PayPal-Request-Id: SUBSCRIPTIONJP-' . $timestamp . '-001',
            "Authorization: Basic  " . base64_encode($this->getSetting('paypal_clientid') . ":" . $this->getSetting('paypla_secret')) . "\""
        );
        //dd(date(\DateTime::ISO8601));
        $postdata = array(
            "plan_id" => $plan->getIdPaypal(),
            "start_time" => $fecha->format('c'),
            "subscriber" => array(
                "name" => array(
                    "given_name" => $user->getNombre(),
                    "surname" => $user->getApellido()
                ),
                "email_address" => $user->getEmail()
            ),
            "application_context" => array(
                "brand_name" => "Jp Channel",
                "locale" => "en-US",
                "shipping_preference" => "SET_PROVIDED_ADDRESS",
                "user_action" => "SUBSCRIBE_NOW",
                "payment_method" => array(
                    "payer_selected" => "PAYPAL",
                    "payee_preferred" => "IMMEDIATE_PAYMENT_REQUIRED"
                ),
                "return_url" => 'https://thejpchannel.com/es/esperando_activacion',
                "cancel_url" => 'https://thejpchannel.com/es/esperando_activacion'
            )
        );
        $postdata = json_encode($postdata);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);


        $data = curl_exec($ch);
        $data = json_decode($data);


        $information = curl_getinfo($ch);

        curl_close($ch);
        return $data;
    }

    private function getResults($entidad)
    {
        $qb = $this->em->createQueryBuilder()
            ->select('s')
            ->from($entidad, 's', 's.llave')
            ->where('s.llave is not null')
            ->getQuery()
            ->getArrayResult();
        return $qb;
    }

    /**
     * Retorna la variable con textos populada la primera vez que se llama esta función, los textos se traen de la base de datos
     * @return multitype: Arreglo con los textos intenacionalizados
     */
    private function getTextos()
    {
        if ($this->textos == null) {
            $this->textos = $this->getResults('App:Texto');
        }
        return $this->textos;
    }

    /**
     * Obtener uno de los texto fijos internacionalizados según el UserCulture actual y el nombre del texto solicitado. Los textos se traen de la base de datos
     * @param string $key Nombre (identificador) del texto solicitado
     * @return string El texto solicitado
     */
    public function getTexto($key)
    {
        $arrTextos = $this->getTextos();
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        if (isset($arrTextos[$key]) && $arrTextos[$key] != '')
            return $arrTextos[$key]['valor_' . $locale];
        else
            return '';
    }


    private function getTextosBig()
    {
        if ($this->textosBig == null) {
            $this->textosBig = $this->getResults('App:TextoBig');
        }
        return $this->textosBig;
    }

    public function getTextoBig($key)
    {
        $arrTextos = $this->getTextosBig();
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        if (isset($arrTextos[$key]) && $arrTextos[$key] != '')
            return $arrTextos[$key]['valor_' . $locale];
        else
            return '';
    }

    /**
     * Retorna la variable con settings populada la primera vez que se llama esta función, los textos se traen de la base de datos
     * @return multitype: Arreglo con los settings
     */
    private function getSettings()
    {
        if ($this->settings == null) {
            $this->settings = $this->getResults('App:Setting');
        }
        return $this->settings;
    }


    /**
     * Obtener uno de los texto fijos internacionalizados según el local actual y el nombre del texto solicitado. Los textos se traen de la base de datos
     * @param string $key Nombre (identificador) del texto solicitado
     * @return string El setting solicitado
     */
    public function getSetting($key)
    {
        $settings = $this->getSettings();
        if (isset($settings[$key]) && $settings[$key]['valor'] != '')
            return $settings[$key]['valor'];
        else
            return '';
    }

    public function getGaleria($key)
    {
        return $this->em->createQueryBuilder()
            ->from('App:Image', 'i')
            ->select('i')
            ->leftJoin('i.galeria', 'g')
            ->where('g.llave  = :llave')
            ->andWhere('i.visible = 1')
            ->setParameter('llave', $key)
            ->orderBy('i.orden', 'asc')
            ->getQuery()
            ->getResult();

    }


    public function getCategoriaBlog($id = null)
    {
        $filtros = [];
        if ($id) {
            $filtros["id"] = $id;
        }
        return $this->em->getRepository(CategoriaBlog::class)->findBy($filtros, array("orden" => "desc"));
    }


    public function getVideosCategoria($categoria)
    {
        $filtros = [];
        $filtros["visible"] = true;
        $filtros["categoria"] = $categoria;


        return $this->em->getRepository(Video::class)->findBy($filtros, array("orden" => "desc"));

    }

    public function getVideo($id)
    {
        $filtros = [];
        $filtros["visible"] = true;
        $filtros["id"] = $id;


        return $this->em->getRepository(Video::class)->findBy($filtros, array("orden" => "desc"));


    }

    public function getVideos()
    {
        $filtros["visible"] = true;
        return $this->em->getRepository(Video::class)->findBy($filtros, array("orden" => "desc"));

    }



    public function getPost($categoria = null)
    {
        $filtros = [];
        if ($categoria) {
            $filtros["categoria"] = $categoria;
        }
        return $this->em->getRepository(Post::class)->findBy($filtros, array("fecha" => "desc"));
    }

    public function getSeo($url, $homepage)
    {
        $url_abs = $url;
        $url = str_replace($homepage, '', $url);
        $path = $this->container->getParameter('app.path.product_images');
        $qb = $this->em->createQueryBuilder();
        $qb2 = $this->em->createQueryBuilder();
        //exit(dump($url));
        if ($url == "")
            $url = "/";
        $seo = $qb
            ->select('p.id', 'p.titulo as titulo', "concat('" . $path . "/',p.image) as imagen", 'p.descripcion as descripcion')
            ->from(Seo::class, 'p')
            ->where('p.url = :url')
            ->setParameter('url', $url)
            ->orderBy('p.id', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if ($seo == null) {
            $seo = $qb2
                ->select('s.id', 's.titulo as titulo', "concat('" . $path . "/',s.image) as imagen", 's.descripcion as descripcion')
                ->from(Seo::class, 's')
                ->orderBy('s.id', 'desc')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        }
        return $seo;
    }

    private function getImagenes()
    {
        if ($this->imagenes == null) {
            $this->imagenes = $this->getResults('App:Image');
        }
        return $this->imagenes;
    }


    public function getImagen($key)
    {
        $imagenes = $this->getImagenes();
        if (isset($imagenes[$key])) {
            return $imagenes[$key];
        } else
            return [];
    }


    /**
     * @param $asunto
     * @param $to
     * @param $html
     * @return bool|string
     */
    public function sendMailAWS($asunto, $to, $html)
    {
        return $this->aws->sendMail("info@thejpchannel.com", $asunto, $to, $html);
    }

    /**
     * @param $asunto
     * @param $to
     * @param $html
     * @return bool|string
     */
    public function sendMailIB($asunto, $to, $html)
    {
        $postdata = array(
            'from' => "noreply@mail.thejpchannel.com",
            'to' => $to,
            'replyTo' => "info@thejpchannel.com",
            'subject' => $asunto,
            'html' => $html,
            'intermediateReport' => 'true'
        );
        return $this->send_mail_infoBip($postdata);
    }

    function send_mail_infoBip($postdata)
    {
        $url = 'https://api.infobip.com/email/1/send';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type:  multipart/form-data ; boundary=' . MULTIPART_BOUNDARY
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_USERPWD, 'FERNANDOiridian123' . ":" . 'zuca2020');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getBody($postdata));
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $data = curl_exec($ch);
        $information = curl_getinfo($ch);
        //die(dump($information));
        curl_close($ch);
        return $data;
    }

    public function sendMailPHP($asunto, $to, $html)
    {
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->SMTPDebug = 2;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtpout.secureserver.net';                   // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'info@thejpchannel.com';                 // SMTP username
            $mail->Password = 'Maruchis53&';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('info@thejpchannel.com', 'Info Jp Channel');
            $mail->addAddress($to);     // Add a recipient
            //$mail->addAddress('contact@example.com');               // Name is optional
            //$mail->addReplyTo('info@example.com', 'Information');
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $asunto;
            $mail->Body = $html;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $mail->send();
            return 'Message has been sent';
        } catch (Exception $e) {
            return 'Message could not be sent.';
            return 'Mailer Error: ' . $mail->ErrorInfo;
        }
    }

    function getBodyOld($fields)
    {
        $content = '';
        foreach ($fields as $FORM_FIELD => $value) {
            $content .= '--' . MULTIPART_BOUNDARY . EOL;
            $content .= 'Content-Disposition: form-data; name="' . $FORM_FIELD . '"' . EOL;
            $content .= EOL . $value . EOL;
        }
        return $content . '--' . MULTIPART_BOUNDARY . '--'; // Email body should end with "--"
    }

    function getBody($fields)
    {
        $content = '';
        foreach ($fields as $FORM_FIELD => $value) {
            if ($FORM_FIELD == 'attachment') {
                $content .= '--' . MULTIPART_BOUNDARY . EOL;
                $content .= 'Content-Disposition: form-data; name="attachment"; filename="' . basename($value) . '"' . EOL;
                $content .= 'Content-Type: application/x-object' . EOL;
                $content .= EOL . file_get_contents($value) . EOL;
            } else {
                $content .= '--' . MULTIPART_BOUNDARY . EOL;
                $content .= 'Content-Disposition: form-data; name="' . $FORM_FIELD . '"' . EOL;
                $content .= EOL . $value . EOL;
            }
        }
        return $content . '--' . MULTIPART_BOUNDARY . '--'; // Email body should end with "--"
    }

    /*
     * Method to get the headers for a basic authentication with username and passowrd
    */
    function getHeader($username, $password)
    {
        // basic Authentication
        $auth = base64_encode("$username:$password");

        // Define the header
        return array("Authorization:Basic $auth", 'Content-Type: multipart/form-data ; boundary=' . MULTIPART_BOUNDARY);
    }

    /**
     * @param $subject
     * @param $from
     * @param $to
     * @param $custom
     * @param $template
     */
    public function sendMail($asunto, $to, $html)
    {
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $senderUser = $this->getSetting('sender_mail_user');
            $senderPass = $this->getSetting('sender_mail_pass');
            $senderHost = $this->getSetting('sender_mail_host');
            $senderSubject = $this->getSetting('sender_mail_name');
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $senderHost;                   // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $senderUser;                 // SMTP username
            $mail->Password = $senderPass;                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom($senderUser, $senderSubject);
            $mail->addAddress($to);     // Add a recipient
            //$mail->addAddress('contact@example.com');               // Name is optional
            //$mail->addReplyTo('info@example.com', 'Information');
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $asunto;
            $mail->Body = $html;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            return 'Message has been sent';
        } catch (\Exception $e) {
            return 'Message could not be sent.';
            return 'Mailer Error: ' . $mail->ErrorInfo;
        }
    }





    /**
     * @param $entity
     */
    public function saveFire($entity)
    {
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->em;
        $metadata = $em->getClassMetadata(get_class($entity));
        $campos = $metadata->fieldNames;
        $accessor = PropertyAccess::createPropertyAccessor();
        $arr = array();
        foreach ($campos as $campo) {
            $valor = $accessor->getValue($entity, $campo);
            if ($valor instanceof \DateTime) {
                $arr[$campo] = $valor->format("Y-m-d H:i:s");
            } else {
                $arr[$campo] = $valor;
            }
        }
        $campos = $metadata->associationMappings;
        foreach ($campos as $campo) {
            $campo = $campo['fieldName'];
            $valor = $accessor->getValue($entity, $campo);
            $arr_in = array();
            if ($valor instanceof PersistentCollection || $valor instanceof ArrayCollection) {
                foreach ($valor as $inner) {
                    array_push($arr_in, $inner->getId());
                }
                $arr[$campo] = $arr_in;
            } else {
                if (is_string($valor) || $valor == null) {
                } else
                    $arr[$campo] = $valor->getId();
            }
        }
        $json = json_encode($arr);
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/../../firebase_credentials.json');
        $firebase = (new Factory())
            ->withServiceAccount($serviceAccount)
            ->create();
        $database = $firebase->getDatabase();
        $tabla = explode(':', get_class($entity));
        $tabla = end($tabla);
        $tabla = explode("\\", get_class($entity));
        $tabla = end($tabla);
        $ref = $database->getReference("/$tabla/" . $entity->getId())->set($arr);
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
