<?php


namespace App\Service;


use Aws\Credentials\Credentials;
use Aws\Exception\AwsException;
use Aws\Pinpoint\PinpointClient;

class AWSHelper
{

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $projectId;

    private $pinpointClient;

    /**
     * AWSHelper constructor.
     * @param string $key
     * @param string $secret
     * @param string $region
     * @param string $projectId
     */
    public function __construct(array $awsConfig/*string $awsKey, string $awsSecret, string $awsRegion, string $awsProjectId*/)
    {
        $awsKey = $awsConfig["key"];
        $awsSecret = $awsConfig["secret"];
        $awsRegion = $awsConfig["region"];
        $awsProjectId = $awsConfig["projectId"];
        $this->key = $awsKey;
        $this->secret = $awsSecret;
        $this->region = $awsRegion;
        $this->projectId = $awsProjectId;

        $credentials = new Credentials($awsKey, $awsSecret);
        $this->pinpointClient = new PinpointClient(array(
                'credentials' => $credentials,
                'region' => $awsRegion,
                'version' => 'latest')
        );

    }

    public function sendMail($sender, $asunto, $to, $html) {

        $TOADDRESS = $to;

        try {

            $result = $this->pinpointClient->sendMessages([
                'ApplicationId' => $this->projectId,
                'MessageRequest' => [ // REQUIRED
                    'Addresses' => [
                        $TOADDRESS => [
                            'ChannelType' => 'EMAIL',
                        ],
                    ],
                    'MessageConfiguration' => [ // REQUIRED
                        'EmailMessage' => [
                            'FromAddress' => $sender,
                            'SimpleEmail' => [
                                'HtmlPart' => [
                                    'Charset' => 'utf-8',
                                    'Data' => $html,
                                ],
                                'Subject' => [
                                    'Charset' => 'utf-8',
                                    'Data' => $asunto,
                                ]/*,
                                'TextPart' => [
                                    'Charset' => 'utf-8',
                                    'Data' => 'my sample text',
                                ]*/
                            ]
                        ],
                    ],
                ]
            ]);

            return $result;

        } catch (AwsException $e){

            // output error message if fails
            error_log($e->getMessage());
            return null;
        }
    }


}