<?php

namespace App\API;

use Algolia\SearchBundle\SearchService;
use App\Entity\PagoSuscripcion;
use App\Entity\Plan;
use App\Entity\Suscripcion;
use App\Entity\Usuario;
use App\Service\QI;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use phpDocumentor\Reflection\Types\Array_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class APIController extends AbstractController
{
    private $qi;
    private $userManager;
    private $retryTtl;
    private $tokenGenerator;
    protected $searchService;
    private $serializer;

    public function __construct(QI $qi, UserManagerInterface $userManager, TokenGeneratorInterface $tokenGenerator, SearchService $searchService)
    {
        $this->qi = $qi;
        $this->userManager = $userManager;
        $this->retryTtl = 7200;
        $this->tokenGenerator = $tokenGenerator;
        $this->searchService = $searchService;

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }


    /**
     * Returns a JSON response
     *
     * @param array $data
     * @param array $headers
     *
     * @return JsonResponse
     */
    public function response($data, $headers = [], $status = 200)
    {
        return $this->JsonResponse($data, $status, $headers);
    }

    private function serializeVideo($video)
    {
        $videoSer = array(
            "titulo" => $video->getTitulo(),
            "url" => $video->getAwsUrl() ? $this->qi->getSetting('bucket_s3') . $video->getAwsUrl() : null,
            "imagen" => $video->getImagen().''
        );

        return $videoSer;
    }

    private function JsonResponse($data, $status=200) {
        $response = new JsonResponse($data, $status);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET,POST,OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', '*');
        //$response->headers->set('Access-Control-Max-Age', '1728000');
        //$response->headers->set('Access-Control-Allow-Credentials', 'false');
        return $response;
    }

    /**
     * @Route("/videos", name="get_videos_api", methods={"GET"})
     */
    public function apiVideosGet(Request $request)
    {
        if($request->getMethod() === "OPTIONS") {
            return $this->JsonResponse([]);
        }

        $destacado = $this->getDoctrine()->getManager()->getRepository('App:Video')->findOneBy(array('destacado' => true), array('orden' => 'asc'));
        $categorias = $this->getDoctrine()->getManager()->getRepository('App:CategoriaVideo')->findBy(array('visible' => true), array('orden' => 'asc'));
        $categoriasSerializable = [];
        foreach ($categorias as $categoria) {
            $catSer = array(
                "nombre" => $categoria->getNombre(),
                "videos" => []
            );
            foreach ($categoria->getVideos() as $video) {

                array_push($catSer["videos"], $this->serializeVideo($video));
            }
            array_push($categoriasSerializable, $catSer);
        }
        //$jsonContent = $this->serializer->serialize(array("destacado" => $this->serializeVideo($destacado), "categorias"=>$categoriasSerializable), 'json');
        $content = array("destacado" => $this->serializeVideo($destacado), "categorias" => $categoriasSerializable);
        $response =  $this->JsonResponse($content);
        return $response;
    }

    /**
     * @Route("/login",methods={"POST"})
     * @param JWTTokenManagerInterface $JWTManager
     * @return JsonResponse
     */
    public function getTokenUser(Request $request, JWTTokenManagerInterface $JWTManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        if($request->getMethod() === "OPTIONS") {
            return $this->JsonResponse([]);
        }
        if (0 !== strpos($request->headers->get('Content-Type'), 'application/json')) {
            return $this->JsonResponse(array(), 400);
        }
        $em = $this->getDoctrine()->getManager();
        $body = json_decode($request->getContent(), true);
        $email = $body["email"];
        $password = $body["password"];
        $user = $em->getRepository(Usuario::class)->findOneBy(array("email" => $email));
        if (!$user) {
            return $this->JsonResponse(['error' => "Usuario no encontrado.", "email" => $body["email"]], 400);
        }
        if ($passwordEncoder->isPasswordValid($user, $password)) {
            return $this->JsonResponse([/*'token' => $JWTManager->create($user),*/ "email" => $body["email"]]);
        } else {
            return $this->JsonResponse(['error' => "Credenciales inválidas.", "email" => $body["email"]], 400);
        }
    }


//     /**
//     * @Route("/create-subscription",methods={"GET"})
//     * @param JWTTokenManagerInterface $JWTManager
//     * @return JsonResponse
//      *
//     */
//    public function createSubscription(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
//       /* if (0 !== strpos($request->headers->get('Content-Type'), 'application/json')) {
//            return $this->JsonResponse(array(), 400);
//        }*/
//        $em = $this->getDoctrine()->getManager();
//        $emails = [
//            "catatorregrosa@hotmail.com",
//            "gilaryvalencia85@hotmail.com",
//            "ts.monicagutierrez@gmail.com",
//            "andrestejedor@gmail.com",
//            "dlvargasr@correo.udistrital.edu.co",
//            "daqj90@gmail.com",
//            "claudia_roa@icloud.com",
//            "laurmo61@hotmail.com",
//            "g.garcia@accesspark.co",
//            "cbenitezc@unal.edu.co",
//            "carolinit2@gmail.com",
//            "higuera631@hotmail.com",
//            "dajimenezj@gmail.com",
//            "fpintor09@gmail.com",
//            "alfeb03@gmail.com"
//        ];
//        $suscripciones = [];
//        foreach($emails as $email) {
//            $sus = $this->crearUsuarioYSuscripcionVIP($email, $passwordEncoder);
//            array_push($suscripciones, $sus);
//        }
//        dump($suscripciones);
//        exit();
//
//    }
//
//    public function crearUsuarioYSuscripcionVIP($email, UserPasswordEncoderInterface $passwordEncoder)
//    {
//        $fechaPago = new \DateTime();
//        $fechaVenc = new \DateTime("@" . ($fechaPago->getTimestamp() + 60 * 60 * 24 * 31));
//
//        $em = $this->getDoctrine()->getManager();
//        $user = $this->getDoctrine()->getRepository(Usuario::class)->findOneBy(array("email" => $email));
//        if (!$user) {
//            exit();
//        }
//        $user->setEnabled(true);
//
//        $subscription = new Suscripcion();
//        $subscription->setUsuario($user);
//        $subscription->setActiva(true);
//        $subscription->setIdPayu("VIP_MANUAL");
//        $subscription->setPlanSuscripcion($this->getDoctrine()->getRepository(Plan::class)->find(1));
//        $subscription->setEmail($email);
//        $subscription->setDuracion(1);
//        $subscription->setStatus("VIP_MANUAL");
//        $subscription->setValidaHasta($fechaVenc);
//        $subscription->setIdPayu("VIP_MANUAL");
//        $subscription->setPayerId("VIP_MANUAL");
//
//        $em->persist($subscription);
//        $em->flush();
//
//        $pagoSubs = new PagoSuscripcion();
//        $pagoSubs->setSuscripcion($subscription);
//        $pagoSubs->setState("VIP_MANUAL");
//        $pagoSubs->setIdPayu("VIP_MANUAL");
//        $pagoSubs->setFecha($fechaPago);
//
//        $subscription->addPago($pagoSubs);
//        $user->addSuscripcione($subscription);
//
//        $em->persist($pagoSubs);
//        $em->flush();
//        return $subscription;
//    }

}