<?php
namespace App\Controller;

use App\Entity\Usuario;
use Doctrine\Bundle\DoctrineBundle\Registry;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider;
use AppBundle\Entity\User;

class MyFOSUBProvider extends FOSUBUserProvider
{

    /**
     * @param UserManagerInterface $userManager
     * @param array $properties
     */
    public function __construct(UserManagerInterface $userManager, array $properties)
    {
        parent::__construct($userManager, $properties);

    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $property = $this->getProperty($response);

        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));
        $email = $response->getEmail();
        // check if we already have this user
        $existing = $this->userManager->findUserBy(array('email' => $email));
        if ($existing instanceof Usuario) {
            // in case of Facebook login, update the facebook_id
            if ($property == "facebookId") {
                $existing->setFacebookId($username);
            }
            // in case of Google login, update the google_id
            if ($property == "googleId") {
                $existing->setGoogleId($username);
            }
            $this->userManager->updateUser($existing);

            return $existing;
        }

        // if we don't know the user, create it
        if (null === $user || null === $username) {
            /** @var Usuario $user */
            $user = $this->userManager->createUser();
            $nick = $response->getEmail();

            $user->setLastLogin(new \DateTime());
            $user->setEnabled(true);

            $user->setUsername($nick);
            $user->setUsernameCanonical($nick);
            $user->setPassword(sha1(uniqid()));
            $user->addRole('ROLE_USER');

            if ($property == "facebookId") {
                $user->setFacebookId($username);
            }
            if ($property == "googleId") {
                $user->setGoogleId($username);
            }
            if ($property == "instagramId") {
                $user->setInstagramId($username);
            }
        }

        $user->setEmail($response->getEmail());
        try {
            $user->setNombre($response->getFirstName());
            $user->setApellido($response->getLastName());

            $this->userManager->updateUser($user);
        }catch (\Exception $e){

        }

        return $user;
    }
}