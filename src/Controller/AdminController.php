<?php
/**
 * Created by PhpStorm.
 * User: Mauricio
 * Date: 25/04/2019
 * Time: 7:25 PM
 */

namespace App\Controller;

use AlterPHP\EasyAdminExtensionBundle\Controller\EasyAdminController;
use AlterPHP\EasyAdminExtensionBundle\Security\AdminAuthorizationChecker;
use App\Entity\Ciudad;
use App\Entity\Color;
use App\Entity\Especialidad;
use App\Entity\Masivo;
use App\Entity\Medico;
use App\Entity\PagoSuscripcion;
use App\Entity\Plan;
use App\Entity\Producto;
use App\Entity\Suscripcion;
use App\Entity\Usuario;
use App\Service\QI;
use Doctrine\ORM\Query\ResultSetMapping;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Exception;
use FOS\UserBundle\Model\UserManagerInterface;

#use function Google\Cloud\Samples\Datastore\properties;
#use Kreait\Firebase;
use mysqli;
use phpDocumentor\Reflection\Types\ClassString;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdminController extends EasyAdminController
{
    private $validator;
    private $firebase;
    private $userManager;
    private $qi;


    /**
     * AdminController constructor.
     */
    public function __construct(QI $qi, UserManagerInterface $userManager)
    {
        $this->qi = $qi;
        $this->userManager = $userManager;

        //$this->firebase = $firebase;

    }

    /**
     * @Route("/sincronizar_suscripciones_pagas", name="sincronizar_suscripciones_pagas")
     */
    public function sincronizarSuscripcionesPagas(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $limbo = [];
        $suscripcion = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->from('App:Suscripcion', 's')
            ->select('s')
            ->leftJoin('s.pagos', 'p')
            ->addSelect('p')
            ->where('s.activa = 0 ')
            //->setParameter('padre',null)
            ->getQuery()
            ->getArrayResult();
        foreach ($suscripcion as $s) {
            $pagos = $s['pagos'];
            if (count($pagos) == 0 && $s['id_payu']) {
                array_push($limbo, $s);
            }
        }
        $activas = [];
        $rechazados = [];
        foreach ($limbo as $s) {
            $activa = false;
            $resp = $this->qi->getPagoSuscripcionPayu($s['id_payu']);
            $sus = $this->getDoctrine()->getRepository(Suscripcion::class)->find($s['id']);
            if (isset($resp->recurringBillList)) {
                foreach ($resp->recurringBillList as $p) {
                    $pago = new PagoSuscripcion();
                    $pago->setIdPayu($p->id);
                    $pago->setOrderId($p->subscriptionId);
                    $pago->setMonto($p->amount);
                    $pago->setMoneda($p->currency);
                    $fecha = new \DateTime();
                    $fecha->setTimestamp($p->dateCharge);
                    $pago->setFecha($fecha);
                    $pago->setState($p->state);
                    $pago->setSuscripcion($sus);
                    $em->persist($pago);
                    $em->flush();
                    $this->qi->saveFire($pago);
                    if ($p->state == 'completed' || $p->state == 'PAID') {
                        $activa = $activa || true;
                    } else {
                        $activa = $activa || false;
                    }
                }
            } else {
                $pago = new PagoSuscripcion();
                $pago->setIdPayu("");
                $pago->setOrderId("");
                $pago->setMonto(0);
                $pago->setMoneda("COP");
                $fecha = new \DateTime();
                $pago->setFecha($fecha);
                $pago->setState("EXPIRED");
                $pago->setSuscripcion($sus);
                $em->persist($pago);
                $em->flush();
            }
            if ($activa) {
                $sus->setActiva(1);
                $em->persist($sus);
                $em->flush();
                $sus->setEmail("");
                $sus->setDuracion(0);
                $sus->setIdPayu("");
                $sus->setPayerId("");
                $sus->setIdPaypal("");
                $sus->setPlanId("");
                $sus->setUsuario(null);
                $sus->setValidaHasta(null);
                $this->qi->saveFire($sus);
                array_push($activas, $s['id']);
            } else {
                array_push($rechazados, $s['id']);
            }

        }


        return $this->render('basic/sincronizarPayu.html.twig', [
            'activas' => $activas,
            'rechazadas' => $rechazados
        ]);

    }

    public function reembolsarAction()
    {
        // controllers extending the base AdminController get access to the
        // following variables:
        //   $this->request, stores the current request
        //   $this->em, stores the Entity Manager for this Doctrine entity

        // change the properties of the given entity and save the changes
        $id = $this->request->query->get('id');
        $entity = $this->em->getRepository(Suscripcion::class)->find($id);
        $pago = $this->em->getRepository(PagoSuscripcion::class)->findOneBy(array('suscripcion' => $entity, 'state' => 'APPROVED'), array('id' => 'desc'));
        $refund = $this->qi->makeRefundPayu($pago, $entity);
        if ($refund) {
            if ($refund->code == 'SUCCESS') {

                $entity->setActiva(0);
                $entity->setReembolsada(true);
                $entity->setFechaReembolso(new \DateTime());
                $this->em->persist($entity);

                $pago->setState('REFUND_REQUESTED');
                $this->em->persist($entity);

                $this->em->flush();
                $this->addFlash('success', 'Solicitud de reembolso enviada');
            } else {
                $this->addFlash('error', $refund->error);
            }


        } else {
            $this->addFlash('error', 'Ha ocurrido un error, revisa que sea una transacción de PAYU e intenta de nuevo');
            //dd('no pago');
        }
        // redirect to the 'list' view of the given entity ...
        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'usuario' => $entity->getUsuario()->getId(),
            'entity' => $this->request->query->get('entity'),
        ));

    }

    public function getSuscripcionesDobles()
    {
        $em = $this->getDoctrine()->getManager();

        $users = [];
        $duplicados = [];

        $limit = 5000;
        $offset = 0;
        while ($suscripciones = $em->getRepository(Suscripcion::class)->findBy(array("activa" => 1), array(), $limit, $offset)) {
            foreach ($suscripciones as $s) {
                $userId = $s->getUsuario()->getId();
                if (isset($users[$userId])) {
                    $users[$userId] = $users[$userId] + 1;
                } else {
                    $users[$userId] = 1;
                }

            }

            $offset += $limit;
        }

        foreach ($users as $k => $u) {
            if ($u > 1) {
                $duplicados[$k] = $u;
            }

        }

        return $duplicados;
    }

    public function listSuscripcionDobleAction()
    {
        $duplicados = $this->getSuscripcionesDobles();

        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        if (count($duplicados) > 0) {
            $this->entity['list']['dql_filter'] = "entity.id in (" . join(',', array_keys($duplicados)) . ')';
        } else {
            $this->entity['list']['dql_filter'] = 'entity.id =-1';
        }
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        //dump($this->entity['list']['dql_filter']);
        //die();

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['list', $this->entity['templates']['list'], $parameters]);

    }


    protected function initialize(Request $request)
    {
        parent::initialize($request);
    }

    public function createNewUsuarioEntity()
    {
        return $this->userManager->createUser();
    }

    public function persistUsuarioEntity($user)
    {
        $this->userManager->updateUser($user, false);
        parent::persistEntity($user);
    }

    public function updateUsuarioEntity($user)
    {
        $this->userManager->updateUser($user, false);
        parent::updateEntity($user);
    }


    protected function listSuscripcionAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $usuario = $this->request->query->get('usuario', null);
        if ($usuario) {
            $this->entity['list']['dql_filter'] = "entity.usuario = " . $usuario;
        } else {
            $this->entity['list']['dql_filter'] = "";
        }
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        //dump($this->entity['list']['dql_filter']);
        //die();

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['list', $this->entity['templates']['list'], $parameters]);
    }

    protected function listPagoSuscripcionAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $suscripcion = $this->request->query->get('suscripcion', null);
        if ($suscripcion) {
            $this->entity['list']['dql_filter'] = "entity.suscripcion = " . $suscripcion;
        } else {
            $this->entity['list']['dql_filter'] = "";
        }
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        //dump($this->entity['list']['dql_filter']);
        //die();

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['list', $this->entity['templates']['list'], $parameters]);
    }


    public function mailAprobacion($asunto, $llave, $contenido_mail)
    {
        $qi = $this->qi;
        $html = $qi->getTextoBig('mail_base');
        $html = str_replace(['(contenido)']
            , [
                $contenido_mail
            ]
            , $html);
        $mails = $qi->getSetting($llave);
        foreach (explode(',', $mails) as $mail) {
            $qi->sendMailAWS('RECORDATI | ' . $asunto, $mail, $html);
        }
    }

    public function createUserAndSubscriptionFromPayUData($dataPayu, UserPasswordEncoderInterface $passwordEncoder, $fechaVenc, $fechaPago)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(Usuario::class)->findOneBy(array("email" => ($dataPayu["customer"]->email)));
        if (!$user) {
            $user = new Usuario();
            $user->setUsername($dataPayu["customer"]->email);
            $user->setEmail($dataPayu["customer"]->email);
            $user->setNombre($dataPayu["customer"]->fullName);
            $user->setEnabled(true);
            $user->setIdPayu($dataPayu["customer"]->id);

            if (!$user->getUsername()) return null;

            $encodedPassword = $passwordEncoder->encodePassword($user, $dataPayu["subscription"]->id);
            $user->setPassword($encodedPassword);
            $em->persist($user);
            $em->flush();
        }

        $subscription = new Suscripcion();
        $subscription->setUsuario($user);
        $subscription->setActiva(true);
        $subscription->setIdPayu($dataPayu["subscription"]->id);
        $subscription->setPlanSuscripcion($this->getDoctrine()->getRepository(Plan::class)->find(1));
        $subscription->setEmail($dataPayu["customer"]->email);
        $subscription->setStatus("AUTOCORREGIDA");
        $subscription->setValidaHasta($fechaVenc);
        $subscription->setIdPayu($dataPayu["customer"]->id);
        $subscription->setPayerId($dataPayu["customer"]->id);

        $em->persist($subscription);
        $em->flush();

        $pagoSubs = new PagoSuscripcion();
        $pagoSubs->setSuscripcion($subscription);
        $pagoSubs->setState("APPROVED");
        $pagoSubs->setIdPayu($dataPayu["subscription"]->id);
        $pagoSubs->setFecha($fechaPago);

        $subscription->addPago($pagoSubs);
        $user->addSuscripcione($subscription);

        $em->persist($pagoSubs);
        $em->flush();
        return $subscription;
    }

    public function createUserAndSubscriptionFromPayPalData($dataPayPal, $passwordEncoder, $fechaVenc, $fechaPago)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(Usuario::class)->findOneBy(array("email" => ($dataPayPal->subscriber->email_address)));
        if (!$user) {
            $user = new Usuario();
            $user->setUsername($dataPayPal->subscriber->email_address);
            $user->setEmail($dataPayPal->subscriber->email_address);
            $user->setNombre($dataPayPal->subscriber->name->given_name);
            $user->setApellido($dataPayPal->subscriber->name->surname);
            $user->setEnabled(true);

            if (!$user->getUsername()) return null;
            $encodedPassword = $passwordEncoder->encodePassword($user, $dataPayPal->id);
            $user->setPassword($encodedPassword);
            $em->persist($user);
            $em->flush();
        }

        $subscription = new Suscripcion();
        $subscription->setUsuario($user);
        $subscription->setActiva(true);
        $subscription->setIdPayu($dataPayPal->id);
        $subscription->setPlanSuscripcion($this->getDoctrine()->getRepository(Plan::class)->find(1));
        $subscription->setEmail($dataPayPal->subscriber->email_address);
        $subscription->setStatus("AUTOCORREGIDA");
        $subscription->setValidaHasta($fechaVenc);
        $subscription->setIdPaypal($dataPayPal->id);
        $subscription->setPayerId($dataPayPal->id);

        $em->persist($subscription);
        $em->flush();

        $pagoSubs = new PagoSuscripcion();
        $pagoSubs->setSuscripcion($subscription);
        $pagoSubs->setState("APPROVED");
        $pagoSubs->setIdPaypal($dataPayPal->id);
        $pagoSubs->setFecha($fechaPago);

        $subscription->addPago($pagoSubs);
        $user->addSuscripcione($subscription);

        $em->persist($pagoSubs);
        $em->flush();
        return $subscription;
    }


    /**
     * @Route("/suscripciones-dobles/arreglar", name="arreglar_suscripciones_dobles", methods={"POST"})
     */
    public function fixSuscripcionesDobles(Request $request)
    {
        $duplicados = $this->getSuscripcionesDobles();

        foreach ($duplicados as $key => $value) {
            $user = $this->getDoctrine()->getRepository(Usuario::class)->find($key);
            $suscripciones = $user->getSuscripciones();
            foreach ($suscripciones as $suscripcion) {
                if (!$suscripcion->getIdPayu() && !$suscripcion->getIdPaypal() && $suscripcion->getActiva()) {
                    $suscripcion->setActiva(false);
                }

                if ($user->getSuscripcionesActivas() > 1 && $suscripcion->getActiva() && ($suscripcion->getIdPaypal() || $suscripcion->getIdPayu())) {

                    if ($suscripcion->getIdPayu()) {
                        if (!$suscripcion->getReembolsada()) {
                            $pago = $suscripcion->getPagos()[0];
                            if ($pago) {
                                $pago->setState("REEMBOLSADO");
                                //PROD ONLY
                                $this->qi->makeRefundPayu($pago, $suscripcion);
                                $suscripcion->setActiva(false);
                            }
                        }
                    } else {
                        //PROD ONLY
                        $this->qi->cancelarSuscripcion($suscripcion);
                        $suscripcion->setActiva(false);
                    }
                }
                $this->getDoctrine()->getManager()->flush();
            }
        }

        $this->getDoctrine()->getManager()->flush();

        return $this->redirect($this->generateUrl('easyadmin', array("entity" => "suscripcionDoble", "action" => "list")));
    }

    /**
     * @Route("/facturas", name="faturas_admin")
     */
    public function showFacturas(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $em = $this->getDoctrine()->getManager();
        $dateBegin = null;
        $dateFinal = null;
        $form = $this->createFormBuilder()
            ->add('dateBegin', DateType::class)
            ->add('dateFinal', DateType::class)
            ->add('fix', CheckboxType::class, array('required' => false))
            ->getForm();
        $form->handleRequest($request);
        $facturasPayU = null;
        $pagas = [];
        $newSubscription = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $dateBegin = ($form->get('dateBegin')->getData())->format('Y-m-d');
            $dateFinal = ($form->get('dateFinal')->getData())->format('Y-m-d');
            $fix = $form->get('fix')->getData();

            $facturasPayU = $this->qi->getRecurringBillsPayU($dateBegin, $dateFinal);
            $facturasPayPal = $this->qi->getRecurringBillsPayPal($dateBegin, $dateFinal);

            if ($facturasPayPal) {
                foreach ($facturasPayPal as $factura) {
                    try {
                        $pagaInfo = array("paypalId" => $factura->transaction_info->paypal_reference_id, "payuId" => "");

                        $suscripcion = $this->getDoctrine()->getRepository(Suscripcion::class)
                            ->findOneBy(array('id_paypal' => $factura->transaction_info->paypal_reference_id));

                        $fechaPago = new \DateTime("@" . floor(strtotime($factura->transaction_info->transaction_initiation_date)));
                        $fechaVenc = new \DateTime("@" . ($fechaPago->getTimestamp() + 60 * 60 * 24 * 31));

                        $pagaInfo["fechaPago"] = $fechaPago;
                        $pagaInfo["fechaVenc"] = $fechaVenc;
                        $pagaInfo["pasarela"] = "PayPal";
                        $pagaInfo["monto"] = "" . $factura->transaction_info->transaction_amount->currency_code . " " . $factura->transaction_info->transaction_amount->value;

                        if (!$suscripcion) {
                            $pagaInfo["suscripcionId"] = "EMPTY";
                            $pagaInfo["usuario"] = "EMPTY";
                            $pagaInfo["new"] = false;
                            if ($fix) {
                                $dataPayPal = $this->qi->getCustomerAndSubscriptionPayPal($pagaInfo["paypalId"]);
                                $newSubscription = $this->createUserAndSubscriptionFromPayPalData($dataPayPal, $passwordEncoder, $fechaVenc, $fechaPago);
                                if ($newSubscription) {
                                    $pagaInfo["new"] = true;
                                    $pagaInfo["suscripcionId"] = $newSubscription->getId();
                                    $pagaInfo["usuario"] = $newSubscription->getUsuario()->getEmail();
                                }
                            }
                            array_push($pagas, $pagaInfo);
                        } elseif ( $suscripcion->getActiva() == 0 && $fechaVenc->getTimestamp() > time() && !$suscripcion->getReembolsada()) {
                            if( !$suscripcion->getUltimoPago() || ( $suscripcion->getUltimoPago() > ( time() - 60 * 60 * 24 * 31) && in_array($suscripcion->getUltimoPago()->getState(), ["completed", "APPROVED", "COMPLETADA MANUALMENTE", "COMPLETADO MANUALMENTE"])) ){
                                $pagaInfo["usuario"] = $suscripcion->getUsuario()->getEmail();
                                $pagaInfo["suscripcionId"] = $suscripcion->getId();
                                $pagaInfo["new"] = false;

                                if ($fix) {
                                    $suscripcion->setActiva(true);
                                    $suscripcion->setValidaHasta($pagaInfo["fechaVenc"]);
                                }
                                array_push($pagas, $pagaInfo);
                            }
                        }
                    } catch (\Exception $ex) {
                        //exit(dump($ex));
                    }
                }
            }

            if ($facturasPayU && $facturasPayU->recurringBillList) {
                $facturasPayU = $facturasPayU->recurringBillList;
                foreach ($facturasPayU as $factura) {
                    try {
                        if ($factura->state === "PAID") {
                            $pagaInfo = array("payuId" => $factura->subscriptionId, "paypalId" => "");
                            $suscripcion = $this->getDoctrine()->getRepository(Suscripcion::class)
                                ->findOneBy(array('id_payu' => $factura->subscriptionId));

                            $fechaPago = new \DateTime("@" . floor(($factura->dateCharge / 1000)));
                            $fechaVenc = new \DateTime("@" . floor((($factura->dateCharge) + 1000 * 60 * 60 * 24 * 31) / 1000));

                            $pagaInfo["fechaPago"] = $fechaPago;
                            $pagaInfo["fechaVenc"] = $fechaVenc;
                            $pagaInfo["monto"] = $factura->amount;
                            $pagaInfo["pasarela"] = "PayU";

                            if (!$suscripcion) {
                                $pagaInfo["suscripcionId"] = "EMPTY";
                                $pagaInfo["usuario"] = "EMPTY";
                                $pagaInfo["new"] = false;
                                if ($fix) {
                                    $dataPayu = $this->qi->getCustomerAndSubscriptionPayU($pagaInfo["payuId"]);
                                    $newSubscription = $this->createUserAndSubscriptionFromPayUData($dataPayu, $passwordEncoder, $fechaVenc, $fechaPago);
                                    if ($newSubscription) {
                                        $pagaInfo["new"] = true;
                                        $pagaInfo["suscripcionId"] = $newSubscription->getId();
                                        $pagaInfo["usuario"] = $newSubscription->getUsuario()->getEmail();
                                    }
                                }
                                array_push($pagas, $pagaInfo);
                            } elseif ( $suscripcion->getActiva() == 0 && $fechaVenc->getTimestamp() > time() && !$suscripcion->getReembolsada()) {
                                if( !$suscripcion->getUltimoPago() || ( $suscripcion->getUltimoPago() > ( time() - 60 * 60 * 24 * 31) && in_array($suscripcion->getUltimoPago()->getState(), ["completed", "APPROVED", "COMPLETADA MANUALMENTE", "COMPLETADO MANUALMENTE"])) ){
                                    $pagaInfo["usuario"] = $suscripcion->getUsuario()->getEmail();
                                    $pagaInfo["suscripcionId"] = $suscripcion->getId();
                                    $pagaInfo["new"] = false;

                                    if ($fix) {
                                        $suscripcion->setActiva(true);
                                        $suscripcion->setValidaHasta($pagaInfo["fechaVenc"]);
                                    }
                                    array_push($pagas, $pagaInfo);
                                }
                            }
                        }

                    } catch (\Exception $ex) {
                    }
                }
            }
            if ($fix) {
                $em->flush();
            }
        }
        return $this->render('easy_admin/facturas.html.twig', [
            'form' => $form->createView(),
            'facturas' => $pagas,
            "user" => $newSubscription
        ]);
    }


    private function reporteNativoGenORM($cols, $cadena)
    {
        $rsm = new ResultSetMapping();
        foreach ($cols as $col) {
            $rsm->addScalarResult($col, $col, 'string');
        }
        $reporte = $this->getDoctrine()->getManager()->createNativeQuery($cadena, $rsm)->getArrayResult();
        return $reporte;
    }

}
