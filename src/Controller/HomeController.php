<?php


namespace App\Controller;

use App\Entity\Cancelacion;
use App\Entity\Contacto;

use App\Entity\Faq;
use App\Entity\PagoSuscripcion;
use App\Entity\PayuLog;
use App\Entity\Plan;
use App\Entity\Suscripcion;
use App\Entity\Usuario;
use App\Entity\Video;
use App\Form\ContactoType;
use App\Service\QI;
use Aws\CloudFront\CloudFrontClient;
use FOS\UserBundle\Event\GetResponseNullableUserEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Algolia\SearchBundle\SearchService;


class HomeController extends AbstractController
{
    private $qi;
    private $userManager;
    private $retryTtl;
    private $tokenGenerator;
    protected $searchService;


    public function __construct(QI $qi, UserManagerInterface $userManager, TokenGeneratorInterface $tokenGenerator, SearchService $searchService)
    {
        $this->qi = $qi;
        $this->userManager = $userManager;
        $this->retryTtl = 7200;
        $this->tokenGenerator = $tokenGenerator;
        $this->searchService = $searchService;
    }

    /**
     * @Route("/ejecutarpagolog", name="ejecutarpagolog")
     */
    public function ejecutarpagolog(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $page = 7;
        $size = 500;
        $pagos = $this->getDoctrine()->getManager()->createQueryBuilder()
            ->select('p')
            ->from(PayuLog::class, 'p')
            ->where('p.id < 89000')
            ->setFirstResult(($page - 1) * $size)
            ->setMaxResults($size)
            ->getQuery()
            ->getArrayResult();
        //dd($pagos);

        foreach ($pagos as $p) {
            $json = json_decode($p["data"]);
            if (!$json) {
                dump($p);
            } else {
                if (isset($json->resource)) {//paypal
                    //dump("paypal");
                    if ($json->event_type == "PAYMENT.SALE.COMPLETED") {

                        $id = $json->resource->id;
                        $state = $json->resource->state;
                        $billing_agreement_id = $json->resource->billing_agreement_id;

                        /* @var $suscripcion Suscripcion */
                        $suscripcion = $this->getDoctrine()->getRepository(Suscripcion::class)->findOneBy(array("id_paypal" => $billing_agreement_id));
                        if (!$suscripcion) {
                            dump($json);

                            //dd($billing_agreement_id);
                        } else {
                            $pago = new PagoSuscripcion();
                            $pago->setSuscripcion($suscripcion);
                            $pago->setState($state);
                            $pago->setIdPaypal($id);
                            $em->persist($pago);
                            $em->flush();
                            $suscripcion->setActiva(true);
                            $valida = new \DateTime();
                            $valida = new \DateTime(date('Y-m-d', strtotime("+2 days", strtotime("+" . $suscripcion->getDuracion() . " months", $valida->getTimestamp()))));
                            //dd($valida);
                            $suscripcion->setValidaHasta($valida);


                            //dump($suscripcion);
                            $em->persist($suscripcion);
                            $em->flush();
                        }

                    }
                } else {//payu
                    //dump("payu");

                    $referenceCode = $json->reference_sale;
                    $arr_ref = explode('_', $referenceCode);
                    $reference_recurring_payment = $json->reference_recurring_payment;
                    $susId = explode('_', $reference_recurring_payment)[0];

                    $suscripcion = $this->getDoctrine()->getRepository(Suscripcion::class)->findOneBy(array('id_payu' => $susId));
                    if (!$suscripcion) {
                        dump($json);
                        dd($susId);
                    } else {
                        //dd($suscripcion);
                        $cus = "";
                        if (isset($json->cus)) {
                            $cus = $json->cus;
                        }
                        $pago = new PagoSuscripcion();
                        $pago->setIdPayu($json->transaction_id);
                        $pago->setOrderId($json->reference_sale);
                        $pago->setMonto($json->value);
                        $pago->setMoneda($json->currency);
                        $pago->setCus($cus);
                        $pago->setReferencePol($json->reference_pol);
                        $fecha = new \DateTime($json->operation_date);
                        $pago->setFecha($fecha);
                        $pago->setState($json->response_message_pol);
                        $pago->setSuscripcion($suscripcion);

                        //dump($pago);
                        $em->persist($pago);
                        $em->flush();

                        if ($json->state_pol == 4) {

                            $suscripcion->setActiva(1);
                            $valida = new \DateTime();
                            $valida = new \DateTime(date('Y-m-d', strtotime("+2 days", strtotime("+" . $suscripcion->getDuracion() . " months", $valida->getTimestamp()))));
                            //dd($valida);
                            $suscripcion->setValidaHasta($valida);
                            //dump($suscripcion);
                            $em->persist($suscripcion);
                            $em->flush();
                        }
                    }
                }
            }

        }
        dd("fin " . $page . " " . count($pagos));

    }

    /**
     * @Route("/aws_video", name="aws_video")
     */
    public function awsVideo(Request $request)
    {
        $ruta = __DIR__ . '/pk-APKAI5SCAT6RQHF27WAA.pem';
        $key_pair_id = 'APKAI5SCAT6RQHF27WAA';
        //dd($ruta);

        $url_domain = "https://d2ni8vd6etkcfg.cloudfront.net/assets/";
        $object = "361dc059-0a26-48ab-9cfd-7312fdc50ad9/HLS/test.m3u8";
        //$object = "5f483a49-62c3-4f91-bb25-5441bfb29114/mp4/The+Juanpis+Live+Show+-+Entrevista+a+Esteman_Mp4_Avc_Aac_16x9_1920x1080p_24Hz_6Mbps_qvbr.mp4";
        $expiry = new \DateTime('+10 minutes');
        $params = array(
            'url' => $url_domain . $object,
            'expires' => $expiry->getTimestamp(),
            'private_key' => $ruta,
            'key_pair_id' => $key_pair_id
        );
        $cloudFront = new CloudFrontClient(array(
                'private_key' => $ruta,
                'key_pair_id' => $key_pair_id,
                'region' => 'us-east-1',
                'version' => 'latest')
        );
        $url1 = $cloudFront->getSignedUrl(
            $params
        );

        $url = $cloudFront->getSignedUrl(
            array(
                'url' => $url_domain . '*',
                'expires' => $expiry->getTimestamp(),
                'private_key' => $ruta,
                'key_pair_id' => $key_pair_id
            )
        );;


        $cookies = [];

        $cookies['CloudFront-Key-Pair-Id'] = $key_pair_id;
        $policy =
            [
                'Statement' =>
                    [
                        [
                            'Resource' => $url_domain . '*',
                            'Condition' =>
                                [
                                    'DateLessThan' =>
                                        [
                                            'AWS:EpochTime' => $expiry
                                        ]
                                ]
                        ]
                    ]
            ];
        $policy = json_encode($policy);
        $cookies['CloudFront-Policy'] = base64_encode($policy);
        parse_str(parse_url($url, PHP_URL_QUERY), $signed_url_arr);
        $signature = $signed_url_arr['Signature'];
        $cookies['CloudFront-Signature'] = $signature;
        $host_parts = array_reverse
        (
            explode('.', parse_url($url_domain, PHP_URL_HOST))
        );
        $cookie_domain = 'https://d2ni8vd6etkcfg.cloudfront.net';

        $cookie_path = parse_url($url_domain, PHP_URL_PATH);
        //dd($cookies);
        foreach ($cookies as $cookie_name => $cookie_value) {
            setcookie($cookie_name, $cookie_value, 0, $cookie_path, $cookie_domain);
        }


        return $this->render('basic/aws_video.html.twig', [
            'url' => $url1,
            'signature' => $signature,
            'key_pair_id' => $key_pair_id,
            'expires' => $expiry->getTimestamp()
        ]);
    }

    /**
     * @Route("/resetting/iridian", name="reset-iridian")
     */
    public function sendEmailAction(Request $request)
    {
        $username = $request->request->get('username');

        $user = $this->userManager->findUserByUsernameOrEmail($username);

        $event = new GetResponseNullableUserEvent($user, $request);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        if (null !== $user && !$user->isPasswordRequestNonExpired($this->retryTtl)) {
            $event = new GetResponseUserEvent($user, $request);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }

            if (null === $user->getConfirmationToken()) {
                $user->setConfirmationToken($this->tokenGenerator->generateToken());
            }

            $event = new GetResponseUserEvent($user, $request);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }

            $this->sendResettingEmailMessage($user);

            $user->setPasswordRequestedAt(new \DateTime());
            $this->userManager->updateUser($user);

            $event = new GetResponseUserEvent($user, $request);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }
        }

        return new RedirectResponse($this->generateUrl('fos_user_resetting_check_email', array('username' => $username)));
    }

    public function sendResettingEmailMessage(UserInterface $user)
    {

        $url = $this->generateUrl('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        $mensaje = str_replace('%link%', $url, $this->qi->getTextoBig('mail_contrasena'));
        $this->qi->sendMailAWS($this->qi->getTexto('asunto_mail_resetting'), $user->getEmail(), $mensaje);
    }

    /**
     * @Route("/test", name="test")
     */
    public function test(Request $request)
    {

        $SuscripcionId = 'faa323a2b78e';//resp->id
        $resp = $this->qi->getPagoSuscripcionPayu($SuscripcionId);
        dd($resp);
        $nuevoCliente = array('fullName' => "fern", 'email' => 'ferney@ir.co');
        //$resp = $this->qi->crearClientePayu($nuevoCliente);
        $clienteid = "69493ipxxssk";//resp->id

        $nuevaTarjeta = array("name" => "APPROVED",
            "document" => "1020304050",
            "number" => "4111111111111111",
            "expMonth" => "01",
            "expYear" => "2021",
            "type" => "VISA",
            "address" => array(
                "line1" => "Address Name",
                "line2" => "17 25",
                "line3" => "Of 301",
                "postalCode" => "00000",
                "city" => "City Name",
                "state" => "State Name",
                "country" => "CO",
                "phone" => "300300300"
            ));
        $resp = $this->qi->crearTarjetaUsuario($this->getUser(), $nuevaTarjeta);
        dd($resp);

        //$resp = $this->qi->crearTarjetaPayu($nuevaTarjeta,$clienteid);

        $tokenTarjeta = '60cb2db6-4aed-4601-b810-1d2f89e2b1f6';//resp->token
        $cuotas = 1;
        $planId = $this->qi->getSetting('plan_id_payu');
        $suscripcion = array('clienteId' => $clienteid, 'tokenTarjeta' => $tokenTarjeta, 'installments' => $cuotas, 'planId' => $planId);
        //$resp = $this->qi->crearSuscripcionPayu($suscripcion);
        $dt = new \DateTime();
        $dt->setTimestamp(1585371600000);

        //$resp = $this->qi->getPagosUsuarioPayu('78f2ec0rbzrk');
        return $this->render('basic/home.html.twig', [
        ]);
    }

    /**
     * @Route("/cancelar-reembolsadas", name="cancelarrembolsadas")
     */
    public function cancelarrembolsadas(Request $request)
    {
        $suscripciones = $this->getDoctrine()->getRepository(Suscripcion::class)->findBy(array('reembolsada' => true));
        $resp = [];
        foreach ($suscripciones as $suscripcion) {
            $resp[$suscripcion->getId()] = $this->qi->cancelarSuscripcion($suscripcion);
        }
        dd($resp);
        //$resp = $this->qi->getPagosUsuarioPayu('78f2ec0rbzrk');
        return $this->render('basic/home.html.twig', [
        ]);
    }

    /**
     * @Route("/", name="homepage")
     */
    public function home(Request $request)
    {
        if ($this->getUser()) {
            $estado = $this->checkSuscripcion();
            if ($estado == 'pendiente') {
                return $this->redirectToRoute('esperando_activacion');
            } else if ($estado == 'activa') {
                return $this->redirectToRoute('videos');
            } else if ($estado == 'vencida') {
                return $this->redirectToRoute('info-personal');
            }
        }
        $faqs = $this->getDoctrine()->getRepository(Faq::class)->findBy(array('visible' => true), array('orden' => 'desc'));
        return $this->render('basic/home.html.twig', [
            'faqs' => $faqs
        ]);
    }


    /**
     * @Route("/make-cancelacion", name="make-cancelacion")
     */
    public function makeCancelacion(Request $request)
    {
        if ($this->getUser()) {

            $suscripcion = $this->getDoctrine()->getRepository('App:Suscripcion')->findOneBy(array('usuario' => $this->getUser()), array('activa' => 'desc', 'id' => 'desc'));
            $respuesta = $this->qi->cancelarSuscripcion($suscripcion);
            $cancelacion = new Cancelacion();
            $persist = true;
            if ($suscripcion->getCancelacion()) {
                $cancelacion = $suscripcion->getCancelacion();
                $persist = false;
            } else {
                $cancelacion->setSuscripcion($suscripcion);
            }
            $cancelacion->setFecha(new \DateTime());
            try {
                if(!$respuesta) $respuesta = "NO DATA";
                $respuestaActual = $cancelacion->getRespuesta();
                $nuevaRespuesta = json_encode($respuesta);
                if($respuestaActual) {
                    $nuevaRespuesta = $nuevaRespuesta. "\n\n".$respuestaActual;
                }
                $cancelacion->setRespuesta($nuevaRespuesta);
            } catch (\Exception $e) {
                $errorActual = $cancelacion->getError();
                $nuevoError = $e->getMessage();
                if($errorActual) {
                    $nuevoError = $nuevoError. "\n\n".$errorActual;
                }
                $cancelacion->setError($nuevoError);
            }
            $em = $this->getDoctrine()->getManager();
            if ($persist) {
                $em->persist($cancelacion);
            }
            $em->flush();
            $estado = $this->checkSuscripcion();
            if ($estado == 'pendiente') {
                return $this->redirectToRoute('esperando_activacion');
            } else if ($estado == 'activa') {
                return $this->redirectToRoute('videos');
            } else if ($estado == 'vencida') {
                return $this->redirectToRoute('info-personal');
            }
        }
        return $this->render('basic/home.html.twig', [
        ]);
    }

    /**
     * @Route("/salir-espera/{id}", name="salir-espera")
     */
    public function salirEspera(Request $request, $id)
    {


        $suscripcion = $this->getDoctrine()->getRepository('App:Suscripcion')->find($id);

        $res = $this->qi->checkPagosPSE($suscripcion);

        $pago = new PagoSuscripcion();
        $pago->setIdPayu("0");
        $pago->setIdPaypal("0");
        $pago->setState("EXPIRED_FORCED");
        $pago->setSuscripcion($suscripcion);
        $em = $this->getDoctrine()->getManager();
        $em->persist($pago);
        $em->flush();

        return $this->redirectToRoute('info-personal');


    }

    /**
     * @Route("/check-pago-pse/{id}", name="check-pago-pse")
     */
    public function checkPagoPse(Request $request, $id)
    {


        $suscripcion = $this->getDoctrine()->getRepository('App:Suscripcion')->find($id);

        $res = $this->qi->checkPagosPSE($suscripcion);
        if ($res != null && $res->result->payload != null) {
            $resultado = array('pagos' => 1);
        } else {
            $pago = new PagoSuscripcion();
            $pago->setIdPayu("0");
            $pago->setIdPaypal("0");
            $pago->setState("EXPIRED");
            $pago->setSuscripcion($suscripcion);
            $em = $this->getDoctrine()->getManager();
            $em->persist($pago);
            $em->flush();
            $resultado = array('pagos' => 0);
        }

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        // Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $encoders);
        $json = $serializer->serialize($resultado, 'json');
        return new Response($json);


    }

    public function checkPlan()
    {
        $message = null;
        if (!$this->getUser()) {
            return $message;
        }

        $suscripcion = ($this->getDoctrine()->getRepository('App:Suscripcion')->findOneBy(array('usuario' => $this->getUser()), array('activa' => 'desc', 'id' => 'desc')));
        $plan = $suscripcion->getPlanSuscripcion();
        if ($plan){
            $plan = $suscripcion->getPlanSuscripcion()->getId();
            if ($plan <> 1) {
                $message = 'Tu suscripción se cancelará cuando  finalice el tiempo contratado.';
            }
        }

        return $message;
    }

    /**
     * @Route("/cancelar_suscripcion", name="cancelar_suscripcion")
     */
    public function cancelarSuscripcion(Request $request)
    {
        $planMessage = $this->checkPlan();
        if ($planMessage == null) {
            $estado = $this->checkSuscripcion();
            if ($estado == 'pendiente') {
                return $this->redirectToRoute('esperando_activacion');
            } else if ($estado == 'vencida') {
                return $this->redirectToRoute('info-personal');
            }

            return $this->render('basic/cancelarSuscripcion.html.twig', array(
                "estado" => $estado,
            ));
        } else {
            $request->getSession()->getFlashBag()->add('info', $planMessage);
            return $this->redirectToRoute('videos');
        }
    }

    /**
     * @Route("/mail-prueba", name="mail-prueba")
     */
    public function mailPrueba(Request $request)
    {
        $asunto = 'probando';
        $to = 'ferney@iridian.co';
        $html = 'pruebasss';
        $r = $this->qi->sendMailIB($asunto, $to, $html);
        dd($r);

        return $this->render('basic/mail.html.twig', [
        ]);
    }

    /**
     * @Route("/terminos-condiciones", name="terminos-condiciones")
     */
    public function terminosComndiciones(Request $request)
    {
        return $this->render('basic/terminos_condiciones.html.twig', [
        ]);
    }


    /**
     * @Route("/videos", name="videos")
     */
    public function videos(Request $request)
    {
        $query = $request->get('query');
        $estado = $this->checkSuscripcion();
        if ($estado == 'pendiente') {
            return $this->redirectToRoute('esperando_activacion');
        } else if ($estado == 'vencida') {
            return $this->redirectToRoute('info-personal');
        }
        $destacado = $this->getDoctrine()->getManager()->getRepository('App:Video')->findOneBy(array('destacado' => true), array('orden' => 'asc'));
        $categorias = $this->getDoctrine()->getManager()->getRepository('App:CategoriaVideo')->findBy(array('visible' => true), array('orden' => 'asc'));
        return $this->render('basic/videos.html.twig', array(
            "categorias" => $categorias,
            "destacado" => $destacado,
            'query' => $query
        ));
    }

    /**
     * @Route("/contacto", name="contacto")
     */
    public function contacto(Request $request)
    {
        $contacto = new Contacto();
        $form = $this->createForm(ContactoType::class, $contacto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contacto);
            $entityManager->flush();
            $asunto = $this->qi->getSetting('asunto_mail_contacto');
            $receiver = $this->qi->getSetting('receiver_mail_contacto');
            $this->qi->sendMail($asunto, $receiver, $contacto->getHtml());
            return $this->render('contacto/contacto.html.twig', [
                'gracias' => true
            ]);
        }
        return $this->render('contacto/contacto.html.twig', [
            'gracias' => false,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/buscador", name="buscador")
     */
    public function buscador(Request $request)
    {
        $query = $request->get('query');
        $em = $this->getDoctrine()->getManagerForClass(Video::class);
        $videos = $this->searchService->search($em, Video::class, $query);
        $estado = $this->checkSuscripcion();
        if ($estado == 'pendiente') {
            return $this->redirectToRoute('esperando_activacion');
        } else if ($estado == 'vencida') {
            return $this->redirectToRoute('info-personal');
        }
        return $this->render('productos/buscador.html.twig', [
            'videos' => $videos,
            'query' => $query,

        ]);
    }

    /**
     * @Route("/pagar-payu/{id}", name="pagar-payu")
     */
    public function pagarPayu(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $plan = $this->getDoctrine()->getRepository(Plan::class)->findOneBy(array('id_payu' => $id));
        /** @var  $user User */
        $user = $this->getUser();
        /** @var  $newS Suscripcion */
        $newS = new Suscripcion();

        $newS->setDuracion($plan->getDuracion());
        $valida = new \DateTime();
        $valida = new \DateTime(date('Y-m-d', strtotime("+2 days", strtotime("+" . $newS->getDuracion() . " months", $valida->getTimestamp()))));
        //dd($valida);
        $newS->setValidaHasta($valida);

        $newS->setUsuario($user);
        $newS->setEmail($user->getEmail());
        $newS->setPayerId($user->getId());
        $newS->setPlanId($plan->getidPayu());
        $newS->setStatus('creada_payu');
        //$newS->setIdPayu($suscripcion->id);
        $newS->setActiva(false);
        $newS->setPlanSuscripcion($plan);
        $em->persist($newS);
        $em->flush();

        $referenceCode = 'Juanpis_' . $newS->getId();
        $newS->setIdPayu($referenceCode);

        $subtotal = 0;
        $impuestos = 0;
        $impuestoTotal = 0;
        $test = $this->qi->getSetting("modo_pruebas_payu");
        /** @var  $envio Direccion */
        $costo_envio = 0;
        $total = 0;
        $descripcion_text = '';
        $descripcion = '<table align="center" border="0" cellpadding="5px" cellspacing="0" class="mcnTextContentContainer" style="color: #999;font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%">';
        $descripcion .= '<thead>
                            <tr>
                                <th>Plan</th>
                                <th>Precio</th>
                            </tr>
                        </thead>
                        <tbody>';
        $root = $this->generateUrl('homepage', array(), UrlGeneratorInterface::ABSOLUTE_URL);


        $subtotal = $plan->getPrecioBase();
        $impuestos = $plan->getImpuesto();
        $impuestoTotal = $impuestoTotal + $impuestos;
        $total += $subtotal + $impuestos;


        $descripcion .= '<tr style="border-bottom: 1px solid #eae9e9;">';

        $descripcion .= '<td style="text-align:center;"><p style="display:inline-block;vertical-align:middle">
                                        <span style="width:100%;display:inline-block;text-align: center;">' . $plan->getNombre() . '</span>
                                    </p></td>';
        $descripcion .= '<td style="text-align:center;"> $' . number_format($plan->getPrecio()) . '</td>';
        $descripcion .= '</tr>';

        $descripcion_text .= '|' . $plan->getNombre() . ' - ';
        $descripcion_text .= '$' . number_format($plan->getPrecio()) . ' - ';

        $descripcion .= '<tr>
                        <td colspan="3"></td>
                        <td style="text-align: right;"><strong>Total</strong></td>
                        <td style="text-align:center;">$' . number_format($total) . '</td>
                    </tr>';


        $descripcion .= '</table>';


        $total = $total + $costo_envio;
        $em->persist($newS);
        $em->flush();
        $descripcion_text .= 'Costo envio, $' . number_format($costo_envio) . ' |';
        $descripcion_text .= 'TOTAL: $' . number_format($total);


        $tax = round($impuestoTotal, 0);
        $taxReturnBase = round($total - $costo_envio - $impuestoTotal, 0);
        $merchantId = $this->qi->getSetting('payu_merchantId');
        $apikey = $this->qi->getSetting('payu_apiKey');
        $currency = 'COP';
        $accountId = $this->qi->getSetting('payu_accountId_webCheckout');
        $firma = md5($apikey . '~' . $merchantId . '~' . $referenceCode . '~' . $total . '~' . $currency);


        return $this->render('compra/pagar.html.twig', [
            'suscripcion' => $newS,
            'tax' => $tax,
            'test' => $test,
            'taxReturnBase' => $taxReturnBase,
            'firma' => $firma,
            'referenceCode' => $referenceCode,
            'descripcion_text' => substr($descripcion_text, 0, 250),
            'merchantId' => $merchantId,
            'apiKey' => $apikey,
            'accountId' => $accountId,
            'currency' => $currency,
            'total' => $total,
            'email' => $user->getEmail()
        ]);
    }


    /**
     * @Route("/info-personal", name="info-personal")
     */
    public function infoPersonal(Request $request)
    {
        $planes = $this->getDoctrine()->getRepository(Plan::class)->findBy(array('visible' => true, 'es_recurrente' => false), array('orden' => 'desc'));
        $planeRec = $this->getDoctrine()->getRepository(Plan::class)->findOneBy(array('visible' => true, 'es_recurrente' => true), array('orden' => 'desc'));
        $suscripcionError = false;
        $estado = $this->checkSuscripcion();
        if ($estado == 'activa') {
            return $this->redirectToRoute('videos');
        }
        if ($estado == 'pendiente') {
            return $this->redirectToRoute('esperando_activacion');
        }
        if ($request->isMethod('POST')) {

            $data = $request->request->all();
            $data['number'] = str_replace("-", "", $data['number']);

            $resp = $this->qi->crearTarjetaUsuario($this->getUser(), $data);
            $error = '';
            if (!is_numeric($resp)) {
                $desc = $resp->description;
                if ($this->startsWith($desc, 'number:')) {
                    $error = 'Lo sentimos, hay un error con el número de la tarjeta';
                    //dd($error);
                } else {
                    if ($desc == 'The credit card expiration date is not valid') {
                        $error = 'Lo sentimos, la fecha de expiración es inválida';
                    } else {
                        $error = $desc;
                    }
                }
                //dd($resp);
            }
            if ($error == '') {
                return $this->redirectToRoute('esperando_activacion');
            } else {
                return $this->render('perfil/info_personal.html.twig', [
                    'datos' => $data,
                    'error' => $error,
                    'show' => 'show',
                    'suscripcion_error' => $suscripcionError,
                    'planes' => $planes
                ]);
            }
        }
        $test = 0;
        if ($test) {

            $data = array("name" => "APPROVED",
                "document" => "1020304050",
                "number" => "411111111111111",
                "expMonth" => "01",
                "expYear" => "2021",
                "type" => "VISA",
                "address" => array(
                    "line1" => "Address Name",
                    "line2" => "17 25",
                    "line3" => "Of 301",
                    "postalCode" => "00000",
                    "city" => "City Name",
                    "state" => "State Name",
                    "country" => "CO",
                    "phone" => "300300300"
                ));
        } else {
            $data = array("name" => "",
                "document" => "",
                "number" => "",
                "expMonth" => "",
                "expYear" => "",
                "type" => "",
                "address" => array(
                    "line1" => "",
                    "line2" => "",
                    "line3" => "",
                    "postalCode" => "",
                    "city" => "",
                    "state" => "",
                    "country" => "",
                    "phone" => ""
                ));
        }


        $suscripcion = $this->getDoctrine()->getRepository('App:Suscripcion')->findOneBy(array('usuario' => $this->getUser()), array("activa" => "desc",'id' => 'desc'));
        $estado = $this->checkSuscripcion();

        if ($estado == 'pendiente') {
            return $this->redirectToRoute('esperando_activacion');
        } else if ($estado == 'activa') {
            return $this->redirectToRoute('videos');
        } else if ($estado == 'vencida' && $suscripcion) {
            $suscripcionError = true;
        }


        return $this->render('perfil/info_personal.html.twig', [
            'datos' => $data,
            'error' => '',
            'show' => '',
            'suscripcion_error' => $suscripcionError,
            'planes' => $planes,
            'planRec' => $planeRec
        ]);
    }

    function startsWith($string, $startString)
    {
        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    }


    /**
     * @Route("/esperando_activacion", name="esperando_activacion")
     */
    public function esperandoActivacion(Request $request)
    {
        $estado = $this->checkSuscripcion();

        if ($estado == 'pendiente') {

        } else if ($estado == 'activa') {
            return $this->redirectToRoute('videos');
        } else if ($estado == 'vencida') {
            return $this->redirectToRoute('info-personal');
        }

        $suscripcion = $this->getDoctrine()->getRepository(Suscripcion::class)->findOneBy(array('usuario' => $this->getUser()), array('id' => 'desc'));

        $id = $suscripcion->getId();

        return $this->render('perfil/esperando_activacion.html.twig', [
            'id' => $id,
            'ref' => $suscripcion->getIdPayu()
        ]);
    }

    function checkSuscripcion()
    {

        if (!$this->getUser()) {
            return 'vencida';
        }

        $suscripcion = $this->getDoctrine()->getRepository('App:Suscripcion')->findOneBy(array('usuario' => $this->getUser()), array('activa' => 'desc', 'id' => 'desc'));
        $hoy = new \DateTime();


        if ($suscripcion) {
            $validaHasta = $suscripcion->getValidaHasta();
            if ($hoy > $validaHasta) {

                $suscripcion->setActiva(0);
                $em = $this->getDoctrine()->getManager();
                $em->persist($suscripcion);
                $em->flush();
                return 'vencida';
            }
        }

        if ($suscripcion && $suscripcion->getActiva()) {
            return "activa";
        } else {
            $pagos_pendientes = $this->getDoctrine()->getRepository(PagoSuscripcion::class)->findBy(array('suscripcion' => $suscripcion));

            $esperando = true;
            foreach ($pagos_pendientes as $p) {
                $esperando = $esperando && $p->getState() == 'PENDING';
            }

            if ($suscripcion && $esperando) {
                return "pendiente";
            } else {
                return 'vencida';
            }
        }
    }

    /**
     * @Route("/recuperar", name="recuperar")
     */
    public function recuperar(Request $request)
    {
        return $this->render('perfil/recuperar.html.twig', [
        ]);
    }


    /**
     * @Route("/terminos-condiciones", name="terminos")
     */
    public function terminos(Request $request)
    {
        $titulo = $this->qi->getTexto('title_terminos');
        $descripcion = $this->qi->getTextoBig('terminos');
        return $this->render('termino_politica/terminos.html.twig', ['titulo' => $titulo, 'descripcion' => $descripcion, 'ruta' => 'terminos', 'query' => ''
        ]);
    }

    /**
     * @Route("/politica-privacidad", name="politica")
     */
    public function politica(Request $request)
    {
        $titulo = $this->qi->getTexto('title_politica');
        $descripcion = $this->qi->getTextoBig('politica');
        return $this->render('termino_politica/terminos.html.twig', ['titulo' => $titulo, 'descripcion' => $descripcion, 'ruta' => 'politica'
        ]);
    }

    /**
     * @Route("/correo-paypal", name="correos_paypal")
     */
    public function correosPaypal(Request $request)
    {
        $tuplas = [['Diego Acevedo', 'dievedo@gmail.com', 'I-R6UPLV3L7SC9'], ['Diego Acevedo', 'dievedo@gmail.com', 'I-R6UPLV3L7SC9'], ['Paul Calvachi Bastidas', 'fernando.calvachi@gmail.com', 'I-915FWBSWMK6F'], ['Paul Calvachi Bastidas', 'fernando.calvachi@gmail.com', 'I-915FWBSWMK6F'], ['Valentina Echavarria', 'vcardenasechavarria@gmail.com', 'I-FJSNST2L8ETJ'], ['Valentina Echavarria', 'vcardenasechavarria@gmail.com', 'I-FJSNST2L8ETJ'], ['gustavo muriel', 'tavo55403@hotmail.com', 'I-23ACRUC03USF'], ['gustavo muriel', 'tavo55403@hotmail.com', 'I-23ACRUC03USF'], ['ana maria vallejo fernandez', 'annyvallejof@gmail.com', 'I-GY3Y4MDJT19K'], ['Tomas Becerra', 'dacali.designs@gmail.com', 'I-C2SR8VJNMS0H'], ['jonathan cardona', 'djjonyg@gmail.com', 'I-4UFL898ME6YV'], ['Tomas Becerra', 'dacali.designs@gmail.com', 'I-C2SR8VJNMS0H'], ['jonathan cardona', 'djjonyg@gmail.com', 'I-4UFL898ME6YV'], ['Ana Oviedo', 'isa.1224@hotmail.com', 'I-X48BKE8T27AV'], ['Ana Oviedo', 'isa.1224@hotmail.com', 'I-X48BKE8T27AV'], ['Jose Condor Lopez', 'jose.g.condor@live.de', 'I-8FEC2B3YFGJM'], ['Jose Condor Lopez', 'jose.g.condor@live.de', 'I-8FEC2B3YFGJM'], ['Jonathan Elias Argueta', 'jonathanarguett@gmail.com', 'I-LNY35RUGCR41'], ['Andres Doval', 'afdoval@me.com', 'I-YKVTT62RC6YP'], ['Jonathan Elias Argueta', 'jonathanarguett@gmail.com', 'I-LNY35RUGCR41'], ['Andres Doval', 'afdoval@me.com', 'I-YKVTT62RC6YP'], ['santiago otalvaro', 'santy-2000@hotmail.com', 'I-HVE88TNP39DK'], ['santiago otalvaro', 'santy-2000@hotmail.com', 'I-HVE88TNP39DK'], ['Monica Rozo', 'mona.rozo@hotmail.com', 'I-W1VPCMTLR2RY'], ['Monica Rozo', 'mona.rozo@hotmail.com', 'I-W1VPCMTLR2RY'], ['Luz Angela aguirre', 'luza1323@hotmail.com', 'I-8R7KHKVHJX27'], ['gloria restrepo', 'restrepo28@comcast.net', 'I-KLHXBBYSY6GV'], ['Laura Cepeda', 'laurabcepedaerazo@yahoo.es', 'I-NVABT6L63M2K'], ['Martha Juliana Pacheco Sanchez', 'julianapachecos22@gmail.com', 'I-6TPUC3P4WPRA'], ['Didier Garcia', 'didiertours2009@icloud.com', 'I-GPVFW1P6W1GP'], ['Rafael Luna', 'tenkula22@gmail.com', 'I-LKS9LD1SVKDP'], ['Luisa Montoya', 'lm.montoyaq@gmail.com', 'I-3N1VR7W1RA36'], ['Angela Calinski', 'angela.calinski@outlook.com', 'I-VBPWPRR31Y3K'], ['Paula Amaya', 'pamarquez05@gmail.com', 'I-1L9A7MBNUEJ8'], ['Maria Barbosa', 'angieb34@hotmail.com', 'I-SSAATP9VE21A'], ['Andres Castaneda', 'fusser_69@hotmail.com', 'I-VWDRD4MT7VWB'], ['john rendon', 'rendon2804@gmail.com', 'I-DNVWXY90M0HL'], ['Laura Duque', 'tefam_13@hotmail.com', 'I-VG1RERKHG5DX'], ['Carolina lopez tobon', 'caro.lopez23@hotmail.com', 'I-KU72818PHL1H'], ['Silvia Piedrahita', 'silva82ta@gmail.com', 'I-NYPKAJ3G0JL1'], ['jimmy neira', 'janl34@hotmail.com', 'I-E43G4TFWHSYE'], ['Deimer Quiroga Lopez', 'deimer.quiroga@latam.com', 'I-FL064CUF9TDB'], ['Cristian Rodas', 'Cristian.rodas92@hotmail.com', 'I-FXVVKJ4H0JLX'], ['Angie Argel', 'angieargelgonzalez@gmail.com', 'I-U6C9L3H1FFY5'], ['Lugo Designs Inc', 'cristianlugo.r@gmail.com', 'I-CH0BE1LV812N'], ['Ricardo Adolfo Saavedra Lozano', 'riasaajr@yahoo.es', 'I-STXH10C0N5ME'], ['Juan Diaz', 'juanpablodiazsoto22@gmail.com', 'I-J2L6F3042L85'], ['Ervin Soza R', 'nabh_3086@yahoo.com', 'I-S6K5J2UN47PT'], ['Ali Fares', 'alefares09@hotmail.com', 'I-055X3LTU4V51'], ['sololab', 'monika@monikabravo.com', 'I-DCCRT1X854YA'], ['David rodriguez', 'dfrodriguez827@gmail.com', 'I-M4XM1B9XFCNC'], ['JULIA DE LA HOZ', 'juliacdelahoz@hotmail.com', 'I-WNJ96MMJ0RDF'], ['DIEGO CASTRO', 'dacas86@gmail.com', 'I-ETUJVLFD1K27'], ['Julio Ricaurte', 'julioricaurtem@gmail.com', 'I-E1SA2D261Y5H'], ['Juan  Preciado', 'jppreciadom@gmail.com', 'I-4DXS587AVP59'], ['Harold Ivan Macias Alvarado', 'di.haroldmacias@gmail.com', 'I-DEJYUXF1KAXY'], ['STEVEN A BECERRA SALA', 'alejobersal22@gmail.com', 'I-RLTLFR4U7B8J'], ['Juan Mazier Castillo', 'juanmaz09@hotmail.com', 'I-D6B7GW994H47'], ['ivan andres machuca coronel', 'ivan-a-m@hotmail.com', 'I-08XU9R47F51Y'], ['sergio a garnica', 'angaritalina60@hotmail.com', 'I-SHVA6KC63BP3'], ['nelson correa valdes', 'nelsonco2010@gmail.com', 'I-HY67H6TLS8NB'], ['DAVID MARTINEZ SANCHEZ', 'estradausuga83@gmail.com', 'I-3CL0S1H4M0BL'], ['LUIS JULIO', 'lucajugo@hotmail.com', 'I-04LX76D6HGKM'], ['MAXIMO ROSEIGUEZ', 'cybermaxrogers@outlook.com', 'I-J5XXHHWHP28G'], ['MAXIMO ROSEIGUEZ', 'cybermaxrogers@outlook.com', 'I-KPCL41CJSNXP'], ['Agustin Zavala', 'guty_zavala@hotmail.com', 'I-YGTK87P1EG4H'], ['Sebastian Torres', 'torresduque.sebastian@gmail.com', 'I-NAC9KCMKEU2U'], ['Manuel Cometa hernandez', 'manuelfelipe.cometa@gmail.com', 'I-HPGKYE9F1WUK'], ['Luis Cuartas', 'lufecu_8@hotmail.com', 'I-FAMKEHKDXRBL'], ['Federico Gómez', 'kikogomez9@gmail.com', 'I-G2GVMH92PXEW'], ['cristina bustamante', 'cb699@cornell.edu', 'I-NBT4DWTCVN2N'], ['paola gaviria', 'paotaborda@hotmail.com', 'I-8MG03TG0MY6F'], ['Maria Cubillos', 'mjcm75@yahoo.com', 'I-1L648TJNLSXW'], ['Federico Gómez', 'kikogomez9@gmail.com', 'I-3AVRSF8S24FT'], ['Diana Romero', 'nanis0987@hotmail.com', 'I-D4A440VYSPJC'], ['hasbleidy carvajal', 'laura.perez08200287@icloud.com', 'I-R9KFLH7BC9EE'], ['RODRIGO MARTINEZ VALLEJO', 'michaellicol@gmail.com', 'I-5H0YLCY85008'], ['Valentina Guevara', 'valenmedina16@hotmail.com', 'I-MMND3EFRC6WD'], ['Esteban Moreno', 'esteban.moreno9309@gmail.com', 'I-KMM4C9TTMR80'], ['Maria Camacho', 'mariacamachoalbornoz@gmail.com', 'I-KN5UDM5W83E3'], ['Gustavo Zuñiga', 'Guszun@gmail.com', 'I-39BRXGBWWM0X'], ['Santiago Montoya', 'elsantimontoya@gmail.com', 'I-DF1S1CWRBSKC'], ['', 'juanpischannel@gmail.com', '1VW126127G9834719'], ['Producción Musical', 'drake.drees2017@hotmail.com', 'I-B9W8YKM7FC1S'], ['Hector Torres Garza', 'hectorromeo7_@hotmail.com', 'I-9DUDR0RT7BL2'], ['Cristian Montealegre', 'cristianmontealegre00@gmail.com', 'I-BXAWY69FA1C5'], ['Juan david patino', 'jucradle@gmail.com', 'I-95JU9DSUJKW6'], ['Juan Sanchez', 'jssn77@gmail.com', 'I-9370HYVXAGHS'], ['Diana Venegas', 'dvenegas689@gmail.com', 'I-CR8U971LB184'], ['nohora luna', 'nohrajuliana@yahoo.com', 'I-64LUTRG5DU8S'], ['Barreto Consulting Services, Inc.', 'gustavo@barreto.net', 'I-DS2WJ29MT4NL'], ['Andres Felipe Cuartas Zapata', 'fecuza0509@hotmail.com', 'I-GMG7T1ALYTLP'], ['Carlos Botero', 'carbog85@hotmail.com', 'I-J7AEAWDVA1UH'], ['MARGARITA MORENO', 'mmercado216@gmail.com', 'I-6GCSCG5JJV1N'], ['Patricia Rodriguez', 'nelsontocasuchil@hotmail.com', 'I-2N1SC7NWEFHF'], ['Johanna Denisi', 'johanna.denisi@gmail.com', 'I-469NKE986GT5'], ['Santiago Montoya', 'elsantimontoya@gmail.com', 'I-W1LF4628G5T0'], ['MARIO MONROY', 'mariomonroy1010@hotmail.com', 'I-M10GW0BA3A55'], ['michelly arango', 'muchelljia@hotmail.com', 'I-SLJRFT9US3AT'], ['Juan Tellez', 'jmtsarmiento16@gmail.com', 'I-K9EYMBR7KM8Y'], ['john m arias giraldo', 'Michaelarias283@gmail.com', 'I-HYSWGSE7SJPE'], ['FERNANDO PAREDES A', 'fernando.paredes@deprati.com.ec', 'I-1NDXBN8MG9Y1'], ['Ricardo Quijano', 'riquila74@gmail.com', 'I-5XL7188C1UWN'], ['Francisco Pacheco', 'yomopaco@live.com', 'I-0HUA9PEGB3AM'], ['LAURA SANCHEZ', 'Lsanc149@fiu.edu', 'I-DECWR12050D2'], ['Juan David Sanchez Torres', 'juandast92@gmail.con', 'I-C3WFJ7J7VBP8'], ['Maria A Jimenez', 'alejajimenezla@gmail.com', 'I-9FMVX61CAXL0'], ['DIANA HERNANDEZ', 'didimar08@yahoo.com', 'I-EWKM5F5RHYU2'], ['Jonathan arias', 'jonathanarias1437@gmail.com', 'I-30SJJDS0KC9Y'], ['Gabriel Zambrano', 'gzambranolindarte@gmail.com', 'I-YN68CSHBVED7'], ['Diana Oesch Ortiz', 'dianaoesch@me.com', 'I-C96FP66WF1D3'], ['liliana galvez', 'galvezliliana69@yahoo.com', 'I-M79AVJGBTWDH'], ['juan camilo mo', 'jusernam@hotmail.com', 'I-KK6KU6M6S5JH'], ['Elizabeth Hernandez', 'diegotarazona@hotmail.com', 'I-G6W40NKUSRBF'], ['geovanny cardenas', 'giovannycardenas8@gmail.com', 'I-APKV6XDXL98N'], ['marcela villa', 'juliotoral@gmail.com', 'I-N516THPB2AHS'], ['Miguel Nino', 'miguelsoccer10@gmail.com', 'I-2VE880E8SS7U'], ['', 'juanpischannel@gmail.com', '27B61343R0117862U'], ['jovanni charria', 'jovannycharria@gmail.com', 'I-9664P9S2U3AR'], ['Roger Cachope', 'roger_cachope@hotmail.com', 'I-SWXKWSCMBKDF'], ['ronald herazo', 'rherazo@me.com', 'I-B2YKJECD63HV'], ['Darwin Suarez', 'mauricino128@gmail.com', 'I-6M5TMYHD2M8T'], ['Jaqueline Marmolejo Garcia', 'jaquegarciasya@gmail.com', 'I-NC4GWKE7FCHL'], ['johanna Milena Avella Gutierrez', 'johanna.avella@hotmail.com', 'I-T5SC486XHMY3'], ['thomas estrada', 'testrada88@gmail.com', 'I-3FK0XKSCX2VR'], ['carlos andres ortiz f', 'Carlitos_caof@hotmail.com', 'I-LSNWGU9R06DY'], ['paula yepes', 'paula._2711@hotmail.com', 'I-JFWYN72MWX3S'], ['Valeria Gonzalez', 'valegonsan@hotmail.com', 'I-GVGKXUFJ2365'], ['Elkin Gutiérrez', 'respaldobonyic@gmail.com', 'I-6DPLGCUDCT7A'], ['Online Services', 'sofia.prada@gmail.com', 'I-16XX9CASGKVF'], ['Michael Andres Carrillo Pinzon', 'ma.carrillo2595@gmail.com', 'I-JM9645HEXS71'], ['Javier Mejia', 'javi.mejia16@gmail.com', 'I-4LR2FCH9FC7N'], ['Paola Margarita torres', 'margui_p@hotmail.com', 'I-40K7XU5G0LE8'], ['Daniela Garcia Ramirez', 'danigarcia2497@hotmail.com', 'I-1DP6E9UBN11P'], ['claudia marcela amaya gutierrez', 'maramaya2000@yahoo.com', 'I-4YJ07MWL1SE3'], ['Agustin Zavala', 'guty_zavala@hotmail.com', 'I-R9TX8RGV5280'], ['camilo castro', 'CAMILO.CASTRO002@MYMDC.NET', 'I-BJKJYRAN0422'], ['Santiago Granados', 'sgaiu318@gmail.com', 'I-H63H167UTDMJ'], ['Maria Alejandra Rojas Campuzano', 'ale.admon21@gmail.com', 'I-7Y1HKLS2GF7M'], ['Sara Pareja', 'sparejar12@gmail.com', 'I-SD6WCWDWLERX'], ['Online Pro', 'paulinalt@yahoo.com', 'I-VXY3VD58U6R5'], ['nelson castano', 'nelson36050@yahoo.com', 'I-MVEXP2TXG397'], ['Mateo Pino', 'mateopinoleon8@icloud.com', 'I-SV54JD59EVYA'], ['Jackeline Restrepo Buritica', 'jhi04102013@gmail.com', 'I-PEEMK1SGBVYP'], ['Ramiro Sandoval', 'sanramiro@gmail.com', 'I-4VWPGFC9LVL8'], ['Jhon Villegas', 'jhonjvillegas@hotmail.com', 'I-SN3HLBLSBJJW'], ['Alejandro Santa', 'chuletapublicitaria@gmail.com', 'I-6EFJNP5DGPGH'], ['Nizarindani Sopeña', 'nsopena@gmail.com', 'I-TCTYWG0BUBD1'], ['oscar castañeda', 'ocas3@hotmail.com', 'I-TMXSEFJAXPXD'], ['marisela alamo', 'mary_alamo80@hotmail.com', 'I-N4WCE070PFYY'], ['lina aragon', 'laragon@hellounlimited.com', 'I-698AHEWEBRU0'], ['Pablo Carvajal', 'pablocarvajalr@gmail.com', 'I-6US4TJG1SUHE'], ['Angelica Cardona Forero', 'mariang33@gmail.com', 'I-2M43722UH9WH'], ['LUIS F PEREZ-GONZALEZ', 'cachifodecali@gmail.com', 'I-72RYHVR3EM8L'], ['Natalia Jimenez', 'nataliagrado1@gmail.com', 'I-AGYRX0PC6WGN'], ['Nicolas Gutierrez', 'nicotutoguti@hotmail.com', 'I-5WJFL7F6NYKD'], ['Cristian Norena', 'Cristian.camilo.9@hotmail.com', 'I-Y3KT6ELD0EWX'], ['Maria Pinzon', 'mafepigo9@hotmail.com', 'I-H059YJCCG5NH'], ['LUZ C Andrade Maestre', 'yiyissfonseca@icloud.com', 'I-HPD28WPEHXP7'], ['JOHAM A PARRA CORREDOR', 'johanarley13@gmail.com', 'I-8EYDK9VW7A9J'], ['alejandra farfan', 'alejandra.farfan@alexandrebirman.com', 'I-DSP699F5V5S2'], ['Jose Perez', 'jojup27@hotmail.com', 'I-XL0BKDS3A8KJ'], ['Felipe Rodriguez', 'felipe.rodriguez@sb-canada.com', 'I-3XB4MFXMDR6L'], ['Mery Franco', 'lotusbodycontourspa@gmail.com', 'I-NBL14TF6LFPC'], ['Gustavo Bruce Reyes', 'gusp83@hotmail.com', 'I-FJR1MH1LLEJ6'], ['Juan leal', 'jleal@unilatina.edu', 'I-VVUVDKB5XPFT'], ['Gabriel Galindo', 'rdsupermarket7403@gmail.com', 'I-YF4CCEUAN72J'], ['Bejarano Bejarano', 'mvzleber@yahoo.com', 'I-553PETR5P59F'], ['luisa chaves', 'lfchaves82@gmail.con', 'I-2GNU1373YF6F'], ['sarita arbelaez jurado', 'sari11arbelaez@hotmail.com', 'I-7GUDMYLKY2G7'], ['Biviana Beltrán', 'vivivaleailin05@hotmail.com', 'I-826NJTS6HXMW'], ['Sandra Santamaria', 'carosantany@gmail.com', 'I-XU898W12N2JJ'], ['Carolina Fritman Ortiz', 'carolinafritman@hotmail.com', 'I-T31Y29NV1BMF'], ['Juan Oliveros', 'juanoliveros77@gmail.com', 'I-FJSDSE2L8ETJ'], ['marcela salazar', 'marcemigue28@gmail.com', 'I-0XTFPT6XK7CH'], ['Joaquin De vizcaya', 'fabricedevizcaya2015@gmail.com', 'I-6KT0BGX4WLMW'], ['Cristian Castaneda', 'cristiangarzon1242@gmail.com', 'I-EXABTMKFBBT8'], ['Diego Saray', 'pipssaray@hotmail.com', 'I-TX6AGP5D8NXS'], ['Gisel Moreno Zamora', 'ga.morenoz@uniandes.edu.co', 'I-MEBJHDYR6SJK'], ['Libaniel Velez', 'esteban10v@gmail.com', 'I-7MY7PWWCN1WA'], ['Faviola Mora', 'faviolamoragonzalez@gmail.com', 'I-P99N58SW8S5V'], ['Johan Aristizabal', 'aristizabaljohan@gmail.com', 'I-NDMBDSWJFNF7'], ['Camila Murcia', 'camilamurcia24@hotmail.com', 'I-UH189X2RJYM9'], ['carlos gonzalez', 'caangonzalez1@gmail.com', 'I-A9PDGSY887NJ'], ['luis castaneda', 'lahcaudio@gmail.com', 'I-PMAY9267YPW6'], ['OSCAR FERNANDEZ', 'oscarfernandez_vigo@yahoo.es', 'I-TKTB448RJ4HS'], ['Pablo Martinez', 'pablormr88@gmail.com', 'I-W20GGSF7FLR7'], ['Stephany Torres Lasso', 'stephanytorres92@gmail.com', 'I-743NVSBR66V3'], ['viviana Bermudez', 'garciabermudezsamusanti@gmail.com', 'I-UTFXF6LM2CN0'], ['cesar alzate', 'cesar.aao@hotmail.com', 'I-BKPCS5HXT1MK'], ['Mariant Mogollon', 'mariantwefer@gmail.com', 'I-2KXYC0XKX4WX'], ['Juan Beltran', 'milojose2014@gmail.com', 'I-HNYPKJTDHWYH'], ['Francisco Pinzon', 'fpinzon005@gmail.com', 'I-G7HX1RFYTN4B'], ['Miller Diaz', 'millerdiaz95@gmail.com', 'I-V1X5K2BB7CGY'], ['catterine valverde', 'sofiahenao183@gmail.com', 'I-1S2RPJ62CX33'], ['Cristian Cardona', 'davidcrgamez@gmail.com', 'I-LUFUKEU1DN5P'], ['Daniela Ramirez', 'daniela_ramirez_fuentes@hotmail.com', 'I-2GNBSBA36K0C'], ['WholeTech', 'jhongarcia9601@hotmail.com', 'I-J2LVFB042L85'], ['Sebastian Gonzalez', 'sebas1105@gmail.com', 'I-KVYKBLNA74RP'], ['camilo montoya', 'camilomontoya9124@gmail.com', 'I-33EP5RDH8E1X'], ['JUAN CARDENAS', 'jmcg94@hotmail.com', 'I-G6Y74XK4VC4N'], ['Alejandro Velasquez', 'alejandrovelech@gmail.com', 'I-WB7WXRXYX2Y9'], ['Nicolas Patiño machado', 'Nicolacito1994@hotmail.com', 'I-XTY5UPUU2THK'], ['Jorge Menjura', 'kmenjur09@hotmail.com', 'I-RMPPE3YMWMHA'], ['Melissa Díaz zambrano', 'mmdiaz1404@gmail.com', 'I-23R076BTF3H0'], ['Juanita Miranda', 'Juanitamiranda@hotmail.com', 'I-FN14HL780CHS'], ['Shakira Ortiz', 'shakirin13@hotmail.com', 'I-TK5GLL2HH1N7'], ['L Valencia Charry', 'lvc619@gmail.com', 'I-GK8YRJHJ8812'], ['Johanna Catalina Gómez González', 'jcatalina.gomezg@hotmail.com', 'I-9W9MVKV7M6GV'], ['Hanssell perez', 'Hanssellpere@icloud.com', 'I-E91XMY1SBV45'], ['Alejandro Ramirez', 'skyrompiendo@icloud.com', 'I-1Y0PUMM5WSR9'], ['Maurice Jesus cova', 'mariannadurans@icloud.com', 'I-EN0FFLAA7S8R'], ['Ana Ramirez', 'anitaramirezpulido@gmail.com', 'I-A5PJU8GM3C9V'], ['D Nino Beltran', 'danielcamilo114@gmail.com', 'I-4FJ08YWGU6DV'], ['Andres Felipe Alvarez', 'anfealvarez85@gmail.com', 'I-HASAKSYWRNDL'], ['maria Quintero', 'cesar.rivadeneira@hotmail.com', 'I-E908J8BWU69S'], ['LUCAS ADRIAN ESTRADA USUGA', 'estradausuga83@gmail.com', 'I-RN5LY93AHFW1'], ['Fabian Galvis', 'elosmeza@gmail.com', 'I-1ACMP2626AA5'], ['Camilo Velez', 'velezcamilo@hotmail.com', 'I-TVVG549M681T'], ['Luisa Laguado', 'luisalaguado3@hotmail.com', 'I-28695D435UKJ'], ['Mateo Fabrega', 'mfabrega44@gmail.com', 'I-4EY3B8D47G9P'], ['Sara Martinez', 'Saralm@yahoo.com', 'I-S6L7XU6VS59A'], ['Jenny Castro', 'jencastro14@gmail.com', 'I-R9YS3XDNBFF6'], ['wilman ospina', 'wilman.e.ospina@gmail.com', 'I-C3WAJBJ7VBP8'], ['Juan Alonso', 'alonsojuanmanuel50@gmail.com', 'I-GPUSUYJ64L6X'], ['Gioliano Gonzalez', 'Gioliano32@gmail.com', 'I-H1PCF0D5V5J1'], ['Juan sebastian Medina lopez', 'sebastianlopez_21@hotmail.com', 'I-WEX9N7A2VK3N'], ['edgar caicedo', 'edgarc.gras@gmail.com', 'I-DECTRM2050D2'], ['Oscar Castillo', 'osi_castillo@hotmail.com', 'I-Y3FB764CKUPU'], ['LEIDY CUBILLOS', 'didi1396cubicubi@gmail.com', 'I-WJMALPP57FS2'], ['Maria Montana', 'marii_0510@hotmail.com', 'I-LAJAKDA0L0DW'], ['Isleidy Baldwin', 'elite.embroidery@yahoo.com', 'I-T5LHDFW4PF3A'], ['Luz Estupinan Ayala', 'tobog89@gmail.com', 'I-8JXM41TV1JFP'], ['Fredy Correa', 'frecors8@gmail.com', 'I-B2YGJ2CD63HV'], ['Alejandro Zapata Guisao', 'alejandrozg03@hotmail.com', 'I-4VR0PKG1R89R'], ['JAIRO BUITRAGO CHAVES', 'alexanderbuitragoch@gmail.com', 'I-5DS8ERW9EDF4'], ['Juan Villarreal', 'nbici.ec@gmail.com', 'I-0SHW5A2KU6TT'], ['ricardo toledo', 'ricardo.toledo.enriquez@gmail.com', 'I-CJHKHYUCRX58'], ['simon garcia', 'simon0695@hotmail.com', 'I-FFVP1YEANACH'], ['alex calderon', 'Alexcalso@hotmail.com', 'I-KHPTMM2V7WTB'], ['Cindy Garcia Ortiz', 'garci_cindy77@hotmail.com', 'I-AYVVL2AAPT91'], ['Elieth Melendez', 'eliethk1706@hotmail.com', 'I-HX1FLH53N1XM'], ['gustavo adolfo rodriguez osorio', 'garo_7310@hotmail.com', 'I-YX8MH91G70WU'], ['JUAN ANDRES RODRIGUEZ MARIN', 'andresmoon@hotmail.com', 'I-6DPPG8UDCT7A'], ['Maria Maldonado', 'mariaxmg18@gmail.com', 'I-MG6VYNTYE9T4'], ['Angelica Rincon', 'diaadia0729@gmail.com', 'I-KE778LBKCSN2'], ['Oscar Lora', 'donloraoskr1@gmail.com', 'I-WJF222BEWAUV'], ['NATALIA LOPEZ GOMEZ', 'natalialopezgomez1@gmail.com', 'I-MVE4N8YFXCBR'], ['Martha Gallego', 'marthaegallego@yahoo.com', 'I-UD2818B9SR1E'], ['ana candela alzate', 'mylenitacandela@hotmail.com', 'I-NFUAAG80YS3U'], ['Lisseth Tatiana Camacho Bueno', 'tatiscamacho7@gmail.com', 'I-9G4L3VATFKUU'], ['efrain rivera', 'efrariverac@gmail.com', 'I-YNDCYLFWG580'], ['oskar torrealba', 'golfgrips4sale@gmail.com', 'I-DP6KBYN5TRT1'], ['AES AUTOSPORTS', 'parcealexskate@hotmail.com', 'I-S03N6PNP6EMK'], ['Carlos Rodríguez', 'mauriciorodriguezsalcedo@gmail.com', 'I-35W41YE1A2NC'], ['Geraldin lopez', 'geraldin.lpz.villamizar@gmail.com', 'I-XGFU4C1U9L8A'], ['Harol Gomez', 'harolgomez_211@hotmail.com', 'I-U7B0E439TD5R'], ['Lorena Sanchez', 'Loryayi@hotmail.com', 'I-Y6UF9VJJBMXC'], ['Jorge Roldan', 'kikeroldan@hotmail.com', 'I-E9YDMB13LCH3'], ['Lina Castano', 'linacas1992@gmail.com', 'I-K2HK049NPED7'], ['Rodolfo Del Pozo', 'erikapri266@gmail.com', 'I-05PHU7A8U2PA'], ['Hector Gonzalez', 'hfgonzalez221@gmail.com', 'I-3XB3MBXMDR6L'], ['ELIZABETH GALINDO', 'ejgalind@hotmail.com', 'I-BDU32FT7PW8F'], ['Juan Sebastián Oviedo Arévalo', 'juanso2312@gmail.com', 'I-SB4BKWEY6HR9'], ['nelson diaz', 'nightday060@gmail.com', 'I-7MLV84JEN08V'], ['Andrea Ochoa', 'magan8a@hotmail.com', 'I-5C3TF6BXM498'], ['Alejandro Osorio', 'alejoso300@gmail.com', 'I-9D4GTA52TVVW'], ['Nestor Martinez', 'nestonine@gmail.com', 'I-KFMVXRFSB2P9'], ['Tania Marcela Vega', 'marcelavega64@gmail.com', 'I-3DL5FH43ANRY'], ['alejandro ariza', 'alejandroariza2210@hotmail.com', 'I-9PCL62EGBVWU'], ['Sergio Gomez', 'contactodemian@gmail.com', 'I-1GHGX42CE3K2'], ['Laura Demisie', 'laurademisie12@gmail.com', 'I-GBRD0VA4M0L6'], ['Abdon Castro', 'abdon.castro@gmail.com', 'I-ASBWAACTS71D'], ['David González', 'df.gonzalez2070@uniandes.edu.co', 'I-8AXM4JCT727G'], ['Diana Vaca Delgado', 'dianiv26@gmail.com', 'I-HPGWY69F1WUK'], ['', 'juanpischannel@gmail.com', '2AN031191N753204T'], ['Diego Felipe Contecha Porras', 'contecha_137@icloud.com', 'I-1AXEALH4FUVP'], ['PayPal', 'juanpischannel@gmail.com', '1VW126127G9834719'], ['', '', '1VW126127G9834719'], ['Johan Sebastián Ramirez Falla', 'sebastianfalla145@gmail.com', 'I-NDM6DMWJFNF7'], ['juan arevalo', 'juank87321@gmail.com', 'I-XPKFC3DRWKEY'], ['YOAN MANUEL ESPINOSA IDARREGA', 'yoanma_17@hotmail.com', 'I-U1VM3DX5MKNE'], ['Diego Julian  Guzman Campo', 'alkimiaxx@icloud.com', 'I-Y4LCXSX7B29V'], ['pablo tovar', 'pablotovar1995@gmail.com', 'I-C2SV7CJNMS0H'], ['daniel prieto', 'danielprietoc@hotmail.com', 'I-AT5LFHS67U1M'], ['Andres Pinto de la Ossa', 'adidda@gmail.com', 'I-X56K9P5FA7A5'], ['Juan Camilo Ortiz Gomez', 'camilo@imersys.com', 'I-28BTT3HP989H'], ['Catalina neira morales', 'kta218@gmail.com', 'I-HHTXYBNPWLRU'], ['Andres Felipe Granda Franco', 'andresfgranda@live.com', 'I-TMJ6MYSF4VA7'], ['Camilo Gutierrez', 'camilogutierrez950111@hotmail.com', 'I-2KX0CVXKX4WX'], ['Lina Trujillo', 'linatrugue@gmail.com', 'I-7GWMGTCECY19'], ['Eliana Ramirez', 'juanpabedoya@gmail.com', 'I-LUFNJAU1DN5P'], ['Felipe Forero Ardila', 'fforero02@gmail.com', 'I-2H9AVHG1HG36'], ['Kadyr Florez', 'kaliche22@hotmail.com', 'I-APPY0MTU7FLN'], ['Lissette Vargas', 'liseth202@gmail.com', 'I-JCSFR18AKSBR'], ['Yenny Miranda', 'j.mirandamora@gmail.com', 'I-KB18ARMW6GWF'], ['Fabio A. Garcia Lozada', 'falex3105@gmail.com', 'I-W9WGDCW7MK0W'], ['Juan Camilo Acosta Montenegro', 'juankmilo02@hotmail.com', 'I-0RT9VVS2NBGF'], ['julian londono', 'julilond8@gmail.com', 'I-0VX2KS2HD2VN'], ['raul gutierrez', 'raulguti491@gmail.com', 'I-30W81E8UBAD1'], ['Juan Gamez Valbuena', 'andres_gamez1983@me.com', 'I-48X3DHSWLCYJ'], ['Justin Potter', 'justin.potter55@gmail.com', 'I-BRB04DH57TWX'], ['CAMILO PIESCHACON GODOY', 'camilo.pieschacon@gmail.com', 'I-4VR5PPG1R89R'], ['Isaac Montero solorzano', 'isaacmonterosolorzano@gmail.com', 'I-J8W9868YUCB3'], ['Juanita Constantin Jaramillo', 'constantinjaramillo@gmail.com', 'I-P6XWVBWRNX8B'], ['vanessa Medina', 'matiasteamo030213@gmail.com', 'I-XVLRE4T36D72'], ['Tienda de ropa', 'orlantovar@gmail.com', 'I-K9UPLD8KAU00'], ['giraldo david', 'bocampo1996@hotmail.com', 'I-85SSTR9TYPLG'], ['KATERINE PIERRE-LOUIS', 'gkaty474@gmail.com', 'I-AKXCNER2HF8B'], ['Martin Alonso Aguirre', 'tin1985@gmail.com', 'I-D3EWA30DJ3M9'], ['Jose Alejandro Fuquen Galindo', 'afuquen@live.com', 'I-YH8P7TJNCCUP'], ['Martin Velez', 'martancho@gmail.com', 'I-J6RLBRVCGMF1'], ['Juan Gaviria', 'j8gavi23@gmail.com', 'I-M1LJND9GURDD'], ['Antonio Hurtado Suarez', 'colombiano_95@hotmail.com', 'I-K00EG358PMT9'], ['Fernando Andara', 'andara.fernando@gmail.com', 'I-C07LBUX8X55Y'], ['Andres Jaramillo', 'pipejaca@hotmail.com', 'I-CH8P51FJEGPT'], ['JULIAN ANDRES NAVARRO LOPEZ', 'juliannavarro.8606@gmail.com', 'I-3N4EL8GD6MA6'], ['senora super sweep', 'senorasupersweep@gmail.com', 'I-35R83T2GWFUX'], ['Luis Enrrique Chiriboga Otero', 'luis.e.chiriboga@gmail.com', 'I-3TJAWSP0F9AU'], ['David Moreno Moreno', 'lsramonesls@gmail.com', 'I-1PEEAF9NPT4J'], ['Luisa natalia ojeda', 'luisanataliazo@gmail.com', 'I-2SL953WPBYDD'], ['William Andrés Álvarez Garzón', 'willyalvarez91@gmail.com', 'I-LHG6VWDNNB39'], ['Gustavo Marín', 'gustavomarintorres@gmail.com', 'I-KJVDDHJRF5RJ'], ['nicolas gomez', 'mdolibrengb@hotmail.com', 'I-5MYTMW9SWFE8'], ['Daniel Gomez Potdevin', 'dangou91@gmail.com', 'I-CSBC7057LUJ4'], ['Daniel Gomez B', 'danibasket1@gmail.com', 'I-HLAP4GMC0K2J'], ['andrea barrera', 'andreabarrera2@hotmail.com', 'I-UYRD3S4TS3LD'], ['Angelica Bermudez', 'conmigoessuficient@hotmail.com', 'I-HBMJTDVECNRE'], ['David Le', 'david_le1992@hotmail.com', 'I-Y6UA90JJBMXC'], ['Juan Arias', 'ariasjd91@gmail.com', 'I-72UG47099EKG'], ['Diana Torres', 'dianis7070@hotmail.com', 'I-X1U80FJ8GCEB'], ['Jonatan andres botero echeverri', 'jbotechev@protonmail.com', 'I-1KAF2WU6PRY0'], ['Oscar Villegas Acevedo', 'mr.oscar.villegas@gmail.com', 'I-FCJY11023BU3'], ['Steve Penagos', 'danny_penagos@hotmail.com', 'I-S78FH3HLWVWJ'], ['Hadelly Rojas', 'Hadellyrc@hotmail.com', 'I-5U8S4HLL4JDA'], ['David Valencia Cuervo', 'davacocinero@gmail.com', 'I-G2H3FMH0KXAR'], ['oscar Poveda', 'opoveda230@gmail.com', 'I-8S4BELE1YW59'], ['Daniel Ricardo', 'danielricardo45@hotmail.com', 'I-EEFNW284ENDG'], ['Andres Infante', 'andresinfa80@gmail.com', 'I-8CEG0D3MD3TK'], ['Camilo Calderon Pokorny', 'camilocalderon@outlook.com', 'I-HPL3GMRW22U9'], ['Pedro Gahyda', 'gahydanicolas@gmail.com', 'I-EH0T1RPREGEY'], ['Claudia Gonzalez', 'clauditagr123@gmail.com', 'I-K3TKN5J7DJCM'], ['Luciana Villa Castrillon', 'lujuana11@hotmail.com', 'I-HUATAGNPX3UC'], ['juan rueda castellanos', 'juanruedac28@gmail.com', 'I-DV0BXLMTH47P'], ['Melba Naranjo', 'naranjomelba12@gmail.com', 'I-YBMUWX7KXRJ1'], ['juan rangel', 'jerj1505@hotmail.com', 'I-TKSSNNAK5CKK'], ['Cristóbal Durán Bello', 'cdbcristobal1995@gmail.com', 'I-3DHH0EDHSJ8G'], ['carolina Gamez', 'carigamez@hotmail.com', 'I-AG1J8SRTCJ5U'], ['Tienda de ropa', 'orlantovar@gmail.com', 'I-25GFGSTB89MT'], ['Pierre Hurel', 'gonroga.infos@gmail.com', 'I-XKSGUHAX0YKW'], ['jhon m toro gaviria', 'jhotoga143@hotmail.com', 'I-25KKSLPPEWME'], ['Leonardo MIKAN', 'leitomikan@gmail.com', 'I-0WVSPLELRL5M'], ['Walter Rodríguez', 'choco.mc@gmail.com', 'I-72NJYJMERP34'], ['エステバン ウリベ', 'eikantochigi7@gmail.com', 'I-H7TLBB8D3K42'], ['mariana bogota', 'bogotamariana@gmail.com', 'I-ME52CC21XLDU'], ['juan samper', 'jean_samper@hotmail.com', 'I-BW4JH6LD1N1G'], ['Juan Martinez', 'juank_20_32@hotmail.com', 'I-STXM1GC0N5ME'], ['Juan Gonzalez', 'juanjogo77@yahoo.com', 'I-F4XG7GNT2SVB'], ['christian burgos luna', 'christianb@mail.tau.ac.il', 'I-8K6PTREDXP9H'], ['José Romero', 'jaromero023@gmail.com', 'I-LD5DLTSV4EHH'], ['Luz Adriana Tovar Mantilla', 'adritovar77@yahoo.es', 'I-HD1U5KH1UCY0'], ['alejandro villarreal', 'alejovilla25@gmail.com', 'I-FH09G67X9EYF'], ['laura Parrado', 'laurav1212@hotmail.com', 'I-JFRW6XSMPGKN'], ['Diana Bermúdez', 'dianabermudez1582@gmail.com', 'I-8MG73EG0MY6F'], ['Catalina Fajardo', 'catafv@gmail.com', 'I-YNH0H0R6WH0K'], ['4yourday', 'diegoadj@4yourday.com', 'I-ML3D8U9NTDM8'], ['Carlos Dardon', 'dardon0128@gmail.com', 'I-F36L38M2GW5C'], ['Francisco de Paula Ruiz Escamilla', 'fruizescamilla@gmail.com', 'I-HY5BRMB6BH8U'], ['Fernando Ferrel Ballestas', 'frferrelb@unal.edu.co', 'I-YT4NGHMJMB79'], ['Laura Betancourt', 'laurabetancourtlopez@gmail.com', 'I-30FDLT59HXRW'], ['Natalia Perez', 'nataliaperez@berkeley.edu', 'I-WW20EN27BS46'], ['Katherine Bedoya', 'katherine-0304@hotmail.com', 'I-RKWXAMM0LWL8'], ['Christian Berner', 'blackfire7@gmx.de', 'I-9H0D12AY942P'], ['Jaime Guio', 'cali-juan-12@hotmail.com', 'I-Y1V4UBMVYXHT'], ['Daniel Gonzalez', 'dalogo92@gmail.com', 'I-WNWKP3CJ8R4N'], ['Milton Lopera', 'piccoto.2323@gmail.com', 'I-ANNYYSAWUR4T'], ['Luisa Fernanda Ramírez García', '23240luisa@gmail.com', 'I-1FU6E3NR3BFS'], ['Jaime Cruz', 'jrodrigocruz26@icloud.com', 'I-2S74Y4CWY9VF'], ['Valentina Henao', 'villahenaov@gmail.com', 'I-9N2T2XCUW3XJ'], ['Andres Martin', 'andresska188@gmail.com', 'I-TMRY9EPVBX2C'], ['alejandro pennha', 'jpennha@yahoo.com', 'I-BFBWHYTVJVF1'], ['Diego Velez', 'bleox96@gmail.com', 'I-1RF7P5SVSCF4'], ['M Gutierrez Lizaraz', 'manuela-g1703@hotmail.com', 'I-V4A5RWMVEJFY'], ['Vilma Schafer', 'schafer_alejandra@hotmail.com', 'I-46RTHDEPBM4D'], ['Cristian sanchez', 'cristianito890523@gmail.com', 'I-60TY0UK0FFH2'], ['francisco valencia', 'franciscojosevalencia@hotmail.com', 'I-FXLALPDGH63M'], ['Ximena Castellanos', 'elveximena@gmail.com', 'I-7YEL8965JUVM'], ['Yorlandy Grajales Pantoja', 'yorlandygp@gmail.com', 'I-J03S1MTNWLMF'], ['David Manrique Moreno', 'davidlmanriquem@gmail.com', 'I-SN2LPSS0FNHY'], ['Jaime Lopez', 'jaime.lopez.nates@gmail.com', 'I-V3R3JEL346PT'], ['Paula Lozano', 'palozano.plg@gmail.com', 'I-YXAPTER9WDKE'], ['Yuly Johanna Valenzuela', 'yuly_val@hotmail.com', 'I-5DWT1H6LD38P'], ['Juan Rodriguez', 'phil.rod1124@gmail.com', 'I-5NYEN8JB6DCL'], ['Andres Otero Forero', 'andres.otero@gmail.com', 'I-UW1Y94L35PFG'], ['Lina Sandelin', 'linamaria.sandelin@gmail.com', 'I-G7ABGK81S2JY'], ['Andrea Gonzalez', 'dreadelpi@hotmail.com', 'I-3KW5SWK6N6U6'], ['Irina Gonzalez Sandoval', 'irina85@gmail.com', 'I-BKWD8T25V75A'], ['Brahian Andres Rodriguez Montano', 'andres_style617@hotmail.com', 'I-6GCWC05JJV1N'], ['Roy Ruiz', 'royruiz2013@gmail.com', 'I-GWXMHKJDMEEV'], ['Juliana Daza S', 'julianadazaserrano@gmail.com', 'I-1ACAPX626AA5'], ['jorge oronda', 'pipeandres21@hotmail.com', 'I-H24P98GHA6RU'], ['Maysully Carlsson', 'mayorcar3@hotmail.com', 'I-CF389FA3BAV9'], ['Christian Salazar', 'cdsalazars@gmail.com', 'I-VYRE08303UW4'], ['Angelica Diaz Viancha', 'libgrad@hotmail.com', 'I-6BXLAEWUYRWU'], ['Maria José Vargas Sáenz', 'majovs@gmail.com', 'I-LUJVLJ5J49XY'], ['Angelica Diaz Viancha', 'libgrad@hotmail.com', 'I-82BXH1J76KCP'], ['Mario A Maz C', 'ing.marioandresmaz@gmail.com', 'I-CK0LPBHH9CUA'], ['Idelfonso Bolaños', 'poncho.con@gmail.com', 'I-Y469P5F1MWJR'], ['Estefania Mancera Caro', 'stefaniamc38@gmail.com', 'I-UTA16548RWHX'], ['Luis Johnson', 'luisjohnson@me.com', 'I-74VUB9JW82TT'], ['Laura Cano Salazar', 'lauraca330@hotmail.com', 'I-NURH3SB4KV1H'], ['PayPal', 'juanpischannel@gmail.com', '51X633195K086072C'], ['LAURA MOLINA', 'lala2003@hotmail.com', 'I-MLRHMG9P1NH0'], ['Joel Velasquez', 'joelnyc88@hotmail.com', 'I-SRVURPP9C4JX'], ['Camilo Gil', 'juankacipa@gmail.com', 'I-8R75HUVHJX27'], ['edwin erazo', 'edwingeo04@gmail.com', 'I-YUSNBPPFYTTV'], ['Zadira Rivera Castro', 'zadirarivera@gmail.com', 'I-ACEM498XXG8S'], ['cv carreno Vanegas', 'vivi0287@hotmail.com', 'I-SULAF6UCG2H4'], ['Francisco Javier Garcia Peinado', 'johanagomezr@outlook.com', 'I-TGNG1VGX2L0V'], ['Natasha Gaviria Iglesias', 'natashahuguette@gmail.com', 'I-HLKS5DV3Y2X2'], ['Valeria Castillo Ferrerosa', 'vcastilloferrerosa@gmail.com', 'I-066475RPKDY0'], ['Diana Florez', 'dcflorezn@gmail.com', 'I-97RCJSJNBSLV'], ['Cecilia Vargas', 'ys.julian@hotmail.com', 'I-4HHTMCEJ6JAV'], ['JORGE MARTINEZ', 'jorgeenriquemartinez.martinez@gmail.com', 'I-NCWE0HRH91EY'], ['Ivonne Echevarría', 'imarie_21@hotmail.com', 'I-1GDU90356L74'], ['yorssy moreno', 'yorssy_m@hotmail.com', 'I-MWC7BHE34PYE'], ['francisco javier hernandez villegas', 'francolumbo@hotmail.com', 'I-VV8XSK0DA47J'], ['Sofia Milena Salamanca Granados', 'sofilena@gmail.com', 'I-8R1254BYB657'], ['Oscar Valderrama Barreto', 'oscarvalderramab@hotmail.com', 'I-CGNC8LNL6TYY'], ['David Belalcazar', 'david.mx5007@gmail.com', 'I-BKFXK0HMFU38'], ['Zamy Andrea Palacio', 'zamy_andrea.palacio@chello.at', 'I-EBTNKBLJ1HTL'], ['Philippe Ortega', 'philippe.ortega91@gmail.com', 'I-EAAWNBM2999T'], ['Paula Andrea Diaz Caicedo', 'paula1107@outlook.de', 'I-115VREBCDPY4'], ['sandra chaquea', 'tatianagris1116@gmail.com', 'I-1YEXRH040ERE'], ['Yhon eduar García silva', 'jhonratt@gmail.com', 'I-2KFW62225GVV'], ['Jorge Aristizabal Osorio', 'licjorge4@hotmail.com', 'I-C86ENDPSVHG9'], ['Stefania Jubiz', 'sjubizmilanes@gmail.com', 'I-G7H90XK9ATL0'], ['Andrés Felipe Gómez Casas', 'af.gomezcasas@gmail.com', 'I-WT0G1HPXBPR1'], ['Manuela Arango Salamanca', 'manuela1608_@hotmail.com', 'I-K4WJAVHRDVSV'], ['Naudylver Martinez Rodriguez', 'martinez.naudylver@gmail.com', 'I-CNCYUXG110D8'], ['Gabriel Mayorga', 'gabriel.mayorga@msn.com', 'I-0XAH0AW3GDVE'], ['Juan Rosero', 'juan.rosero@icloud.com', 'I-H6B2CME0H5KR'], ['Monica Baquero', 'mmbaquero@live.com', 'I-ENCX4SBC3RP4'], ['JORGE BARON', 'carpediemjabm@yahoo.com', 'I-DFHHTM334TLF'], ['Bibiana Raba Mora', 'bibiramo95@gmail.com', 'I-9A68TA227J7F'], ['Federico Munoz', 'Munozfedericob@gmail.com', 'I-55WX02V3NEBN'], ['Jonathan Cano Villa', 'joncv1989@gmail.com', 'I-SV5YJS56EVYA'], ['Marian Derly Morales Otero', 'mariand.morales@gmail.com', 'I-0HKVGL1MCJC1'], ['Daniel Montero', 'colombiandan@gmail.com', 'I-YD2LE9UF8JFY'], ['camilo garcia', 'camilogarcia-20@hotmail.com', 'I-NE2MYVBYAJ0T'], ['Jose Espinosa', 'el.santorugby@gmail.com', 'I-PFL9F6TG4H4S'], ['Paula Jaramillo', 'paulajillo@gmail.com', 'I-4NELEX9217AG'], ['Edgar Suárez Guarnizo', 'suarez.edgar@javeriana.edu.co', 'I-WG4N5GP8HM8J'], ['Muftiyah Bustillo', 'muftiyahb@hotmail.com', 'I-7691K8Y6TFB2'], ['Katherine Vallejo', 'jeffgobo93@gmail.com', 'I-XLGN4RTU6F55'], ['EDNA ROCIO MENDEZ CUELLAR', 'edna_mendez18@hotmail.com', 'I-HJPV53KBM2TA'], ['Johana Oliveros', 'johanao@gmail.com', 'I-9WL10GLFUEJD'], ['Juan Martin Bernal', 'juanmabb8@gmail.com', 'I-98S7UMAR2W02'], ['Gerardo ramos', 'grbarco29@gmail.com', 'I-C2SG7VJSMS0H'], ['Raul Samper', 'raeljeda@gmail.com', 'I-MBAKYKCKAAXB'], ['Luis eduardo olano manzano', 'eduardoolano@gmail.com', 'I-YXCLAB0U06KT'], ['Jean Paul correa Rojas', 'jeanpaul51211@gmail.com', 'I-CE9Y1DRS10P3'], ['Catalina Raba Mora', 'cataraba93@gmail.com', 'I-FG84EVDHWHXM'], ['luis olarte', 'basskill_0582@hotmail.com', 'I-R5MVMMJAV4LC'], ['g caldas lara', 'caldasgabriela2@gmail.com', 'I-6GC7CR5MJV1N'], ['Nicolas Pinzon', 'nipi_98@hotmail.com', 'I-2PM47VSRNDR3'], ['María Fernanda Cardona', 'mafemadrid@live.com', 'I-78PSKKCS9TF6'], ['Yamile Casasbuenas cabezas', 'yicasasbuenasc@unal.edu.co', 'I-7FFFE7CJ3N34'], ['Erick Meneses', 'erickmeneses@gmail.com', 'I-6XP2DU4Y8UBC'], ['Harvic Mena Martinez', 'harvic10@hotmail.com', 'I-XSWBE90C72CF'], ['Bartomeu Mazuelas Sanz', 'bartsanz@gmail.com', 'I-E27TFPP65UXD'], ['Mazdi LLC', 'director.creativo@mazdiseno.com', 'I-TH4M3L5U2TEX'], ['Pamela Cristina Espinosa Saico', 'pamelaespinosasaico@gmail.com', 'I-J53K1A50LUKV'], ['sonia Ruiz', 'howyg361976@gmail.com', 'I-H4T88CK4D1SA'], ['Pamela Cristina Espinosa Saico', 'pamelaespinosasaico@gmail.com', 'I-KTAAFGT6J7H4'], ['Kevin Tovar', 'kevin.andres.09@hotmail.com', 'I-G9W1HDY2CC22'], ['Felix Angulo', 'desocupe09@hotmail.com', 'I-1PETBY9SPT4J'], ['Gabriel Gutierrez', 'gabrielgutierrezpa@gmail.com', 'I-HYRS53MS2GSK'], ['Alejandro Guerrero', 'alejandro.g150515@gmail.com', 'I-K8DNAMUB38NL'], ['Ana Suarez', 'marrerojunior@hotmail.com', 'I-FCC4ATLTRVH0'], ['John Jairo Rozo leon', 'jhonrozo14@gmail.com', 'I-EMJK6TH48W74'], ['Andres Garzon', 'andresgm06@outlook.com', 'I-RU2WC1F73EJ0'], ['Diego Cotrino', 'diegom.cotrino@hotmail.com', 'I-125YK6CX3W68'], ['Andres Valencia', 'volkspapo@icloud.com', 'I-UYR9394WS3LD'], ['Monica Andrea Serna', 'moanser81@gmail.com', 'I-31R7ATR0K3NG'], ['Ever Daniel Zulet Lopez', 'everdaniel25@icloud.com', 'I-HJ18MSB8GDNM'], ['Ana Santos', 'anamsantosv@gmail.com', 'I-LUHGCXSS6UH2'], ['Martha Hernandez', 'mh0728@hotmail.com', 'I-21S09GRMU77S'], ['Nilsa Herrera', 'indirita0903@gmail.com', 'I-1GHYXG2BE3K2'], ['Jesus Ortiz Sandoval', 'jesus.ortiz@usm.cl', 'I-P9984GST8S5V'], ['Arthur MATHIS', 'bibi.vega@hotmail.es', 'I-FEH0XR7X8HKP'], ['johanna portugal', 'johanna_portugal@hotmail.com', 'I-RN5U2FRRRM4E'], ['camilo jaramillo', 'kamillo07@aol.com', 'I-BSNBEA5VXH8X'], ['FERNANDO MARTINEZ', 'mariac.mogollon@yahoo.com', 'I-D6K1MKU905KP'], ['Eduardo Camargo', 'ecamargorealtor@gmail.com', 'I-7GNNBKM787EW'], ['Andres Felipe Cruz Roys', 'andrescruzms1998@hotmail.com', 'I-F4975M45BMSL'], ['montanez felipe', 'j.f.montanez@gmail.com', 'I-WWXXXTS5WJG6'], ['Maria Jose', 'majo_gonzalez_97@hotmail.com', 'I-YUVM85FP1F96'], ['Fernanda Rodriguez', 'spotifysebas22@gmail.com', 'I-C7BFF5XWHVB7'], ['Carolina Niño Celis', 'carito_nino@hotmail.com', 'I-TY9JD4D0LWKU'], ['Luis Giraldo', 'agiraldo410@gmail.com', 'I-FD7FYLARC9TA'], ['Cristian Alzate Henao', 'cristian282898@hotmail.com', 'I-4EYMBGD37G9P'], ['edwin florez', 'edwintrinoflorez@gmail.com', 'I-LYDU7YWA52J0'], ['Juanita Blanco', 'juanitablanco87@gmail.com', 'I-K5V1GL8TN69L'], ['María Jaramillo', 'maelenajaramillor@yahoo.com', 'I-98EVSM1WBVBJ'], ['sebastian villarrial', 'sebastianzarat@hotmail.com', 'I-MLDFLKTVCE8C'], ['Valeria Obando', 'vale_vale1990@hotmail.com', 'I-PL3B16G7GTTM'], ['David Morales', 'davidjdm12@hotmail.com', 'I-V9Y8RA6KWKL8'], ['Oswaldo Barrada', 'felipemarbar030894@gmail.com', 'I-MJGBYHVVF0RL'], ['MÓNICA PACHÓN', 'monipachon19@gmail.com', 'I-62WFC3S5NLEU'], ['philipp schmitt', 'philippschmittu@gmail.com', 'I-XJ1XVUKY1YRX'], ['Magda Ocasiones', 'magdaodel@gmail.com', 'I-PYYMEYK5L3B4'], ['Pablo Andres Moreno Buitrago', 'danielarp1907@gmail.com', 'I-1V4D8GR4Y5MA'], ['Carlos Daniel Sarmiento Tarazona', 'copeman_27@hotmail.com', 'I-8WYVVK43BLX7'], ['joshua lopez', 'joshlt135@hormail.com', 'I-BR62T1KBB0SH'], ['Daniel Gaitan', 'danfer.dg@gmail.com', 'I-REG7H6AANGEU'], ['Juan  Quintero', 'sebitasq@gmail.com', 'I-RU2RC5F73EJ0'], ['luis sandoval', 'luis.sandoval2790@gmail.com', 'I-S0386YNR6EMK'], ['Michelle L. David', 'michellelorenaflorez@gmail.com', 'I-VDEA5GHGJWK9'], ['Nathalia Chaparro', 'natiichaparro@hotmail.com', 'I-USVUAVE8LD1V'], ['John Gonzalez', 'johnfgr35@gmail.com', 'I-E274FFP65UXD'], ['William Rojas', 'williamrojas1927@hotmail.com', 'I-S0NL0XR44NBB'], ['Oscar Soriano', 'oscar-soriano99@hotmail.com', 'I-FT4JVUU4U6JS'], ['Juliana Keyes', 'julianaokeyes@gmail.com', 'I-4AR8UBY69G5G'], ['Mario Barbosa', 'marioabarbosam@gmail.com', 'I-BF6H6VXSHN7S'], ['CESAR A SANTOS RAMIREZ', 'rasec_1113@hotmail.com', 'I-X5619Y5GA7A5'], ['julian arriaga', 'vicente0105@icloud.com', 'I-L245MLGJE411'], ['fernando gonzlez', 'fgt2722@hotmail.com', 'I-HV92C1PJ8FT8'], ['Carolina Garcia', 'cgs5298@hotmail.com', 'I-KXECASK7N98D'], ['Wilmer Castillo', 'wilmer.castillo@hotmail.com', 'I-Y35TVA8W9R5M'], ['Leonardo Anselmi', 'lrubioan@yahoo.com', 'I-E1SMHE291Y5H'], ['Daniel Noriega lopez', 'daninoriega51@gmail.com', 'I-WW23E228BS46'], ['johan Serrano', 'sebas.serrano15@gmail.com', 'I-FUCUMFEDNJRS'], ['Cesar Buitrago', 'cesar.buitrago@outlook.com', 'I-AHN0JSU36C2Y'], ['clara rendon', 'clara.ean@gmail.com', 'I-52BUJ1TCUHSX'], ['College Park Presbyterian Church', 'jonieradrian@hotmail.com', 'I-KFDDJR23LYJG'], ['Belen Mora', 'morabetu@gmail.com', 'I-XV6959S04R4C'], ['Jackeline Gomez', 'jacky8721@gmail.com', 'I-X2KM7HYSJ91K'], ['paula hernandez', 'Paulahv@student.fdu.edu', 'I-TH4U3C5U2TEX'], ['DIEGOP', 'diego.pena2006@gmail.com', 'I-CN1D3Y9PTJU0'], ['Victor Mejia Gamboa', 'lionelmessi3785@gmail.com', 'I-JFWPN72JWX3S'], ['Edwin Dominguez', 'dominguezalex05@gmail.com', 'I-0UE6B4H4P73P'], ['fabrice donat', 'babice1403@hotmail.fr', 'I-M0TKBSJ8EDK6'], ['Andrea Gomez', 'nagogama@gmail.com', 'I-5BH024XBYCRB'], ['Diego Malagon', 'diegodfm02@hotmail.com', 'I-W04WWY2LSVUC'], ['Andres Felipe', 'pipeoloquillops4@hotmail.com', 'I-RFVB7AMJEUPG'], ['Manuel Salas', 'msamaso@yahoo.com', 'I-TMXHHFJDXPXD'], ['Vicente Pesantes', 'vicentiko250288@gmail.com', 'I-JN789L72AF5J'], ['Joan Sebastian Salcedo', 'joansebas40@gmail.com', 'I-R2SYK27M3JW2'], ['Giovanni Orjuela', 'gioglo2017@Outlook.com', 'I-J6EBHECX110T'], ['Giovanna Bailey', 'Giovanna.bailey1@gmail.com', 'I-H984WAR048E2'], ['Nataly Katherine Guaba Beltran', 'nathgb30@hotmail.com', 'I-4A7N9SG5MJWR'], ['andres mauricio lopez', 'andresbedoya0610@gmail.com', 'I-RWWL4067EBLT'], ['Angelica Sanchez Gomez', 'maasanchezgo@gmail.com', 'I-MN8BNUVBMMFM'], ['Grettel J Nieves Martelo', 'rithialeluttel@gmail.com', 'I-75S8SDSMGUJP'], ['Joan S Higuera iglesias', 'parkoursebas@hotmail.com', 'I-KCCM46A4YGR7'], ['Katheryn g Segura arroyo', 'siradzeks@gmail.com', 'I-AKPDKD5PJRFE'], ['Grettel J Nieves Martelo', 'rithialeluttel@gmail.com', 'I-5G476G6N3VK1'], ['Juan sebastian Medina lopez', 'sebastianlopez_21@hotmail.com', 'I-J0M293C2PK9J'], ['Jean Medina', 'jsmedina94@hotmail.com', 'I-72N0YAMHRP34'], ['jose manuel simoes reyes', 'simoesrjmanuel10@gmail.com', 'I-T6TYGK8W9YCF'], ['Orlando Bedoya', 'omanlori12@hotmail.com', 'I-UBKN1SUS2FCL'], ['Wilson Ruiz', 'silver112_46@hotmail.com', 'I-A6FBMHAUCYEX'], ['Darwin Amias', 'darwin-amias@hotmail.com', 'I-448FP0N9Y007'], ['Lidiyan Martinez', 'lidiyanm@gmail.com', 'I-9ECRK5U90EGG'], ['MONIQUE ACEVEDO', 'juanpvacevedo@gmail.com', 'I-4YJHCFX7DYHM'], ['Triana Rosa', 'rosa.triana.fernandez@gmail.com', 'I-9WS90EB9LLLS'], ['Markus Rees', 'markusrees66@gmail.com', 'I-1BA383HG33BX'], ['Andres Reyes', 'saoko1927@gmail.com', 'I-0AU4LBFVD15J'], ['Alvaro Reyes', 'thecorneroftumbayork@gmail.com', 'I-C0X679SHT7DG'], ['Jose Nino', 'miguelno961@gmail.com', 'I-GT7HBDKNCTL9'], ['Andres Reyes', 'saoko1927@gmail.com', 'I-LUJDLE5M49XY'], ['Gloria Fernandez', 'pattyfdez@gmail.com', 'I-58K3MAGKLCHL'], ['Melissa Arbour', 'melissa.arbour@icloud.com', 'I-SDAXJYJ0MD38'], ['Juan Carlos Arevalo Bernal', 'jcarevalo16@outlook.com', 'I-2KPY9P509S5S'], ['Margarita Dura Sebastian', 'chigui8805@hotmail.com', 'I-HSDDWUCRAJM0'], ['CAMILA EUSSE', 'camilaeusse@gmail.com', 'I-GWKT4PX91K7D'], ['nicolas forero', 'nicolasfv-13@hotmail.com', 'I-6UKT7B048E1U'], ['yessika lasso gañan', 'yetalaga@gmail.com', 'I-8CPNH3R74FKH'], ['Erika Riano-Mojica', 'erikarianomojica@gmail.com', 'I-HLGX2UEBU954'], ['Francisco De Angulo', 'francisco@sainc.co', 'I-D3E02H41Y7BH'], ['maria c daza arango', 'mcdazaa@gmail.com', 'I-V8GURNB24JDJ'], ['Genesis Hernandez', 'genesishdz08@gmail.com', 'I-BCTNFBP3HRK8'], ['Edwin Andrés Hernández Mateus', 'andrez_m981@hotmail.com', 'I-3JR6PY99JNNE'], ['Sarith Amaya Frias', 'sdamayaf@gmail.com', 'I-CYVBVA8PBDFA'], ['JUAN VELEX CASTRO', 'velez5dm@yahoo.com', 'I-0B80T38L1F1A'], ['jonathan lugo', 'jonathanflugo05@gmail.com', 'I-XGYJD6WSXYPJ'], ['Cindy Y Hernandez', 'cindyhernandez9108@gmail.com', 'I-VDE858HGJWK9'], ['AnaMaria Barahona', 'anamariabarahonasierra@gmail.com', 'I-W0HHPX2NMNKN'], ['ibeth mathed', 'milyniro06@gmail.com', 'I-AE1E8KUBWDRE'], ['DIEGO VELANDIA', 'diegof.velandiap@gmail.com', 'I-6UJTHK7N51UL'], ['David Valbuena', 'davidgova4@hotmail.com', 'I-3REYFJF9E4WP'], ['giovanni torres', 'gtorres.valencia1911@gmail.com', 'I-FRL0PU11JVH9'], ['warren eslan', 'warren.eslan@hotmail.fr', 'I-CUXC90YP7SKR'], ['Valeria Ruiz', 'ruizvalee24@gmail.com', 'I-E903K4BTU69S'], ['Paola Velásquez R', 'paolavelro@outlook.com', 'I-2PRWVTJ2MM4C'], ['Heidy Ramirez Mendoza', 'ramirez.heidy@hotmail.com', 'I-V6ULTJ2Y45J6'], ['Martha L Munoz', 'malumupe@hotmail.com', 'I-7MCYV5KX0XXJ'], ['Laura A Ruiz Gaona', 'la.alejandraruiz10@gmail.com', 'I-2N1KBFNTEFHF'], ['Luz Alejandra Pena Alvarez', 'lapal865@gmail.com', 'I-D30DXTN1PPDV'], ['Mateo Garzon', 'mateotarquino22@gmail.con', 'I-FT1JJN3SC4LG'], ['Luis Felipe Guzman Vargas', 'guzmanvargas.lf@gmail.com', 'I-ARASRMD530VF'], ['Luis Felipe Guzman Vargas', 'guzmanvargas.lf@gmail.com', 'I-2BWCWRM4TVHN'], ['Alejandro Rosa Herazo', 'alejorosacuda@icloud.com', 'I-JAYSTV4SMF4U'], ['Karl Kuderer', 'kkuderer5@yahoo.com', 'I-2NHNEP8UG14J'], ['Milena Mora', 'mil.anelim.1994@gmail.com', 'I-5GDBVJ0CB17B'], ['Patrick Sosa', 'smartufo007@gmail.com', 'I-TK1W3B9246XC'], ['Bernardo Lukowiecki', 'bernieluko@gmail.com', 'I-P7FM2WGAHPHF'], ['Valeria gonzalez', 'valeriagonzalez228@hotmail.com', 'I-C0P858S3NDSH'], ['Fredy Fonseca', 'andresfonseca082@gmail.com', 'I-WVKKDA259CYM'], ['Julian  Pardo', 'julian081999@hotmail.com', 'I-DF9V6D4YL3MN'], ['Nicolle Montero', 'stephaniaperez2931@gmail.com', 'I-X7VHUM28DA25'], ['ingrid garcia', 'paulinaherrera2019@gmail.com', 'I-3F80DFFFRK74'], ['steven padilla calero', 'jjcalero1913@gmail.com', 'I-S14REAHPJLFS'], ['Edgardo Abadia', 'edabadia35@gmail.com', 'I-VMMSP3AWVNF5'], ['Nelly Mendoza', 'accounts@mendozaautomotive.bz', 'I-R52SSVAG2HAP'], ['DANIEL DUARTE', 'danielperico872@hotmail.com', 'I-VBRBH7N721WN'], ['Milena Agudelo', 'mile_garcia_agudelo04@yahoo.com', 'I-JABASRJRFAMR'], ['Kenia salazar', 'adriana.salazar972@gmail.com', 'I-7DCM6F23JGP4'], ['Gustavo Hernandez', 'junnior056@gmail.com', 'I-7U4VVMWTC99V'], ['Luisa Castellano', 'luisamora16@gmail.com', 'I-5EWFY3Y9M0W0'], ['Maria Falvey', 'Mafeben@yahoo.com', 'I-S6K1KJUS47PT'], ['Christian Arenas', 'crasmar@gmail.com', 'I-H089P6TRAA0B'], ['Juliana Quintero', 'juliana.quinterof@gmail.com', 'I-4P3NXF6RT9YJ'], ['Gina Isabel Gil', 'ginaprietog@gmail.com', 'I-RF1T7XE7LLY0'], ['Angye Carolina  Poveda Henao', 'angie11.ap@gmail.com', 'I-0RSKYHWYRJWK'], ['Juan Carlos Wiesner', 'juankwiesnerteei@gmail.com', 'I-ARC1V66RDNJW'], ['juan cardona', 'juan94cardona@hotmail.com', 'I-A735JDEBU6F6'], ['Diego Merchan', 'merchanpico@hotmail.com', 'I-WTA1F8SA70RL'], ['Juan Fernandez', 'sebastian.ff12@hotmail.con', 'I-V4MVHKCN70DP'], ['Leonardo Mejia A', 'leomejia08@gmail.com', 'I-X2B8HEMFKBV2'], ['MAYRA ALEZONES', 'ALEJANDRASBUSINESS@HOTMAIL.COM', 'I-7GMKEKN68AJL'], ['john heiner ocampo ocampo', 'johnheiner@yahoo.com', 'I-HJ1SN5B8GDNM'], ['Sebastian santos', 'alejandrosan126@hotmail.com', 'I-U5R1WWA80UXT'], ['Juan Velez Gonzalez', 'juanpavelez20@gmail.com', 'I-553NF6R2P59F'], ['Camilo Pava', 'kmilo2012@hotmail.com', 'I-LVC2HJFENM4C'], ['Felipe Rodriguez', 'katalina.1417@hotmail.com', 'I-SB4HJ5E06HR9'], ['Gabriel Daza', 'gfdaza@hotmail.co.uk', 'I-7HYG95JB6AE7'], ['Hernán Darío  Herrera Morales', 'figbrublack@gmail.com', 'I-C4H3L912UTAY'], ['Joseph Garcia', 'josephandrey_95@hotmail.com', 'I-5L3UHFRYFHN5'], ['elizabeth montalvo', 'elimontalvo83@gmail.com', 'I-1AXCBCH3FUVP'], ['Alba Asuad', 'ialba.ps@gmail.com', 'I-HJNP2KHGXKHD'], ['ana cadavid', 'martajimforo@hotmail.com', 'I-00E3X9J5S9VK'], ['Juan David Mongui Rojas', 'juanmongui52@gmail.com', 'I-HUA05LBC8HDX'], ['Luisa Alcala', 'Alcalita@gmail.com', 'I-BA3CP976BWK8'], ['Diana Alzate', 'alejita9510@gmail.com', 'I-YDTAWNCPX6VL'], ['Lilia Roney', 'lilia.roney@gmail.com', 'I-A71U6P1H253C'], ['Silvia Collados', 'silviacolladosv@gmail.com', 'I-RDT7BCR4XS6A'], ['Estefania Garcia', 'tefagarcia13@gmail.com', 'I-T3E2R58X5CDC'], ['Carlos Andres  Ordonez Dallos', 'cordonez2000.co@gmail.com', 'I-809PWLVDWTU0'], ['Nixon Anderson Castellanos Hernandez', 'nixoncastellanos@gmail.com', 'I-6WT1GDS3GR86'], ['Adriana Daza', 'adrianitadaza@gmail.com', 'I-BX2MM9YPU947'], ['Miguel Pinilla', 'angelmiiguel18@hotmail.com', 'I-AWJFRVHA435B'], ['Pablo Antonio Gelvez Munevar', 'pablog14@gmail.com', 'I-96AEVYMPN78R'], ['Julian Calambas', 'juliancalambas1@hotmail.com', 'I-0XTBRA60K7CH'], ['Nelson Lasso', 'nelsonadrian86@gmail.com', 'I-JV9288NT04FC'], ['Luisa Catalina Loaiza Gallo', 'luiloaiza@hotmail.com', 'I-47XB8RDY5D2W'], ['MARIA MORAN', 'camilamoran91@gmail.com', 'I-HNYBJ2TBHWYH'], ['Mariana Guerra lorenzo', 'mgl1305@gmail.com', 'I-15NALAGXV4A2'], ['Lina Maria Silva Rodrig', 'lmarsil@hotmail.com', 'I-D58AXFHYJCC0'], ['Hector Martinez', 'hectoren90@hotmail.com', 'I-VF6CJE1JH41A'], ['Tatiana Zambrano', 'tata28tz@gmail.com', 'I-XLF2D03HWFD5'], ['giovanny moreno cabal', 'gio_mc@outlook.es', 'I-FV1BFTUED54D'], ['Jorge Jaramillo Espinoza', 'comjesmarsa@hotmail.com', 'I-4XRJ9SFUAL2L'], ['Dylan felipe Leyton mutis', 'mutis131@hotmail.com', 'I-5SMVJS0NJMTW'], ['Carlos Suarez', 'carlos.suarez@live.co.uk', 'I-7PBK1VF7G14V'], ['Brayan Conde', 'braycon_1016@hotmail.com', 'I-DACXE3RPTT22'], ['Sebastian Velasquez', 'sebasgarvel@hotmail.com', 'I-FLU6VBRLGB23'], ['Juan Ruiz', 'piperuiz45@gmail.com', 'I-N65F189PP3A3'], ['EDWAR A SOTO PALACIO', 'bryangalvis2004@gmail.com', 'I-XELXYX1K2ACV'], ['Angela M Ortiz', 'angela-barco@hotmail.com', 'I-TAG5THXM8V91'], ['KELLY RODRIGUEZ', 'nanita.1990@hotmail.com', 'I-6TA95GLGRRSK'], ['JENIFFER NYCE', 'jeninyce@yahoo.com', 'I-PVP3A0UX8GN9'], ['Luis Felipe Téllez Mateus', 'lufetetema@hotmail.com', 'I-A3A5PABKEDEG'], ['Juan Rincon', 'juan2089@gmail.com', 'I-JJ4PAJ8H3VCX'], ['Fill Perez', 'philmperez@hotmail.com', 'I-J1NKSX3SSMU9'], ['CAMILA C RODRIGUEZ', 'macaroro@hotmail.com', 'I-3EG57N0TNERA'], ['juan penagos', 'juansebastianpenagosm@gmail.com', 'I-HXVHMK723CB9'], ['Victor Rodriguez', 'victordparc@hotmail.com', 'I-628M1RCUVFSA'], ['Jimmy Rodriguez Lopera', 'rodriguez.lopera@gmail.com', 'I-G6BA0KUWWEEC'], ['laura foote', 'laurabejaranocastro@icloud.com', 'I-2SSDV54R8032'], ['Joan Salazar', 'osmarsa88@gmail.com', 'I-P8P7TPTXDR2U'], ['Nathalia Saavedra', 'nathalia_1289@hotmail.com', 'I-LVSNXXBF3D4A'], ['Sara Escobar', 'saraescobarg@gmx.de', 'I-3MM0ACGT2A5K'], ['Diego Padilla', 'diegopadilla0304@gmail.com', 'I-YU6L39VLVTN3'], ['Ricardo Fuenmayor', 'rfuenmayor@shipwithglt.com', 'I-H7HEWY2LW249'], ['oscar mauricio galvis daza', 'rocksodia2003@msn.com', 'I-LD5CPNST4EHH'], ['Juan Cabeza', 'juanfelipe279@icloud.com', 'I-9NYAT9613TTF'], ['Adriana Uribe', 'apus1111@aol.com', 'I-X6HCJ67CESEV'], ['Omar Martinez', 'bomar_gutan@hotmail.com', 'I-NEXN923TKCLA'], ['venessa Manjarres', 'vanessamanjarres@hotmail.com', 'I-6E46JAG76ASF'], ['Alejandra Hernández', 'alejahdez92@gmail.com', 'I-TDGS5PD19GN5'], ['Ariel Baez', 'a.baez305@gmail.com', 'I-JPTLX6XA26J4'], ['andres rodriguez', 'karibeshockey@gmail.com', 'I-KMMTF1TVMR80'], ['Andrea Garcia', 'angaristi@hotmail.com', 'I-PEGV7LWPJWBU'], ['Jorge Herrera', 'george.939@hotmail.com', 'I-AARV0YU2BK9C'], ['Maurizio Da Rold', 'mauriziodarold@hotmail.com', 'I-9DEXLU5A0FTX'], ['diego Alexander', 'tecnidiegocel@hotmail.com', 'I-1DJEE7GG1041'], ['Viviana Molano', 'vcmolanoc@gmail.com', 'I-TR516TXWUP8C'], ['Juan F Vargas', 'fvdm@outlook.es', 'I-W9D00FXGEES9'], ['Phillip Lopez', 'feliperojaslloveras@hotmail.com', 'I-PNHA3XRW14F4'], ['Jeimy Malmquist', 'amybrea18@gmail.com', 'I-1JJ8PF25VJ3R'], ['Jonathan Guzman', 'colmbnhtz1203@aol.com', 'I-EJL26NV0E6G2'], ['JHON FREDY GUEVARA H.', 'jhon.guegara@agunsa.com', 'I-TWC8GB9UPMSF'], ['Kevin Rojas', 'kevin-rojas98@Outlook.com', 'I-7DCH6Y22JGP4'], ['karim´s', 'karim-marzullo@hotmail.com', 'I-HMX402S59GB0'], ['Shirley Echeverria', 'sechever@mail.usf.edu', 'I-05PJRRA6U2PA'], ['luis andres gomez gelvez', 'gaga110548@gmail.com', 'I-SF5SA8MGTDPF'], ['Diana Montoya', 'dianacmontoya@hotmail.com', 'I-7NW54X5L0584'], ['Daniela Rodríguez Franco', 'Danny_0695@hotmail.com', 'I-245DVESBUL4K'], ['Daniela Padilla', 'deisydanielapadilla@gmail.com', 'I-00PAU0K7XJX2'], ['Javier Parada', 'jepv@hotmail.com', 'I-52J0GG94NMAM'], ['Camilo Curcio', 'kamilocurcio@gmail.com', 'I-NACVLGMMEU2U'], ['Eliana Torres Gutierrez', 'elitorresgutierrez@gmail.com', 'I-2154UPN0XXLW'], ['andres garzon', 'andresedu2001@hotmail.com', 'I-JV9080NT04FC'], ['Lina rubio', 'lbejarano24@icloud.com', 'I-2SSRV14R8032'], ['Monica Patino', 'monilp86@gmail.com', 'I-FYMS3KB8DS3L'], ['Johan alexis Rondón triviño', 'yesmeldy17@gmail.com', 'I-KCNGSS19C3YN'], ['RAFAEL VERNAZA', 'mateo.vernaza@urosario.edu.co', 'I-L9V0MGGD4T5L'], ['Felipe Aguirre', 'aguirrucho84@gmail.com', 'I-P08TAMT1TT0A'], ['jorge romero', 'jerr79@hotmail.com', 'I-LH2YL6GFSWR6'], ['ana duarte', 'carito-dua@hotmail.com', 'I-31H3YGCP91W2'], ['Maribeth Hernandez', 'maribethhg@gmail.com', 'I-WBA5CNM4VNDK'], ['Hector solorzano arias', 'mariobrujo@hotmail.fr', 'I-WT22NT89TXNX'], ['Phillip Lopez', 'feliperojaslloveras@hotmail.com', 'I-YKVPWT2NC6YP'], ['fernando coiran', 'rmediavilla@gmail.com', 'I-7S1CHHV7X6M0'], ['German Diaz', 'Arq_andresdiaz@yahoo.com', 'I-65AC64WSKJR3'], ['Sara Escobar', 'saraescobarg@gmx.de', 'I-L32AP33KVTSL'], ['Angelica Rivera', 'angelmariarivera94@gmail.com', 'I-U74WW4JP77L0'], ['Laura Gutierrez rojas', 'laugutierrez.r@gmail.com', 'I-UKADW4BVMTJ5'], ['milton favian jimenez ipuz', 'tc150212@hotmail.com', 'I-T2LXG0SDV4BV'], ['Ana Valentina Cardenas', 'anavalen147@gmail.com', 'I-CFP3A1PA378D'], ['Ana Valentina Cardenas', 'anavalen147@gmail.com', 'I-LF099W8J5066'], ['humberto murcia', 'jpmurcia7@gmail.com', 'I-10YAW6UV6TM8'], ['lina giraldo', 'Lina2c13@comcast.net', 'I-SU6NW4CPJE9Y'], ['Camilo Candela', 'camilocandela25@gmail.com', 'I-X7T8929HPNU2'], ['Jonathan Prato Dos Santos', 'jonathanjespd@gmail.com', 'I-YEN16V2LGF7U'], ['pablo pedraza', 'estebans1993@hotmail.com', 'I-C8XUSP86RSKS'], ['fabrizio Torosantucci', 'aimola1982@gmail.com', 'I-5GDTWX0DB17B'], ['Diego Bedoya', 'diegobedoya04@gmail.com', 'I-MT4WD0SPKTU4'], ['Daniela Ramirez', 'ramirezdanielaa10@gmail.com', 'I-66BRMBWBGRXD'], ['Lina María Ortiz Hernández', 'linita1382@gmail.com', 'I-ACNXATRGHCJF'], ['Juan Ardila', 'jardila@mail.usf.edu', 'I-0NRB4GAMB32M'], ['stefany guerrero comas', 'wilsonsobrino@hotmail.com', 'I-VMFHX35K9517'], ['Tania Ordonez Mellizo', 'shairon272@gmail.com', 'I-FJASVLN1ARYT'], ['Nicolas Cortes', 'nicolascortescifuentes29-20@outlook.com', 'I-AWJ8S8HA435B'], ['yeni alzate gongora', 'jennylink6@hotmail.com', 'I-E9Y3K315LCH3'], ['Lina María Ortiz Hernández', 'linita1382@gmail.com', 'I-AJX0XMP1XXRJ'], ['Carolina Rodriguez Buitrago', 'crodriguezb25@gmail.com', 'I-6UHG9LND3NPJ'], ['carlos moncada', 'zonagrafic1@hotmail.com', 'I-48G3749PM2X8'], ['Maria Robayo', 'robayo_2000@yahoo.com', 'I-J0V58NRSAAKR'], ['maria velez', 'ferveleztim@hotmail.com', 'I-32CK8X02STB9'], ['Andrea Guerrero', 'andipb.93@gmail.com', 'I-1RA9RYKJ1265'], ['DIANA GIL', 'dianakgiljimenez@gmail.com', 'I-HP404WJ50S07'], ['Carlos Andres Herrera Cardenas', 'carlos-andres.herrera@unilever.com', 'I-X5696Y5HA7A5'], ['Rafael Calle', 'rafacalle@hotmail.com', 'I-HEVMJD8SBBFE'], ['Rafael Duenas', 'rafaelduenasp@gmail.com', 'I-HNY8MXTBHWYH'], ['Andrea Peñalosa Segura', 'apenalosa2089@gmail.com', 'I-D581XKHYJCC0'], ['Santiago Ariza Rodriguez', 'santiagoari95@gmail.com', 'I-XBUAF47E7HHF'], ['JORGE FONSECA', 'jorgefonseca0513@gmail.com', 'I-SJ158GYBSR5E'], ['Claudia Rojas', 'claudialrojasramos@gmail.com', 'I-X6WD7N87DFKK'], ['Daniel Jimenez', 'sm.daniel2009@hotmail.com', 'I-UF78H0R4A5WE'], ['Maria Angelica GARCIA INSUASTY', 'maria_angelica_garcia@yahoo.com', 'I-ERVA7FHT5RNR'], ['Eduardo Palma', 'epalma5@pratt.edu', 'I-THT2TVG3HF6T'], ['Jesus Patino', 'elgomez204@gmail.com', 'I-FRL7HP4D8M3F'], ['Elemental Platform', 'jose.chirinos@elementalplatform.com', 'I-C07AAKX6X55Y'], ['Diana karina Roa palomo', 'diana.roa@outlook.fr', 'I-1P06VBSHW3D5'], ['Matthew Laharenas', 'Mattlq17@hotmail.com', 'I-B2Y3KECB63HV'], ['Isaac Escobar Gallego', 'izzy.esco@yahoo.com', 'I-S7VR5CCMNAJC'], ['scentology', 'am1116@hotmail.com', 'I-8E48S9LDXRDC'], ['clever mendez', 'manuelmmendez@icloud.com', 'I-GC81BAMWX3NW'], ['Mariafernanda Bonilla', 'maferomero64@icloud.com', 'I-0GGUSP30TDBA'], ['jose j  agudelo perez', 'jagudelo1021@hotmail.com', 'I-H2WEC647NB0H'], ['andres Marin', 'marin.andres29@gmail.com', 'I-1JJXPP25VJ3R'], ['Juan Valenzuela', 'juanvalen.jerez@gmail.com', 'I-DJ342K4N6NHG'], ['claudia nivia', 'c.huerfano@cetcservices.com', 'I-2M4K622WH9WH'], ['Ricardo Gallego', 'YesIHaveIt@outlook.com', 'I-VG8CU1DV69JH'], ['mauricio ramirez', 'ramirez.mr383@gmail.com', 'I-TELW71V3SB7U'], ['MARIA CAMILA LOPEZ CELIS', 'mclopez36@hotmail.com', 'I-93U4AWT565H2'], ['sandra Betancourt', 'sandrabetancourt122397@gmail.com', 'I-42UVEYGVP2G5'], ['luis Esteban Hdez', 'mongoles23@hotmail.com', 'I-687CWAJEE5JT'], ['Angela Maria Lopez Salazar', 'anfela85@hotmail.com', 'I-8TMX9XUW6XH8'], ['brandon castiblanco', 'brandoncastiblanco05@gmail.com', 'I-RKYJP4F4YAYR'], ['Paula Huerfano', 'mariapaulahuerfano@gmail.com', 'I-F4XNCSNV2SVB'], ['Owehiman Diaz', 'owehiman888@gmail.com', 'I-M4XU3390FCNC'], ['Paulo Pérez', 'longtreelife@gmail.com', 'I-TK67GWR7E9V7'], ['aura patino', 'steven0287@hotmail.com', 'I-GPG6CCGBWUVN'], ['sandra espinosa', 'sebascampu987@gmail.com', 'I-MYR8532DF2KX'], ['Monica Camacho', 'monica.camacho0191@gmail.com', 'I-CPSDANEUHLUN'], ['Lina Maria Anaya Beltran', 'linama25.93@hotmail.com', 'I-FG2G3VSHJRP4'], ['Buzi, Inc.', 'juan@buziapp.com', 'I-X6HMJJ7CESEV'], ['Felipe Garzon', 'Felipegarzon54@gmail.com', 'I-R9K0M57DC9EE'], ['Joyce Abondano', 'joyce.kinesiologue@gmail.com', 'I-2HNDGB5E92C1'], ['mateo rodriguez', 'browcerouno@gmail.com', 'I-VDJ6BCL3RABS'], ['Jeniffer Casas', 'jeniffercasasce@outlook.com', 'I-MUUPGCXBL1FL'], ['Natalia Romero', 'nfern1090@gmail.com', 'I-RPW3GSU92TKD'], ['Liseth Paola Rodriguez Lara', 'lisethrodriguezt@gmail.com', 'I-1BAUB4HH33BX'], ['ANGELA CANTARERO JIMENEZ', 'angelacantarero2013@gmail.com', 'I-TBVALY09CAX6'], ['Natalia Romero', 'nfern1090@gmail.com', 'I-7745CRS7AE4D'], ['Edward Ramos', 'favianramos69@me.com', 'I-LEYMA3NHGP41'], ['GILBERTO VASQUEZ MORENO', 'basejose_12@hotmail.com', 'I-FUCNN3ECNJRS'], ['Guisselle Garcia Guzman', 'alexandragarciaguzman0@gmail.com', 'I-GT7SRDKPCTL9'], ['katherine jordan', 'katejg25@gmail.com', 'I-A36CAEP68S83'], ['Juan Rodriguez', 'jprodriguezbetancur@gmail.com', 'I-2E9DHRB7729T'], ['Laura Galvis Vargas', 'galvis.laura@gmail.com', 'I-9HKRWW29M89J'], ['Karla Vargas', 'karlavargas.gtb@gmail.com', 'I-E15G75N0YF7Y'], ['Andres Cala', 'andrescalarey@gmail.com', 'I-31SR888RNTD9'], ['Vicente Correas Galan', 'vicentecorreas@gmail.com', 'I-3AKX8DKL5GWN'], ['Andres Huertas Arjona', 'andy87.1@hotmail.it', 'I-NFUTCG8XYS3U'], ['Diana brinez', 'lanenacoleccionista@yahoo.es', 'I-KBCX7NNTXHMH'], ['ANDRES FELIPE MORA', 'flpmora0@gmail.com', 'I-A96F7LVF314K'], ['Silvia Villa', 'siv1224@gmail.com', 'I-0MTWCNP7059L'], ['Tania jaimes', 'tjeb29@gmail.com', 'I-S5PJ45E16RNY'], ['Juliana Rosado', 'juliana.ortiza12@gmail.com', 'I-B62X08CXK18L'], ['Luis Felipe Anturi Grisales', 'luisfelipeanturigrisales@gmail.com', 'I-4HH1LREL6JAV'], ['Andres Tarazona', 'andrestarazonamendez@gmail.com', 'I-CFH18L6FKDT7'], ['paula arias sanchez', 'paulaarias.05@gmail.com', 'I-SMV9UUJA1RV8'], ['Jorge Morales', 'jorgeandresmoralesreyes@gmail.com', 'I-WFKLWUUB982E'], ['LUZ CARMONA', 'pili1108_6@hotmail.com', 'I-EBT8LDM0N2F6'], ['Cristhian Andres Quira Giraldo', 'cristhian.quira@gmail.com', 'I-WL8UNBP3VR3W'], ['Malvy Molano', 'malvy09@gmail.com', 'I-19RSA6SXE46E'], ['Catalina Orrego', 'katyorrego@gmail.com', 'I-MYR7572DF2KX'], ['Jefferson Castro', 'jefree.78@hotmail.com', 'I-SWDH73GC1G88'], ['Natalia Fresen', 'nfresen@yahoo.es', 'I-BSNA575WXH8X'], ['Erwing Gomez', 'erwinggomezquiroga@gmail.com', 'I-Y7JV5C42RVRA'], ['Ricardo Alexander Pelaez Bernal', 'ricpel@hotmail.com', 'I-EHDML65WWWTR'], ['Nirsa Lopez', 'niryu81@hotmail.com', 'I-SX4HUY095KA8'], ['Julian andres Gil florez', 'juliangil82@hotmail.com', 'I-K72HUV0Y62L1'], ['Luisa Oliveira', 'nena1218@hotmail.com', 'I-W0C2CJBJ7WBB'], ['Juanita Ortiz Ocampo', 'juanita.ortizo94@gmail.com', 'I-3R9KV90VWEJ0'], ['JULIANA C PIRA-ORTIZ', 'kata.pira@hotmail.com', 'I-5M6W5LPTG17N'], ['Pablo Echeverri', 'pablo.echeverri.00@gmail.com', 'I-T0MRKUGE0MJR'], ['Fabiana Garcia', 'fabianaggarcia1@gmail.com', 'I-PA92CVXV0J30'], ['fabian Prieto', 'fabiandrespri8210@gmail.com', 'I-GWK15KX81K7D'], ['Jesed Gomez', 'jesedgomez@hotmail.com', 'I-8JX82WTT1JFP'], ['William Jula', 'wfjula@hotmail.com', 'I-F0D320EKLN5P'], ['Erick Beltran', 'gladysmenjura9@gmail.com', 'I-MV4SAM4VE2SR'], ['Paola Barrios', 'paolab9713@gmail.com', 'I-DC501DNP1XL7'], ['Andres Madiedo', 'andrespulidocena@gmail.com', 'I-7EPCEMUUX14L'], ['DESMA', 'fvargas@desma.com.mx', 'I-5MYAEW9PWFE8'], ['Carlos Calvo', 'cechoenigsberg@gmail.com', 'I-KUNU4NL6DEYB'], ['Fabian Zamudio', 'fabian.zamudio@icloud.com', 'I-86KUAPF9J4GF'], ['Mauricio Ospina', 'mospinaster@gmail.com', 'I-M0NY1PLVC1NU'], ['Maria Juliana Heatherly', 'mariajuliana1219@gmail.com', 'I-MVWTU5CE6LYV'], ['Santiago Pinzon', 'astralerrante@gmail.com', 'I-SW094EY13JLL'], ['Jenny Rottmann', 'jennyrepollo@hotmail.com', 'I-7AVS3H6T4BP5'], ['Miguel Vergara', 'miguelvergara39@hotmail.com', 'I-SB4VJ9E16HR9'], ['Maria Alejandra Madiedo Camelo', 'maleja0622@hotmail.com', 'I-8Y47YYE9RCAR'], ['Juan Cardenas', 'juanjosecardenas.c@gmail.com', 'I-E4345AFUHSYE'], ['Sebastian Viveros', 'sebastianviveros30@gmail.com', 'I-SWDC7YGC1G88'], ['BLANCA KAZMIERCZAK', 'BLANCA_KAZ@YAHOO.COM', 'I-VG3FSWM8E5CL'], ['Gloria Henao Marin', 'henao.gloria@gmail.com', 'I-H48Y9MR814H8'], ['Charlene Castillo', 'titichar26@hotmail.com', 'I-T90GLY1566TC'], ['Jimena Cabascango', 'aytzaj@hotmail.om', 'I-18PME53H3ES2'], ['gloria pulgarin', 'gloria12_21@hotmail.com', 'I-HWRJBX8WYYJ2'], ['NICOLAS LOPIERRE AGUIRRE', 'nico-la@live.com', 'I-1CWDSCSV7W7R'], ['Andrea Restrepo', 'restrepoandrea200026@gmail.com', 'I-A23PX172E6CG'], ['jack btesh', 'jaco0844@gmail.com', 'I-JJK5GH7WC8S0'], ['Harold Cruz', 'haroldviviana@gmail.com', 'I-LK023YGAMMU5'], ['Jorge Martinez', 'jemartinez@planetife.com', 'I-6Y6LH5PJV9FA'], ['Daniel Duenas', 'pbgcraigs@gmail.com', 'I-FXMTH7UL39XL'], ['Luis Forero', 'cyhsforero18@gmail.com', 'I-WPPPW051RR11'], ['Luis Forero', 'cyhsforero18@gmail.com', 'I-PHN6C2JCX9US'], ['Johan Baquero', 'johan_baquero@icloud.com', 'I-E0SWJLBRYFFA'], ['JENNIFER RAMIREZ', 'YEYE860511@GMAIL.COM', 'I-ENC431BD3RP4'], ['María hernandez', 'm.alejandra54c@gmail.com', 'I-0HU963EEB3AM'], ['Erika Medina', 'erikajohanamm@hotmail.com', 'I-JCEBGU6A0NV1'], ['Javier León rozo', 'leonrozo.javier@gmail.com', 'I-MEYAXTCNGPTW'], ['jairo a jaramillo', 'jaramillo33@outlook.com', 'I-NCHJ0X5LVRNC'], ['Miguelangel Diaz', 'diaz_miguelangel@hotmail.com', 'I-CN0CY7C1C5MY'], ['Jairo Jaramillo', 'jairojaramillo33@outlook.com', 'I-2GBH20DEC1YW'], ['Juan Cifuentes', 'jnicocifuentes@gmail.com', 'I-4VWX3J77L7TN'], ['Harold Cruz', 'haroldviviana@gmail.com', 'I-X7TL9J9HPNU2'], ['camilo salcedo', 'mtba1@me.com', 'I-N78WVRX5YXGS'], ['JOSE RIVEROS', 'riverosjs@gmail.com', 'I-4N5KP4G5YFLD'], ['Javier Vivas', 'javiervivas2@icloud.com', 'I-ACEG7580XG8S'], ['Leidy T aranda', 'tatianabernal69@gmail.com', 'I-B1N13XT842NL'], ['Leidy Viviana Cordoba Vega', 'leidyviviana.c23@gmail.com', 'I-3AKV8MKL5GWN'], ['Francisco Flores', 'veronicaflores092@gmail.com', 'I-72RRFGR5EM8L'], ['John Arango', 'johnedward93@hotmail.com', 'I-MKTETBBFMYCU'], ['Mariany Rueda Reyes', 'marianyrueda@hotmail.com', 'I-HNWPWXWWYNMF'], ['Tomas Quintero', 'tomas010@icloud.com', 'I-AL0Y306RTNY8'], ['Paola Ricci', 'pomaricci@yahoo.com', 'I-5GENLDJESFYA'], ['Amancay Cepeda M', 'amancaydeatacama@gmail.com', 'I-E27GGYP75UXD'], ['Liliana Perez', 'lilipe04@yahoo.com', 'I-7697JGY7TFB2'], ['Maria Castro', 'mmontoyapictures@gmail.com', 'I-AP1L430V96TB'], ['megan brunette', 'meganbrunette@gmail.com', 'I-4DC9UKEUMUUP'], ['Seryo Castellanos', 'seryo145@hotmail.com', 'I-Y7JJ5L42RVRA'], ['Carlos Jimenez', 'cajv@yahoo.com', 'I-W0XVGMB62R0D'], ['Leticia Rodriguez', 'leticiarodriguezr1968@gmail.com', 'I-FBGSM5SU0L3P'], ['juliana camacho', 'julianacortes1102@gmail.com', 'I-XTNCSTNMJLG7'], ['luz Blandon', 'esdale@hotmail.com', 'I-HTLCNEA4EG3V'], ['Lucy m Giraldo', 'jmurillo0104@gmail.com', 'I-DEA6F33RMXHU'], ['Angelica Diaz', 'angelica_930710@hotmail.com', 'I-9ECKL1U80EGG'], ['karen tatiana castro sierra', 'katasierrac@gmail.com', 'I-U9H846B2WMKR'], ['Jeisson Daza', '3jd1981@gmail.com', 'I-HLLFX3MS0Y0E'], ['Cesar Serna', 'cesarserna424@hotmail.com', 'I-2S1DSM2J4USK'], ['luis lobo', 'd.vid_lobo@hotmail.com', 'I-29JU189KXKPH'], ['Christian Murillo', 'lilchriss.christian@gmail.com', 'I-DUDAKJNN59S3'], ['Catalina Villafane', 'ktavillita@gmail.com', 'I-E282L861Y2G4'], ['Daniel Cossio', 'daniel@argatrans.com', 'I-XY8PNJ3X8KT3'], ['Camilo puyo', 'cmilo50085513@gmail.com', 'I-X07KH2FD1DX9'], ['Bergen County Multisport, LLC', 'coachandres@gmail.com', 'I-58NE0K3NPN8J'], ['Kenny Mendoza', 'kenny.mendoz2014@gmail.com', 'I-YCNNT7D4X4BJ'], ['luis cardona', 'lfelipecardona@gmail.com', 'I-LKCXGSR2DYW5'], ['andres espinal', 'andrezo_10@hotmail.com', 'I-WRMKHRWW9S6L'], ['Andres Navarrete', 'anavarrete@addictive4u.com', 'I-1EWBKYE60PVL'], ['Edgar Gomez', 'edgb57@yahoo.com', 'I-00PF8BUX4JUR'], ['Sebastien Phaneuf', 'sebphaneuf@gmail.com', 'I-4N5RP8G5YFLD'], ['Lance Hess', 'lhessjet@gmail.com', 'I-0X225YXSEKWT'], ['Juan Arango', 'aran.jsa@hotmail.com', 'I-P7U0SS7TTF19'], ['ruben perez', 'rubendpg1989@hotmail.com', 'I-6NFP051HTN96'], ['Mauricio Garzon', 'mauriciog44@gmail.com', 'I-LVC9EXFCNM4C'], ['Myrna Rosentiehl', 'myrperiodista@gmail.com', 'I-7GUHPFLNY2G7'], ['Juan Hinestroza', 'agrofrutaslasjotas@gmail.com', 'I-J3PYUX2V59P0'], ['JOSE RAMOS', 'jr-ramos@live.com', 'I-19N8RHYWFTB0'], ['', 'juanpischannel@gmail.com', '26N161499C458321Y'], ['Daniel Duenas', 'pbgcraigs@gmail.com', 'I-T4VRU2BHC1M7'], ['Maria Camila Barriga Barriga', 'mariacamilabarrigab@gmail.com', 'I-J0VL7ARJAAKR'], ['Sara Giraldo', 'psarisg@gmail.com', 'I-5V335FHFWPE4'], ['Juan Arango', 'aran.jsa@hotmail.com', 'I-BY6JY4WRJYW7'], ['Marcela Vergara', 'marcevergara03@hotmail.com', 'I-YCYJWUVJ8YK5'], ['Irma Lucia Archila Correa', 'luciaarchila@hotmail.com', 'I-VP2BXBETCV01'], ['JUAN BETANCOURT', 'jupabequi@hotmail.com', 'I-XX3V1TWSYH6N'], ['Anderson Mercado', 'andersondmercado@gmail.com', 'I-Y5XY9EG4916G'], ['Orlando Escolar', 'orlesco@yahoo.com', 'I-UG1N3S5V415A'], ['Juan Arteaga', 'juandaarr@hotmail.com', 'I-LPD7DFJWCBCW'], ['Carolina Gonzalez', 'carito761218@gmail.com', 'I-7S1TESV4X6M0'], ['Ruben Recalde', 'rubenjavierrecalde76@hotmail.com', 'I-MR0VPARRKLBL'], ['Juan Montealegre', 'glasspro69@gmail.com', 'I-2GNPKUA66K0C'], ['Pedro Enrique Donoso Ramos', 'pedroedonosor@gmail.com', 'I-9SKLE61AK7HL'], ['Omar Nieves', 'chefrenenievestorres@gmail.com', 'I-YNHAERR3WH0K'], ['Edgard Andres Iglesias Chavez', 'edgardiglesias9@gmail.com', 'I-60L1R5UA70S3'], ['sergio Garcia', 'sergiogarciaospina@gmail.com', 'I-8YB8DS5S8267'], ['Chikiz Empanaditas Gourmet Inc', 'chikizempanaditasgourmet@gmail.com', 'I-H7WAH9HW2TMS'], ['Herbalife', 'yudycolombia@yahoo.es', 'I-8R8BWR74SYHR'], ['Juan Camilo Feliciano', 'juankfelix@hotmail.com', 'I-PL35TXG5GTTM'], ['Bruno Viana', 'quintadomeio@hotmail.com', 'I-E7FUCYN1E6WJ'], ['Juan Velasquez', 'juan.velasquez8@gmail.com', 'I-BEE1MDD4U7AK'], ['Nancy L Florian', 'liflorian21@gmail.com', 'I-09M2SNNL790W'], ['linda monroy', 'lila0210@hotmail.com', 'I-LL962VW551ER'], ['Sergio Andres Moreno Tellez', 'sa.moreno37@gmail.com', 'I-ERWYVN1W6C0F'], ['Cesar Augusto Munar Moreno', 'cesaramunar@gmail.com', 'I-YCNSTBD7X4BJ'], ['Andres Doval', 'afdoval@me.com', 'I-C96GST60F1D3'], ['Maira Toro', 'camitorom@gmail.com', 'I-N515VMPE2AHS'], ['Humberto Juliao', 'hjuliao_007@hotmail.com', 'I-SYEWELSRP2P8'], ['Julian Flechas Alvarado', 'julianflechas@hotmail.com', 'I-UTYELS320RGX'], ['Kevin Luna ortiz', 'kevin.klo1@hotmail.com', 'I-NHFR22PD9K9X'], ['Marco Gomez', 'marcvanhouten@gmail.com', 'I-YBR4P220PNAM'], ['Lelly Aguilar', 'ximena.aguilar83@gmail.com', 'I-ATXPKSXF3DSM'], ['camilo romero', 'camiloromero035@gmail.com', 'I-3J3FYBK5THHD'], ['Michelle Mendez', 'yesikavilla@hotmail.com', 'I-GPVAPWP3W1GP'], ['Andres Gallego', 'agallego343@gmail.com', 'I-XPU1DJSMHLEN'], ['ronald abdor', 'ronald@rabpoint.com', 'I-KRH31JGS10NY'], ['gersain mesa', 'gersamesab@hotmail.com', 'I-VEL5S154YDXL'], ['yeny Serrano', 'carolina.serrano.o@hotmail.com', 'I-H9345LR3TXET'], ['Laura Acosta', 'infomakala9@gmail.com', 'I-9P7FCE1JS1ET'], ['Julian Pachon', 'jpachonjk@gmail.com', 'I-DR06XRDRVSDT'], ['JOHANA VILLEGAS', 'JOHIVG@HOTMAIL.COM', 'I-HADB960UUMG3'], ['MANUELA MEJIA', 'manuelamejiasantos@gmail.com', 'I-YCWV4N4NNXES'], ['Mario Laverde', 'mariofranco1010@gmail.com', 'I-HWLFMMWY4YGM'], ['Sindy Machuca', 'sindylo11@hotmail.com', 'I-WPVV1KJPBN5R'], ['Julio Higuita', 'julio_higuita@hotmail.com', 'I-2BB7KW17N5B2'], ['Diego Erazo', 'diegofelipeerazo@gmail.com', 'I-KTC6DYBHCUGD'], ['Gabriel Laverde', 'gabito99@hotmail.com', 'I-JS87YKK3RXEH'], ['Santiago Maldonado', 'sanlibrawn@hotmail.com', 'I-RJWGBG6TB25D'], ['Ella Dulcey', 'ella0625@gmail.com', 'I-URKX0YW54JKN'], ['Andres Hoyos', 'andresfhoyos950629@gmail.com', 'I-V7JGAUKVYYVH'], ['Carolina Duque', 'sammydiazduque@gmail.com', 'I-D4VU0KV3YJ0K'], ['Paola Pinzón Chivata', 'pao28951@gmail.com', 'I-AMFLCWNHF5L5'], ['Juan Garcia', 'juangar9713@gmail.com', 'I-91BDX5182DMT'], ['Pedro Acero', 'pedroacero15@gmail.com', 'I-0FY3WEN0TXBL'], ['Jessica Velez Lopez', 'jessi0208@outlook.es', 'I-TRVB3VBLL3G9'], ['Andres Rojas', 'rofelip03@hotmail.com', 'I-02JP5U56KTH4'], ['Edwin Leonardo Barajas Ramirez', 'edwinb1990@hotmail.com', 'I-077F4R47JNF3'], ['Yaneth gonzalez', 'lilianacaraballo0106@icloud.com', 'I-9AWVBBTFBKV6'], ['Nicolas  Gonzalez', 'nicolasgonzalezroa03@gmail.com', 'I-49YL35GK2MMS'], ['FERNANDO ALBERTO MORENO SEGURA', 'fmoreno@anderson3.com', 'I-TDG15PDT9GN5'], ['alexandra Ramos', 'alexa2009@me.com', 'I-91SPEV2YFLRH'], ['Carolina Becerra Platin', 'cbwallawalla2017@gmail.com', 'I-CR89B01SB184'], ['Jose nelson cruz', 'josenelsonlopezcruz@gmail.com', 'I-WX376MTWDKWF'], ['Christian Taborda', 'christiantaborda@gmail.com', 'I-YT4FE5MPMB79'], ['Yuli Ibarra', 'ypatriciaibarra@outlook.com', 'I-0A9XJ6CH4KV6'], ['JUAN BURBANO', 'js.burbano11@gmail.com', 'I-8N1YDP8GVC47'], ['Fredy Calderon', 'fredywcalderon@hotmail.com', 'I-2E9VH4B4729T'], ['Andres Julian Marin', 'ajms18@hotmail.com', 'I-L0J5GRSTBFXE'], ['Juan Bermudez', 'juanalbertobermudez@gmail.com', 'I-DUX7X2YU161W'], ['Alejandra Medina', 'alemedina10@hotmail.com', 'I-5VHYKG8N1UF2'], ['juan ruiz', 'juanruizl@yahoo.com', 'I-LB5DNSVK2VY1'], ['vivian ruiz', 'varuizp@gmail.com', 'I-WUCNX7G7MGP0'], ['JONATHAN MARTINEZ SUAREZ', 'jota.mar1988@gmail.com', 'I-59XENUYB5L29'], ['froilan perdomo', 'troy0485@gmail.com', 'I-2SLWFKWJBYDD'], ['Juanita Santos', 'juanita.santos.toro@gmail.com', 'I-LC8WNKF19LFW'], ['Sandra Valhuerdi', 'laurabbm2@gmail.com', 'I-YAM91MF17AYK'], ['Fabian Suarez', 'mistercarllc2@gmail.com', 'I-4HHGL4EP6JAV'], ['santiago alzate alfaro', 'santialal95@hotmail.com', 'I-WU4MD23CHRAH'], ['amalia cardenas', 'mayuscc@hotmail.com', 'I-BWWNFH6FL4EA'], ['Miguel Uribe', 'migueluribe01@hotmail.com', 'I-SN2WS5SWFNHY'], ['luis gomez', 'lfgomez64@yahoo.com', 'I-49GVJC5L2GM3'], ['melissa zea', 'melissazea215@gmail.com', 'I-DLGXL0DL4NB1'], ['Francisco Botero', 'francisco_botero@hotmail.es', 'I-GHRWRAG7WMM8'], ['Alfonso Madronero', 'ponchitojulian@gmail.com', 'I-PG8W37R5T4UY'], ['Nayibe Martin Barrera', 'nayiimartiin@icloud.com', 'I-LC5JNFKSAL2E'], ['', 'juanpischannel@gmail.com', '63D56004S5014215H'], ['Manuel Paez Alvarez', 'sgsebastianpaez@gmail.com', 'I-CDRXT0ADUKG0'], ['Diego Mayorga', 'mayorgatennis18@gmail.com', 'I-TX6CJY5G8NXS'], ['Juan Rivera', 'juanf_rivera@yahoo.com', 'I-FMMXU2NECJ26'], ['Rafael garzon', 'garzon427@gmail.com', 'I-HLK1AA467SBT'], ['angello steven vargas sanchez', 'johannadepablo@gmail.com', 'I-FMVEPSDBHWA9'], ['Luna Blush Jewelry', 'rosagranadosc1986@aol.com', 'I-APPVT1TX7FLN'], ['J LUNA BRAVO', 'julianalb1505@gmail.com', 'I-JTV46CLCUBYJ'], ['Daniela Perez', 'danielaperez2629@gmail.com', 'I-6EFHLVGG45Y2'], ['Ricardo Caicedo', 'ricardcaicedo@gmail.com', 'I-JSE82JWSGK43'], ['Ernesto Diaz Cuadro', 'ernestodiazc92@gmail.com', 'I-GDYGF3C67D0M'], ['Duban Lopera', 'buban0703@outlook.es', 'I-JX4VJKB5TFKE'], ['Rosana Barragan', 'rosanabarraganolarte@gmail.com', 'I-2Y2HMJHBME1Y'], ['daniel ortiz', 'danilox9754@gmail.com', 'I-F67V85GHGHRE'], ['alejandra restrepo', 'carlosandres.ktc@gmail.com', 'I-HY3YVPTU612D'], ['', 'juanpischannel@gmail.com', '35D152630G4309114'], ['Daniela Diaz Ossa', 'danieladiazossa@gmail.com', 'I-9W9F6UV2M6GV'], ['juliana giraldo', 'julygiraldo127@hotmail.com', 'I-P8YYS1AAKHNT'], ['Hadelly Rojas', 'Hadellyrc@hotmail.com', 'I-LUR1V3C6K81H'], ['juliana giraldo', 'julygiraldo127@hotmail.com', 'I-17SCYV7HH41U'], ['Diana casto', 'dianap761@hotmail.com', 'I-CH071AE4RJ7V'], ['Maria Cardona', 'mariacardona763@gmail.com', 'I-48X2P5S0LCYJ'], ['', 'juanpischannel@gmail.com', '2PF14942PG574484E'], ['PayPal', 'juanpischannel@gmail.com', '8W731941KR241554P'], ['Nicolas Zamora G', 'nickozamgav@gmail.com', 'I-VGALPX2KG93K'], ['Andres Gonzalez Salcedo', 'andres@gonzalez.com.co', 'I-RDXRPD11C48D'], ['Neil Leano', 'neil.leano95@gmail.com', 'I-1TD0SAPFB7LC'], ['Johor Barandica', 'johor9610@gmail.com', 'I-T6WLKV7TYJBN'], ['Maria Mesa', 'dannyhr11@gmail.com', 'I-77X4NNRNBBXU'], ['Natalia rivera', 'natalisaru@icloud.com', 'I-6V44YU4WSKB8'], ['Jeison  Gomez', 'yfgp23@gmail.com', 'I-8WYCXK49BLX7'], ['NESTOR CRUZ', 'cruzleguizamonnestor@gmail.com', 'I-6F5XYRVRAVNW'], ['ana rios', 'Nanarios1927@hotmail.com', 'I-EMCAB96TTDNS'], ['Ingrid Vargas', 'ingridvargas718@gmail.com', 'I-SC6SRK1Y9P87'], ['Stephany Carmona', 'stefa10290@hotmail.com', 'I-LV3J8E2WSV60'], ['Victor Andres Usuga', 'avictor979@gmail.com', 'I-CGN37GNN6TYY'], ['Gladys Mireya Abarzúa Vergara', 'mav1971usa@gmail.com', 'I-X9T829LE7BGL'], ['WILSON LOPEZ RAMIREZ', 'andres.loopez@outlook.com', 'I-3X4CMWFCG9DP'], ['Valeria Ordóñez', 'vale-pati@hotmail.com', 'I-WHFE36YTMSX2'], ['Claudia Ramírez', 'claudia.ramirez@creagraphics.com.mx', 'I-3MMPN71URJ1B'], ['Bryan Novoa', 'camilonovoa2723@gmail.com', 'I-P90R302GF2DR'], ['Jose Pinto', 'josepintotrespalacios@gmail.com', 'I-1S2MSX67CX33'], ['Luis cabrera', 'malenatrivino89@gmail.com', 'I-S6KRS6UK47PT'], ['Luisa Piñol Andreu', 'luisa.pinol@gmail.com', 'I-Y5XNTJG4916G'], ['Victoria Tanner', 'victoriatanner@bluewin.ch', 'I-XTDM3MMEKP0W'], ['The Color Life', 'dearojas@hotmail.com', 'I-VD28JP94FTR9'], ['Juan david Ramos vargas', 'juandidi91@gmail.com', 'I-G9E0KLAJT6L4'], ['diana murcia', 'dianamurcia83@gmail.com', 'I-GPUTJRMFSWYR'], ['Francia Viviana Hernández Martínez', 'tynny88@hotmail.com', 'I-WY9RSF73B6NL'], ['Maristela Torres', 'maristelat64@yahoo.com', 'I-SXCAE0MCK0M7'], ['paula Garces Cuesta', 'PAULITANIGGITA@HOTMAIL.COM', 'I-2FBDCV38TJBB'], ['Felipe Javier Gonzalez Garces', 'fjgonzalez43@hotmail.com', 'I-71NY0CR3S2LK'], ['Karoll Egea', 'Karolegea@gmail.com', 'I-698JBEWBBRU0'], ['Manuel Castillo', 'manolomayor@hotmail.com', 'I-SN2KSDSWFNHY'], ['sergio novoa', 'sergio199521@gmail.com', 'I-HC2BGMNLDEB9'], ['Andrea Catalina Cruz Vásquez', 'awdrey417@gmail.com', 'I-W0FE3LHMCENJ'], ['Baraquiel Hernández Herrera', 'baraquielhernandezh@gmail.com', 'I-JSEA3TWSGK43'], ['Paola Cano', 'cano.paolita@gmail.com', 'I-CN8N77VKV372'], ['Maria Paula Pena Patino', 'mp.pepa55@gmail.com', 'I-JT6NP9E5F5RU'], ['Edwin Florez', 'alieddiazvarela@hotmail.com', 'I-CTE3FAP7V8HW'], ['Carlos Ricaurte', 'carlosrproductions@gmail.com', 'I-F6X8FRW6V28W'], ['Bladimir Ortega Lopez', 'bladoortlop@outlook.com', 'I-B57AEUF8HUMM'], ['Laura Gonzalez', 'laurangonzalezb@gmail.com', 'I-KC4H5JEVES9J'], ['andres florez', 'pipe.florez1123@gmail.com', 'I-7WKA98N0FGWG'], ['Viviana Rojas', 'andru481@hotmail.com', 'I-MLLNARD0D9TL'], ['Aldair Zuniga Vargas', 'aldairzuva@gmail.com', 'I-BV4UACGTKFU7'], ['Jhoan Cortes', 'leonardocorteslozano@gmail.com', 'I-P32VWTJY47S7'], ['Lina Ariza', 'lina_ariza@outlook.com', 'I-D30WLTNUPPDV'], ['Marcela Guevara', 'marcelaguevara97@gmail.com', 'I-R89MTDWBYUCP'], ['Marizol Perdomo Cano', 'marypc2002@yahoo.com', 'I-7T3DA3JSWX37'], ['Camilo Sarmiento Valderrama', 'camilo.sarmiento.v@me.com', 'I-7C686NHCAM1V'], ['Claudia andrea Guevara egea', 'Claudiaegea6@Gmail.com', 'I-G6YG7A336R5P'], ['Ana Ramirez', 'anaramirez_106_@hotmail.com', 'I-XUJVAKKMLVLS'], ['Claudia andrea Guevara egea', 'Claudiaegea6@Gmail.com', 'I-7U4CUMW0C99V'], ['Felipe Merizalde', 'femericho@hotmail.com', 'I-HW5NHP39C841'], ['Claudia andrea Guevara egea', 'Claudiaegea6@Gmail.com', 'I-L0R2SU4EJGNC'], ['Sebastian Perez Jaramillo', 'sebas.pj@outlook.com', 'I-FRLFN71UJVH9'], ['Juan Campos', 'juancampos1_1@hotmail.com', 'I-47XS90DV5D2W'], ['Andres Cheng', 'nacional82@hotmail.com', 'I-4200K3MMN8XV'], ['Joseph Palacios', 'jf.palacios2109@outlook.com', 'I-019DJE2BN1DN'], ['Sibylle Heelein', 'sheelein@gmail.com', 'I-6CB7U36RN3LL'], ['Andrea Ramirez', 'andreitamirez@hotmail.com', 'I-E9BLFJRM6LWR'], ['TATIANA CORRECHA', 'TATIANA.SEINSURANCEFL@GMAIL.COM', 'I-ECBUXG2ALWCL'], ['Jenny Alejandra Mendivelso Castro', 'jennyaleh28@gmail.com', 'I-E6BXUT2T0HE6'], ['Lorena Vélez', 'Davebra@hotmail.com', 'I-H88XX84PG5TW'], ['Iván Martínez Castellanos', 'elmadero666@gmail.com', 'I-CUUF24CH2B5K'], ['Andrea Winberg', 'winbergandrea@gmail.com', 'I-7JJM3HHF187L'], ['Gustavo Ramirez', 'garj1979@yahoo.com', 'I-44JPCFVJ2US1'], ['Mateo Gallego yepes', 'yepesgallego94@gmail.com', 'I-5SXF0K83187Y'], ['Jose Antonio Lugo Vasquez', 'brigadapalenkito@gmail.com', 'I-8XB6X4Y7JJ2U'], ['Efrain RODRIGUEZ', 'erp927@gmail.com', 'I-XTD935MEKP0W'], ['maria morales', 'marisabel_1102@outlook.es', 'I-B1XXCELXEYR9'], ['Maria Jose Rivera Aristizábal', 'mariajota_2@hotmail.com', 'I-UJHVFB9C21Y1'], ['Virginia Torres', 'gt_virginia@yahoo.co.uk', 'I-B0AYPEBM28N3'], ['Johana Rios solano', 'johanastar01@gmail.com', 'I-R2NWWMP9PSWS'], ['Cristhian Garzon', 'cristhianf.garzon@gmail.com', 'I-WW5JDP48KFL1'], ['Travis Keale', 'kealeboy42@gmail.com', 'I-1V5N5KELX383'], ['juan enciso', 'juan.enciso@rbc.com', 'I-DYGGC1KVRKFB'], ['Diana Cañizares Herrera', 'dianiya88@gmail.com', 'I-41AG3FXHMXLJ'], ['Jorge Sanchez', 'marios90210@hotmail.com', 'I-XLGH2LT06F55'], ['Juan miguel Posada pelayo', 'Juanchopp98@icloud.com', 'I-9MKK41GLG00D'], ['yessica tascon', 'ytascon87@gmail.com', 'I-VMRE03FAUHLG'], ['180degreesmedellin.com', 'deividrendon1@gmail.com', 'I-EVT7H17SLW2Y'], ['Ivon Maritza Diaz Moreno', 'algaby82@hotmail.com', 'I-RF4XTC1HR9WL'], ['juan pablo castillo martin', 'castillojuank10az@gmail.com', 'I-FH0P2E7T9EYF'], ['Laura Quevedo', 'Lauraquevedo1202@gmail.con', 'I-W679R90JR1G6'], ['Cindy Galeano', 'vanessagaleanoing@gmail.com', 'I-3FKSKESGX2VR'], ['Oscar Velasquez', 'osclarinet@gmail.com', 'I-2HUR60X57SRA'], ['Daniela Garcia', 'danielitatoon@hotmail.com', 'I-BLL085MJG9D9'], ['LIA SALGADO', 'LIAROSIS17@GMAIL.COM', 'I-HJ4AL0LHMPCX'], ['Mauricio Saba', 'maurosaba@hotmail.com', 'I-FLDXSP4R9E6D'], ['Gloria Rincon', 'gloria1468@hotmail.com', 'I-LVS60XBD3D4A'], ['carlos rueda', 'ruedacarl@sjhmc.org', 'I-2KFG46295GVV'], ['Andres Giraldo', 'standre95@icloud.com', 'I-9K0VE9TS8RAK'], ['cindy johanna figueroa ospina', 'cindyo87@yahoo.com', 'I-54YNCDHGGX96'], ['Sonia Perez Caceres', 'sonia.perez.caceres.de@gmail.com', 'I-4VW52X75L7TN'], ['Natalia Osorio', 'natalia.corteso@outlook.com', 'I-TWCREB91PMSF'], ['rodrigo sierra', 'damian-sierra@hotmail.es', 'I-5V5617YC2WT1'], ['ENNGIE RODRIGUEZ CASALLAS', 'ENNGIERODRIGUEZ@GMAIL.COM', 'I-M89NMFXX7K6P'], ['jhonnatan rueda', 'limpbi17_27@hotmail.com', 'I-DC2FR93HYAA3'], ['Laura Alcaraz', 'laura.alcaraz1@gmail.com', 'I-Y21JS7W1U3JX'], ['Andrés Marín', 'andreslaverdemarin@gmail.com', 'I-D3EX494TY7BH'], ['Mauricio mottoa lopez', 'maohlopez28@gmail.com', 'I-C30TNN9DKX2K'], ['jhon didier isaza castrillon', 'jhon.dc@hotmail.com', 'I-YNX4LWXRTR98'], ['daniel albeiro amortegui diaz', 'danielalbeiro180@gmail.com', 'I-FXVURN4D0JLX'], ['JAIME TORRES', 'jaimetorresgamba@gmail.com', 'I-HJ88MEU65WWB'], ['Ana Lucia Ballen Pinzon', 'alula27@hotmail.com', 'I-W3WFTNTKN24P'], ['Johanna Rendón', 'johare.r@hotmail.com', 'I-8XDARV48VXLH'], ['Gusatvo Javier Silvera Marin', 'netflow84@gmail.com', 'I-9ECYNHU20EGG'], ['Lina Jael Benitez Ramos', 'ljahell@gmail.com', 'I-55WDWXV8NEBN'], ['Diana Mápura', 'dianitapte@gmail.com', 'I-R6LWHEXGUENX'], ['MARCELA MAYORGA', 'marcelamayorga2@hotmail.com', 'I-R8XD3MMTPMJT'], ['Gisella Montes', 'alexgis17@msn.com', 'I-3TC49HT1MFUX'], ['PayPal', 'juanpischannel@gmail.com', '6SS396603B120215H'], ['jeisson hernandez', 'jeisonhernandez651@gmail.com', 'I-PBTR6KF7T7G1'], ['Leonardo Nuñez Mora', 'lleonardonm@hotmail.com', 'I-E17FH6RW1SAS'], ['Angie Catalina Hernandez Vallejo', 'cata-hernandez1@hotmail.com', 'I-06KW21K3AFLM'], ['Andreas Sprissler', 'andreas.sprissler@hotmail.com', 'I-LGM4AKWF2VNM'], ['Marlyn Sandoval', 'marlyn.sandoval9@gmail.com', 'I-S8BDTS0B5W69'], ['NAZLY TELLO', 'directormarketing@outlook.com', 'I-FHLACTCLHU30'], ['Angie Catalina Hernandez Vallejo', 'cata-hernandez1@hotmail.com', 'I-WJKAY600BBC5'], ['Gustavo Adolfo Cruz Diaz', 'gcruz2012@gmail.com', 'I-PEERNMSCBVYP'], ['Alvaro Bertorelli', 'alvaro.bertorelli@yahoo.co.uk', 'I-6UFV59VF26GY'], ['xiuxia zeng', 'el.china-89-@hotmail.com', 'I-5MY9G19MWFE8'], ['Francy Paola Moreno Rodríguez', 'paolam0624@hotmail.com', 'I-8XHU9FG37NFP'], ['claudia castillo', 'abonova1236@hotmail.com', 'I-YF57820EH5HB'], ['paula machado', 'pamachadito@hotmail.com', 'I-A2WYX81UGPCK'], ['Marta Escobar', 'marta_ospina@aol.com', 'I-SCRH2J185CLN'], ['Carmen Cristina Vizzi', 'ccvizzi@hotmail.com', 'I-5R8N7C11091J'], ['Pilar Andrea Rosas Briceño', 'pilandros@gmail.com', 'I-GLW21AFBB56E'], ['angie sanchez', 'angiecal17@hotmail.com', 'I-5K46CA53BDPR'], ['Julian Lozano', 'kjsolutions2018@outlook.com', 'I-X3AFAWC7VHUS'], ['Yenny Vargas', 'yevapa@yahoo.com', 'I-2FB9KJUPJ8J7'], ['yenni mora', 'yenni.mora@gmail.com', 'I-0UFNPBDNDH1U'], ['Darly Castrillón', 'castrillondarly@gmail.com', 'I-Y0E9F66NMPS4'], ['Marialejandra Serpa', 'alejita0612@gmail.com', 'I-0N7RBDDHT6B0'], ['Daniel Lopez Herrera', 'lopezxdani@hotmail.com', 'I-5VDY2DU0WT5R'], ['Dana Mora', 'danamora@hotmail.com', 'I-MUU5KLXHL1FL'], ['oscar jimenez', 'oskarjymenez@gmail.com', 'I-KXMNUGEG4CH9'], ['Yesika Ximena Peña Nova', 'giselnico@gmail.com', 'I-UXH27G7NGBYR'], ['isometrik Corp', 'isometrikcorp@gmail.com', 'I-DVV9NMMDVJ5B'], ['Valentina Gutierrez', 'valentinagutierrezh1020@gmail.com', 'I-VFNU4AR8B0LY'], ['jose h castaneda', 'Hcastaneda40@cue.edu.co', 'I-5VHVJ48P1UF2'], ['Daniel Parra', 'daniel.parra.carreno@gmail.com', 'I-G7E2GSX0G8VS'], ['Rolando Robles', 'rrv7925@yahoo.com.ar', 'I-VVUKGFB9XPFT'], ['Manuel Arturo Izquierdo Pena', 'aizquier@gmail.com', 'I-NAN1LDGWG69A'], ['Felipe Perez', 'felipeperezheredia@hotmail.com', 'I-Y0ECE26NMPS4'], ['Diana Diaz', 'diludiazmorales@gmail.com', 'I-V7J2BKKWYYVH'], ['Juliana Onate Berrocal', 'jonatebe@gmail.com', 'I-VYGU6B4NN6CD'], ['William Verde', 'wavv80@gmail.com', 'I-P8YNRDABKHNT'], ['Carlos Andres López Morales', 'devops.carlos@gmail.com', 'I-9HKKY923M89J'], ['JUAN B Uribe', 'uriestra.jb@gmail.com', 'I-NH314431XAVJ'], ['Lina Hacmon', 'linahakmon@gmail.com', 'I-UTP6HE6GH894'], ['Sebastian Salazar', 'juansebastian2723455@gmail.com', 'I-4YJWEYX3DYHM'], ['Daniet Delgado', 'danietdelgado@gmail.com', 'I-AS8WJK09LYUJ'], ['Higo Armando Proas Lozano', 'priashugo@Gmail.com', 'I-KB1HD4MX6GWF'], ['Valentina Jordan', 'valen-jor@hotmail.com', 'I-ATAY5ED27W4H'], ['Francisco Castañeda', 'fjcastanedag1904@gmail.com', 'I-N2UE7DVNCCGH'], ['natalia castano', 'ncastcal@gmail.com', 'I-DT1XBR9M93NY'], ['Higo Armando Proas Lozano', 'priashugo@Gmail.com', 'I-E0SMSA6YAAJ2'], ['diana acosta', 'pa.nanis@gmail.com', 'I-00P923UU4JUR'], ['Fran Esteban FERNANDEZ DEVIA', 'efernandezdevia@outlook.be', 'I-JW7L6XK54H8W'], ['Alejandro Bacot', 'alejandrobacot@gmail.com', 'I-6NF1TW1FTN96'], ['Laura Bohorquez', 'laubohorquez9127@icloud.com', 'I-MDUFS8PTBKW0'], ['Paola Palacios Piedrahita', 'paolandrea.pp@outlook.com', 'I-CN4XVR3XE1CP'], ['Estefany Misas diaz', 'tefa.misas@gmail.com', 'I-8537P0FSXHYN'], ['David Nevarez', '1davidnevarez@gmail.com', 'I-UURU7PFBPNY2'], ['Andres Reyes', 'andresredi.org@gmail.com', 'I-VE3UM38MUJ7H'], ['Laura Reina', 'laurarrp5@gmail.com', 'I-9HKTYM24M89J'], ['Kelly Lopez', 'lopezkell@gmail.com', 'I-G1SH080AVLTR'], ['julian betancourth', 'beta.j.andres@gmail.com', 'I-ECP2XU0TYX88'], ['Walter Inocensio Alfonso', 'fabian.tutata@gmail.com', 'I-FBEA5BVLKXSG'], ['Diego Uribe', 'djcgroup@bellsouth.net', 'I-GY9D6LXBHU2A'], ['dennis gomez', 'arigom7@gmail.com', 'I-HBM5NTVDCNRE'], ['Carolina Cardona', 'carolina.cardonab@gmail.com', 'I-X1U1P2WS5PE8'], ['patricia sepulveda', 'pattysv517@gmail.com', 'I-L5REDL2BNXTG'], ['Pablo Arturo Parra Erazo', 'colombiano.danmark@gmail.com', 'I-HY96NNVW6734'], ['Silvia Duarte Sanmiguel', 'silvilongkiss@hotmail.com', 'I-BXFL1FKT1D7P'], ['Nicolas Fernández Hernandez', 'stephanybernalh@hotmail.com', 'I-D58S3KHTJCC0'], ['marvin lopez', 'marvinlopez803@gmail.com', 'I-P990VV4JLFY4'], ['Mauricio mottoa lopez', 'maohlopez28@gmail.com', 'I-W65D6N1S1FBH'], ['PayPal', 'juanpischannel@gmail.com', '5JV11296CK4441916'], ['juan garcia', 'jkamilo20@yahoo.com', 'I-AXNJML9756H4'], ['PayPal', 'juanpischannel@gmail.com', '04N72570PK152441B'], ['Ivan Gonzalez', 'ivangonzalezv94@gmail.com', 'I-L126PBKL98LK'], ['Sttefany Paola Bolaños Romero', 'sttefany.bolanos@gmail.com', 'I-E89TLBFFW6FG'], ['maria alvarez', 'camialvarez0722@hotmail.com', 'I-N9S54XK6PA66'], ['Milena Mojica', 'gordis2422@hotmail.com', 'I-N8KRU1KUV6NF'], ['Yizza Paola Gomez', 'paolal735@gmail.com', 'I-1MDT0P0BWN0N'], ['Yenny Vergara', 'yeverpi1209@gmail.com', 'I-9KSSMXTPUJF8'], ['Leonardo Hernandez', 'Leoher1@hotmail.com', 'I-6874YAJBE5JT'], ['Christian Carbonnel López', 'ebay@mochilerofivestars.com', 'I-7M08DG9R3DL3'], ['daniela gutierrez', 'danielagudiz_94@hotmail.com', 'I-BSNH3350XH8X'], ['Andrea Duarte', 'andreapd55@gmail.com', 'I-4LPSH868NHTW'], ['Vanessa Vivas', 'vanesa.viivas@icloud.com', 'I-LKP6S8JYTBL3'], ['Andres Falla', 'falla_andres@hotmail.com', 'I-5NN4KMCF297V'], ['Daniel Duque Villarreal', 'danielduque@u.northwestern.edu', 'I-RM8VPLUFH2YG'], ['johann bejarano', 'edbego@gmail.com', 'I-RD0BMDUS6W7N'], ['SANTIAGO TORRIJOS UYABAN BRIAUD', 'torrijossantiago@gmail.com', 'I-VH8WFVNJUSR8'], ['james valencia', 'tefa0106@gmail.com', 'I-E7GSXUDER78X'], ['martha castañeda camacho', 'Jhonatanvalencia03@gmail.com', 'I-ECBH0R2CLWCL'], ['Manuela Diaz', 'manueladr13@hotmail.com', 'I-C6S08PY2YPBP'], ['jeysson pineda', 'andrea.gaby2123@gmail.com', 'I-V4MNCBCJ70DP'], ['Michael Sarmiento', 'digilandstudio@yahoo.com', 'I-VG34P9M5E5CL'], ['jair acevedo', 'chelen007@yahoo.com', 'I-TMC54RPHPW8F'], ['Giovanna Vargas Quintero', 'giovannavarquin@yahoo.es', 'I-AMMJ4V3MGBSC'], ['jeysson pineda', 'andrea.gaby2123@gmail.com', 'I-H214HMJUAXEK'], ['alexi sierra andrade', 'bryanhivac@gmail.com', 'I-5M78J6WWHG55'], ['Catalina Rengifo Duque', 'catalina.rengifo.d@outlook.com', 'I-R1WBWNB06Y9J'], ['juan mojica', 'jcmojica03@gmail.com', 'I-WW0GJUF2P0MK'], ['Maria Aristizabal', 'valentina.granados.093@gmail.com', 'I-DCCUXMX354YA'], ['Warren Benavides', 'wbenavides@outlook.com', 'I-91SKLC21FLRH'], ['Laura Mora New York Corp.', 'laura@lauramorany.com', 'I-K5VAL08XN69L'], ['daniel martinez', 'parcerito1@gmail.com', 'I-VYR0Y03U3UW4'], ['Pro Builder Manufacture Solutions', 'jppppppp@hotmail.com', 'I-3L5HE42HFLV3'], ['Julietas Hope', 'julietashope@gmail.com', 'I-D2SLAF10JGTS'], ['Luz Maria Tamayo Merino', 'luzma1204@hotmail.com', 'I-37BM6KLL7DJJ'], ['ELDY MUÑOZ', 'eldy0712@gmail.com', 'I-MK8876HLV9XK'], ['Alejandra Maldonado', 'aemmaldon@gmail.com', 'I-R0TXT02686S5'], ['Julieth K Machuca Luna', 'julimoon29@gmail.com', 'I-SLN1EF3U4SG7'], ['Dani Orrego', 'daniel.1994.orrego@hotmail.com', 'I-SU6RLSXYRFED'], ['JANETH CORDOBA', 'maffy010@hotmail.com', 'I-23R7853WUUHW'], ['matilde martinez', 'matildeconsuelo@hotmail.com', 'I-ATA452D27W4H'], ['Daniela Perez', 'danielaperez2629@gmail.com', 'I-AE1DDFUFWDRE'], ['Jhon Lores', 'johnlores@icloud.com', 'I-0LSBAUBY6UR6'], ['kevin ayala', 'ayala.kevin1015@gmail.com', 'I-FE76W9G9PTA3'], ['Georgina Alvarez Juarez', 'goga24ghma@gmai.com', 'I-H1221K8YMRC0'], ['JULIO MONROY MORALES', 'juliomonroychile@gmail.com', 'I-ENCCH1BG3RP4'], ['Francis Schambach', 'schambachson@gmail.com', 'I-KHYU5M4VPTYA'], ['julian guerrero', 'juliancho1002@aol.com', 'I-VXY28153U6R5'], ['Julian Diaz', 'jdiaz_30@hotmail.com', 'I-5HD6DFN2L3MG'], ['Josué Rios', 'rios73150@gmail.com', 'I-34F779TEAJ1Y'], ['Julia Sarria', 'julia.sarria2012@gmail.com', 'I-LVS01NBA3D4A'], ['pedro Contreras', 'pefe2209@gmail.com', 'I-F2SUKG3PW2NX'], ['Erika lorena Rengifo lopez', 'erikalo.rengifo@gmail.com', 'I-GN6L7P83H5NC'], ['Jhonier Paull Reasco Marinez', 'paullrmarinez@gmail.com', 'I-PWLN28G38Y5U'], ['daniela buitrago', 'Dannypaola@hotmail.com', 'I-F1PMCP0F0FDE'], ['Juan Alvarez', 'ad3juan@hotmail.com', 'I-TYSTC8GY5PJN'], ['Sistemas jp', 'juanp27102710@gmail.com', 'I-XC6KWYGKWU23'], ['Daniel Pérez', 'truji-91@hotmail.com', 'I-A58DBDHD25Y4'], ['Camila Quiroga', 'camila29quirogac@hotmail.com', 'I-KV2RN4EEPPCD'], ['Andres Reyes', 'andresreyesgomezpepo@gmail.com', 'I-3KPHHTM3GSCE'], ['William Bareno', 'wbareno@hotmail.com', 'I-SCRJ36165CLN'], ['Diego Hernandez', 'diegogurula@gmail.com', 'I-NKTY7437YVV0'], ['pamela mora', 'pammora.1003@gmail.com', 'I-RJW8EG6WB25D']];
        $faltantes = ['1VW126127G9834719', '26N161499C458321Y', '27B61343R0117862U', '2AN031191N753204T', '2PF14942PG574484E', '35D152630G4309114', '63D56004S5014215H', 'I-06KW21K3AFLM', 'I-0AU4LBFVD15J', 'I-0X225YXSEKWT', 'I-17SCYV7HH41U', 'I-1EWBKYE60PVL', 'I-2BWCWRM4TVHN', 'I-2GBH20DEC1YW', 'I-3CL0S1H4M0BL', 'I-3EG57N0TNERA', 'I-5DS8ERW9EDF4', 'I-5U8S4HLL4JDA', 'I-66BRMBWBGRXD', 'I-6BXLAEWUYRWU', 'I-6EFHLVGG45Y2', 'I-75S8SDSMGUJP', 'I-8TMX9XUW6XH8', 'I-9P7FCE1JS1ET', 'I-ACNXATRGHCJF', 'I-ARASRMD530VF', 'I-AS8WJK09LYUJ', 'I-C30TNN9DKX2K', 'I-CFP3A1PA378D', 'I-DF1S1CWRBSKC', 'I-ECP2XU0TYX88', 'I-FAMKEHKDXRBL', 'I-FLDXSP4R9E6D', 'I-FMMXU2NECJ26', 'I-FXMTH7UL39XL', 'I-G1SH080AVLTR', 'I-G2GVMH92PXEW', 'I-G6YG7A336R5P', 'I-GN6L7P83H5NC', 'I-HMX402S59GB0', 'I-J3PYUX2V59P0', 'I-J53K1A50LUKV', 'I-J5XXHHWHP28G', 'I-K9UPLD8KAU00', 'I-KB1HD4MX6GWF', 'I-KPCL41CJSNXP', 'I-LF099W8J5066', 'I-LK023YGAMMU5', 'I-LUR1V3C6K81H', 'I-LYDU7YWA52J0', 'I-NCHJ0X5LVRNC', 'I-P7U0SS7TTF19', 'I-P8YYS1AAKHNT', 'I-PNHA3XRW14F4', 'I-RPW3GSU92TKD', 'I-TGNG1VGX2L0V', 'I-UF78H0R4A5WE', 'I-V4MNCBCJ70DP', 'I-W65D6N1S1FBH', 'I-WEX9N7A2VK3N', 'I-WPPPW051RR11', 'I-X7TL9J9HPNU2', 'I-XJ1XVUKY1YRX', 'I-YGTK87P1EG4H'];
        $creadas = [];
        $sinusuario = [];
        $em = $this->getDoctrine()->getManager();
        foreach ($faltantes as $id_paypal) {
            foreach ($tuplas as $elem) {
                if (array_key_exists(2, $elem)) {
                    if ($elem[2] == $id_paypal) {
                        $user = $this->getDoctrine()->getRepository(Usuario::class)->findOneBy(array('email' => $elem[1]));
                        $elem[3] = 0;
                        if ($user) {
                            $elem[3] = $user->getId();
                            /*
                            $news = new Suscripcion();
                            $news->setUsuario($user);
                            $news->setIdPaypal($elem[2]);
                            $news->setEmail($elem[1]);
                            $news->setPlanId('P-9M267883HE569001LL2AUACY');
                            $news->setStatus('manualmente_creada');
                            $news->setCreatedAt(new \DateTime());
                            $news->setActiva(1);

                            $em->persist($news);
                            $em->flush();
                            $this->qi->saveFire($news);

                            $pago = new PagoSuscripcion();
                            $pago->setSuscripcion($news);
                            $pago->setState('completed');
                            $pago->setCreatedAt(new \DateTime());
                            $pago->setMoneda('USD');
                            $pago->setMonto('3');
                            $pago->setIdPaypal('');
                            $em->persist($pago);
                            $em->flush();
                            $this->qi->saveFire($pago);

                            $elem[4]=$news->getId();
*/
                            array_push($creadas, $elem);

                        } else {
                            array_push($sinusuario, $elem);
                        }

                    }
                }
            }
        }
        return $this->render('termino_politica/paypal.html.twig', [
            'usuarios' => $creadas,
            'sinusuarios' => $sinusuario
        ]);
    }

}
