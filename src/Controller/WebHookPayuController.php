<?php

namespace App\Controller;

use App\Entity\PagoSuscripcion;
use App\Entity\PayuLog;
use App\Entity\Suscripcion;
use App\Service\QI;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WebHookPayuController extends AbstractController
{
    private $qi;
    private $userManager;

    public function __construct(QI $qi, UserManagerInterface $userManager)
    {
        $this->qi = $qi;
        $this->userManager = $userManager;
    }

    /**
     * @Route("/webhook/payu", name="web_hook_payu")
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $log = new PayuLog();
        $log->setData(json_encode($data));
        $em->persist($log);
        $em->flush();
        $reference_sale = $data['reference_sale'];
        $reference_recurring_payment = $data['reference_recurring_payment'];
        $susId = explode('_', $reference_recurring_payment)[0];
        //dump($susId);
        $suscripcion = $this->getDoctrine()->getRepository(Suscripcion::class)->findOneBy(array('id_payu' => $susId));
        //dd($suscripcion);
        $state_pol = $data['state_pol'];
        $ex = explode('-', $reference_sale);
        $pagoId = [];
        for ($i = 0; $i < count($ex) - 1; $i++) {
            array_push($pagoId, $ex[$i]);
        }
        $pagoId = join('-', $pagoId);
        $pago = $em->getRepository(PagoSuscripcion::class)->findOneBy(array("id_payu" => $data['transaction_id']));
        $persist = false;
        if (!$pago) {
            $persist = true;
            $pago = new PagoSuscripcion();
        }
        $pago->setIdPayu($data['transaction_id']);
        $pago->setOrderId($data['reference_sale']);
        $pago->setMonto($data['value']);
        $pago->setMoneda($data['currency']);
        $pago->setCus($data['cus']);
        $pago->setReferencePol($data['reference_pol']);
        $fecha = new \DateTime($data['operation_date']);
        $pago->setFecha($fecha);
        $pago->setState($data['response_message_pol']);
        $pago->setSuscripcion($suscripcion);

        if ($persist) {
            $em->persist($pago);
        }
        $em->flush();
        $this->qi->saveFire($pago);
        $activa = $state_pol == 4;
        if ($activa) {
            $valida = new \DateTime();
            $valida = new \DateTime(date('Y-m-d', strtotime("+2 days", strtotime("+" . $suscripcion->getDuracion() . " months", $valida->getTimestamp()))));
            //dd($valida);
            $suscripcion->setValidaHasta($valida);
        }
        $suscripcion->setActiva($activa);
        $em->persist($suscripcion);
        $em->flush();
        $suscripcion->setEmail("");
        $suscripcion->setDuracion(0);
        $suscripcion->setIdPayu("");
        $suscripcion->setPayerId("");
        $suscripcion->setIdPaypal("");
        $suscripcion->setPlanId("");
        $suscripcion->setUsuario(null);
        $suscripcion->setValidaHasta(null);
        $this->qi->saveFire($suscripcion);

        return new Response('Hello Payu', Response::HTTP_OK);
    }


    /**
     * @Route("/payu-confirmacion",name="pagar_payu_confirmacion", methods={"POST"})
     */
    public function confirmacionPayu(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $log = new PayuLog();
        $full_url = serialize($request->request->all());
        $log->setData($full_url);
        $em->persist($log);
        $em->flush();


        $ApiKey = $this->qi->getSetting('payu_apiKey');
        $merchant_id = $this->qi->getSetting('payu_merchantId');
        $referenceCode = $request->request->get('reference_sale');

        $TX_VALUE = $request->request->get('value');
        $New_value = number_format($TX_VALUE, 1, '.', '');
        $currency = 'COP';
        $transactionState = $request->request->get('state_pol');

        $firma_cadena = "$ApiKey~$merchant_id~$referenceCode~$New_value~$currency~$transactionState";
        $firmacreada = md5($firma_cadena);
        $firma = $request->request->get('sign');
        if (strtoupper($firma) == strtoupper($firmacreada)) {

            $transactionState = $request->request->get('state_pol');
            if ($transactionState == 4) {
                $estadoTx = "APROBADA";
            } else if ($transactionState == 6) {
                $estadoTx = "RECHAZADA";
            } else if ($transactionState == 104) {
                $estadoTx = "PENDIENTE";
            } else if ($transactionState == 7) {
                $estadoTx = "ERROR";
            } else {
                $estadoTx = "ERROR";
            }

            $referenceCode = $request->request->get('reference_sale');
            $arr_ref = explode('_', $referenceCode);
            $id_sus = end($arr_ref);

            $suscripcion = $this->getDoctrine()->getRepository(Suscripcion::class)->find($id_sus);
            $pago = $em->getRepository(PagoSuscripcion::class)->findOneBy(array("id_payu" => $request->request->get('transaction_id')));
            $persist = false;
            if (!$pago) {
                $persist = true;
                $pago = new PagoSuscripcion();
            }
            $pago->setIdPayu($request->request->get('transaction_id'));
            $pago->setOrderId($request->request->get('reference_sale'));
            $pago->setMonto($request->request->get('value'));
            $pago->setMoneda($request->request->get('currency'));
            $pago->setCus($request->request->get('cus'));
            $pago->setReferencePol($request->request->get('reference_pol'));
            $fecha = new \DateTime($request->request->get('transaction_date'));
            $pago->setFecha($fecha);
            $pago->setState($request->request->get('response_message_pol'));
            $pago->setSuscripcion($suscripcion);

            if ($persist) {
                $em->persist($pago);
            }
            $em->flush();

            if ($transactionState == 4) {
                $suscripcion->setActiva(1);
                $em->persist($suscripcion);
                $em->flush();
            }
            return new Response('Hello Payu', Response::HTTP_OK);
        } else {
            return new Response('Hello Payu', Response::HTTP_OK);
        }
        return new Response('Hello Payu', Response::HTTP_OK);
    }

}
