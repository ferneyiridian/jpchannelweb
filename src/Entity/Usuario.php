<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioRepository")
 */
class Usuario extends BaseUser
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apellido;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $celular;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;




    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebookId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $instagramId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $terminos;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $mensajes;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $whatsapp;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $id_payu;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tarjeta", mappedBy="usuario")
     */
    private $tarjetas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Suscripcion", mappedBy="usuario")
     */
    private $suscripciones;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sessionId;


    public function __construct()
    {
        $this->direcciones = new ArrayCollection();
        $this->created_at = new \DateTime();
        $this->favoritos = new ArrayCollection();
        $this->tarjetas = new ArrayCollection();
        $this->suscripciones = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }
        if($this->roles == null)
            $this->roles = [];
        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(?string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }


    public function getCelular(): ?string
    {
        return $this->celular;
    }

    public function setCelular(?string $celular): self
    {
        $this->celular = $celular;

        return $this;
    }

    public function getProviderData(): ?string
    {
        return $this->provider_data;
    }

    public function setProviderData(string $provider_data): self
    {
        $this->provider_data = $provider_data;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * @param mixed $fechaNacimiento
     */
    public function setFechaNacimiento($fechaNacimiento): void
    {
        $this->fechaNacimiento = $fechaNacimiento;
    }




    public function __toString()
    {
        return $this->email;// $this->nombre . " " . $this->apellido;
    }

    public function getZona(): ?string
    {
        return $this->zona;
    }

    public function setZona(?string $zona): self
    {
        $this->zona = $zona;

        return $this;
    }



    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function setFacebookId(?string $facebookId): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(?string $googleId): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    public function getInstagramId(): ?string
    {
        return $this->instagramId;
    }

    public function setInstagramId(?string $instagramId): self
    {
        $this->instagramId = $instagramId;

        return $this;
    }

    public function getTerminos(): ?bool
    {
        return $this->terminos;
    }

    public function setTerminos(?bool $terminos): self
    {
        $this->terminos = $terminos;

        return $this;
    }

    public function getMensajes(): ?bool
    {
        return $this->mensajes;
    }

    public function setMensajes(?bool $mensajes): self
    {
        $this->mensajes = $mensajes;

        return $this;
    }

    public function getWhatsapp(): ?bool
    {
        return $this->whatsapp;
    }

    public function setWhatsapp(?bool $whatsapp): self
    {
        $this->whatsapp = $whatsapp;

        return $this;
    }

    public function getIdPayu(): ?string
    {
        return $this->id_payu;
    }

    public function setIdPayu(?string $id_payu): self
    {
        $this->id_payu = $id_payu;

        return $this;
    }

    /**
     * @return Collection|Tarjeta[]
     */
    public function getTarjetas(): Collection
    {
        return $this->tarjetas;
    }

    public function addTarjeta(Tarjeta $tarjeta): self
    {
        if (!$this->tarjetas->contains($tarjeta)) {
            $this->tarjetas[] = $tarjeta;
            $tarjeta->setUsuario($this);
        }

        return $this;
    }

    public function removeTarjeta(Tarjeta $tarjeta): self
    {
        if ($this->tarjetas->contains($tarjeta)) {
            $this->tarjetas->removeElement($tarjeta);
            // set the owning side to null (unless already changed)
            if ($tarjeta->getUsuario() === $this) {
                $tarjeta->setUsuario(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Suscripcion[]
     */
    public function getSuscripciones(): Collection
    {
        return $this->suscripciones;
    }

    public function addSuscripcione(Suscripcion $suscripcion): self
    {
        if (!$this->suscripciones->contains($suscripcion)) {
            $this->suscripciones[] = $suscripcion;
            $suscripcion->setUsuario($this);
        }

        return $this;
    }

    public function removePago(Suscripcion $suscripcion): self
    {
        if ($this->suscripciones->contains($suscripcion)) {
            $this->suscripciones->removeElement($suscripcion);
            // set the owning side to null (unless already changed)
            if ($suscripcion->getUsuario() === $this) {
                $suscripcion->setUsuario(null);
            }
        }

        return $this;
    }

    public function getLinkSuscripciones(){
        return '<a href="/admin_jpchannel/?entity=Suscripcion&action=list&usuario='.$this->getId().'" target="_blank">Ver</a>';
    }

    public function getSuscripcionesActivas()
    {

        $cant= 0;
        foreach ($this->suscripciones as $s){
            if($s->getActiva()){
                $cant++;
            }
        }
        return $cant;
    }

    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    public function setSessionId(?string $sessionId): self
    {
        $this->sessionId = $sessionId;

        return $this;
    }

}
