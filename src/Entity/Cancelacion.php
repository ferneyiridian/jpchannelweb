<?php

namespace App\Entity;

use App\Repository\CancelacionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CancelacionRepository::class)
 */
class Cancelacion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\OneToOne(targetEntity=Suscripcion::class, inversedBy="cancelacion", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $suscripcion;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $respuesta;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $error;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getSuscripcion(): ?Suscripcion
    {
        return $this->suscripcion;
    }

    public function setSuscripcion(Suscripcion $suscripcion): self
    {
        $this->suscripcion = $suscripcion;

        return $this;
    }

    public function getRespuesta(): ?string
    {
        return $this->respuesta;
    }

    public function setRespuesta(?string $respuesta): self
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    public function getError(): ?string
    {
        return $this->error;
    }

    public function setError(?string $error): self
    {
        $this->error = $error;

        return $this;
    }
}
