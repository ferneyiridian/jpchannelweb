<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriaBlogRepository")
 */
class CategoriaBlog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $imagen;

    /**
     * @ORM\Column(type="integer")
     */
    private $orden = 1;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = true;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SubCategoriaBlog", mappedBy="categoriaBlog")
     */
    private $subCategoriaBlogs;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_es;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_en;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_fr;

    public function __construct()
    {
        $this->subCategoriaBlogs = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'nombre_'.$locale;
        return $this->{$field};
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getImagen(): ?Image
    {
        return $this->imagen;
    }

    public function setImagen(?Image $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return Collection|SubCategoriaBlog[]
     */
    public function getSubCategoriaBlogs(): Collection
    {
        return $this->subCategoriaBlogs;
    }

    public function addSubCategoriaBlog(SubCategoriaBlog $subCategoriaBlog): self
    {
        if (!$this->subCategoriaBlogs->contains($subCategoriaBlog)) {
            $this->subCategoriaBlogs[] = $subCategoriaBlog;
            $subCategoriaBlog->setCategoriaBlog($this);
        }

        return $this;
    }

    public function removeSubCategoriaBlog(SubCategoriaBlog $subCategoriaBlog): self
    {
        if ($this->subCategoriaBlogs->contains($subCategoriaBlog)) {
            $this->subCategoriaBlogs->removeElement($subCategoriaBlog);
            // set the owning side to null (unless already changed)
            if ($subCategoriaBlog->getCategoriaBlog() === $this) {
                $subCategoriaBlog->setCategoriaBlog(null);
            }
        }

        return $this;
    }

    public function getNombreEs(): ?string
    {
        return $this->nombre_es;
    }

    public function setNombreEs(?string $nombre_es): self
    {
        $this->nombre_es = $nombre_es;

        return $this;
    }

    public function getNombreEn(): ?string
    {
        return $this->nombre_en;
    }

    public function setNombreEn(?string $nombre_en): self
    {
        $this->nombre_en = $nombre_en;

        return $this;
    }

    public function getNombreFr(): ?string
    {
        return $this->nombre_fr;
    }

    public function setNombreFr(?string $nombre_fr): self
    {
        $this->nombre_fr = $nombre_fr;

        return $this;
    }
}
