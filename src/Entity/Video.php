<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VideoRepository")
 */
class Video
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoriaVideo", inversedBy="videos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image",cascade={"persist"})
     */
    private $imagen;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titulo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $duracion;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $id_vimeo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $es_gratis;

    /**
     * @ORM\Column(type="integer")
     */
    private $orden;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $visible;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $tags;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $id_preview_vimeo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $destacado;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $aws_url;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $aws_preview_url;

    public function __construct()
    {
        $this->updated_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->getTitulo();
    }

    /**
     * @Groups({"searchable"})
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }



    public function getCategoria(): ?CategoriaVideo
    {
        return $this->categoria;
    }

    public function setCategoria(?CategoriaVideo $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getImagen(): ?Image
    {
        return $this->imagen;
    }

    public function setImagen(?Image $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * @Groups({"searchable"})
     */
    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getDuracion(): ?string
    {
        return $this->duracion;
    }

    public function setDuracion(string $duracion): self
    {
        $this->duracion = $duracion;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getIdVimeo(): ?string
    {
        return $this->id_vimeo;
    }

    public function setIdVimeo(string $id_vimeo): self
    {
        $this->id_vimeo = $id_vimeo;

        return $this;
    }

    public function getEsGratis(): ?bool
    {
        return $this->es_gratis;
    }

    public function setEsGratis(?bool $es_gratis): self
    {
        $this->es_gratis = $es_gratis;

        return $this;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(?bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }

    public function setTags(?string $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getIdPreviewVimeo(): ?string
    {
        return $this->id_preview_vimeo;
    }

    public function setIdPreviewVimeo(?string $id_preview_vimeo): self
    {
        $this->id_preview_vimeo = $id_preview_vimeo;

        return $this;
    }

    public function getResumen(): ?string
    {
        return $this->resumen;
    }

    public function setResumen(?string $resumen): self
    {
        $this->resumen = $resumen;

        return $this;
    }

    public function getDestacado(): ?bool
    {
        return $this->destacado;
    }

    public function setDestacado(?bool $destacado): self
    {
        $this->destacado = $destacado;

        return $this;
    }

    public function getAwsUrl(): ?string
    {
        return $this->aws_url;
    }

    public function setAwsUrl(?string $aws_url): self
    {
        $this->aws_url = $aws_url;

        return $this;
    }

    public function getAwsPreviewUrl(): ?string
    {
        return $this->aws_preview_url;
    }

    public function setAwsPreviewUrl(?string $aws_preview_url): self
    {
        $this->aws_preview_url = $aws_preview_url;

        return $this;
    }
}
