<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubCategoriaBlogRepository")
 */
class SubCategoriaBlog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $imagen;

    /**
     * @ORM\Column(type="integer")
     */
    private $orden;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoriaBlog", inversedBy="subCategoriaBlogs")
     */
    private $categoriaBlog;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getImagen(): ?Image
    {
        return $this->imagen;
    }

    public function setImagen(?Image $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }



    public function getCategoriaBlog(): ?CategoriaBlog
    {
        return $this->categoriaBlog;
    }

    public function setCategoriaBlog(?CategoriaBlog $categoriaBlog): self
    {
        $this->categoriaBlog = $categoriaBlog;

        return $this;
    }
}
