<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SuscripcionRepository")
 */
class Suscripcion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $id_paypal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $payer_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $plan_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PagoSuscripcion", mappedBy="suscripcion")
     */
    private $pagos;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activa;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $id_payu;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $reembolsada;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fecha_reembolso;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $valida_hasta;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duracion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Plan")
     */
    private $plan_suscripcion;

    /**
     * @ORM\OneToOne(targetEntity=Cancelacion::class, mappedBy="suscripcion", cascade={"persist", "remove"})
     */
    private $cancelacion;

    public function __construct()
    {
        $this->pagos = new ArrayCollection();
        $this->created_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->id . " - " . $this->id_paypal.$this->id_payu;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPaypal(): ?string
    {
        return $this->id_paypal;
    }

    public function setIdPaypal(?string $id_paypal): self
    {
        $this->id_paypal = $id_paypal;

        return $this;
    }

    public function getUsuario(): ?Usuario
    {
        return $this->usuario;
    }

    public function setUsuario(?Usuario $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPayerId(): ?string
    {
        return $this->payer_id;
    }

    public function setPayerId(string $payer_id): self
    {
        $this->payer_id = $payer_id;

        return $this;
    }

    public function getPlanId(): ?string
    {
        return $this->plan_id;
    }

    public function setPlanId(?string $plan_id): self
    {
        $this->plan_id = $plan_id;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|PagoSuscripcion[]
     */
    public function getPagos(): Collection
    {
        $criteria = Criteria::create()->orderBy(array("fecha" => Criteria::DESC));
        return $this->pagos->matching($criteria);
    }

    public function getUltimoPago(): PagoSuscripcion
    {
        return $this->getPagos()[0];
    }

    public function addPago(PagoSuscripcion $pago): self
    {
        if (!$this->pagos->contains($pago)) {
            $this->pagos[] = $pago;
            $pago->setSuscripcion($this);
        }

        return $this;
    }

    public function removePago(PagoSuscripcion $pago): self
    {
        if ($this->pagos->contains($pago)) {
            $this->pagos->removeElement($pago);
            // set the owning side to null (unless already changed)
            if ($pago->getSuscripcion() === $this) {
                $pago->setSuscripcion(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getActiva(): ?bool
    {
        return $this->activa;
    }

    public function setActiva(bool $activa): self
    {
        $this->activa = $activa;

        return $this;
    }

    public function getIdPayu(): ?string
    {
        return $this->id_payu;
    }

    public function setIdPayu(?string $id_payu): self
    {
        $this->id_payu = $id_payu;

        return $this;
    }

    public function getPagoSuscripciones()
    {
        return '<a href="/admin_jpchannel/?entity=PagoSuscripcion&action=list&suscripcion=' . $this->getId() . '" target="_blank">Ver</a>';
    }

    public function getReembolsada(): ?bool
    {
        return $this->reembolsada;
    }

    public function setReembolsada(?bool $reembolsada): self
    {
        $this->reembolsada = $reembolsada;

        return $this;
    }

    public function getFechaReembolso(): ?\DateTimeInterface
    {
        return $this->fecha_reembolso;
    }

    public function setFechaReembolso(?\DateTimeInterface $fecha_reembolso): self
    {
        $this->fecha_reembolso = $fecha_reembolso;

        return $this;
    }

    public function getValidaHasta(): ?\DateTimeInterface
    {
        return $this->valida_hasta;
    }

    public function setValidaHasta(?\DateTimeInterface $valida_hasta): self
    {
        $this->valida_hasta = $valida_hasta;

        return $this;
    }

    public function getDuracion(): ?int
    {
        return $this->duracion;
    }

    public function setDuracion(?int $duracion): self
    {
        $this->duracion = $duracion;

        return $this;
    }

    public function getPlanSuscripcion(): ?Plan
    {
        return $this->plan_suscripcion;
    }

    public function setPlanSuscripcion(?Plan $plan_suscripcion): self
    {
        $this->plan_suscripcion = $plan_suscripcion;

        return $this;
    }

    public function getCancelacion(): ?Cancelacion
    {
        return $this->cancelacion;
    }

    public function setCancelacion(Cancelacion $cancelacion): self
    {
        $this->cancelacion = $cancelacion;

        // set the owning side of the relation if necessary
        if ($cancelacion->getSuscripcion() !== $this) {
            $cancelacion->setSuscripcion($this);
        }

        return $this;
    }

    public function getCancelacionAdmin()
    {
        if ($this->cancelacion) {
            return '<a href="/admin_jpchannel/?entity=Cancelacion&action=show&id=' . $this->cancelacion->getId() . '" target="_blank">Ver</a>';
        } else {
            return "<div style=\"text-align: center;\"><span class=\"badge badge-success\">No</span> <div></div></div>";
        }
    }
}
