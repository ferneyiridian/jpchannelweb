/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
//require('@fancyapps/fancybox/dist/jquery.fancybox.min.css');
require('font-awesome/css/font-awesome.min.css');
//require('slick-carousel/slick/slick.css');
//require('slick-carousel/slick/slick-theme.scss');
//require('gijgo/css/gijgo.css');
require('../css/app.scss');
require('swiper/css/swiper.min.css');
//require('remodal/dist/remodal.css');
//require("selectric/public/selectric.css");
//require('remodal/dist/remodal-default-theme.css');
require('bootstrap/dist/css/bootstrap.min.css');

// require('swiper/swiper.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');

const $ = require('jquery');
require('bootstrap/dist/js/bootstrap.min.js');
window.jQuery = $;
//const slick = require("slick-carousel");
//var Swiper = require('swiper');
//require("@fancyapps/fancybox");
//require("gijgo/js/gijgo.js");
//require("selectric/public/jquery.selectric");
//require('remodal/dist/remodal.min');

const Swiper = require('swiper/js/swiper');

//require("sel");
//var validator = require('validator-engine');
import isMobile from 'ismobilejs';

// const userAgent = Request.headers['user-agent'];

window.mobilecheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

$(function () {

    $('.s-g').each(function () {
        var es = $(this).find('.container-s-g');
        var awn = $(this).find('.swiper-button-next');
        var awp = $(this).find('.swiper-button-prev');
        var hijos = $(this).find('.swiper-slide ').length;
        console.log('hjos', hijos);
        var loopS;
        if(hijos > 4){
            loopS = true;
        }else {
            loopS = false

        }
        var bannerSg = new Swiper(es, {
            loop: loopS,
            slidesPerView: 4.5,
            spaceBetween: 5,
            navigation: {
                nextEl: awn,
                prevEl: awp
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                    slidesPerView: 3.5,
                    spaceBetween: 5
                },
                // when window width is >= 480px
                714: {
                    slidesPerView: 4.5,
                    spaceBetween: 5
                },
                // when window width is >= 1024px
                1024: {
                    slidesPerView: 5.5,
                    spaceBetween: 5
                }
            }
        });
    });


    $('.list-art').click(function () {
        console.log('click');
        $(this).parent().next('ul').slideToggle("slow", function () {
            // Animation complete.
        });
    });

    popUp();

    $('.payu').click(function () {
        setTimeout(function(){
            $('html, body').animate({ scrollTop: $('#datos').offset().top }, 'slow');
        }, 1000);
    });

    /*if(aMobile() || aIpad()){


    }else{

    }*/

    $( ".user-id" ).click(function() {
        $(this).parent().find('.op').slideToggle( "slow", function() {
            // Animation complete.
        });
    });

    $('.contFaq').click(function () {
        $(this).toggleClass('abierto')
    });
});


var scrollPosition = 0;

$(window).scroll(function(){
    scrollPosition = $(window).scrollTop();
    //console.log('scrollTp', scrollPosition);
});

function popUp() {

    $('.call-popUp').click(function (event) {
        event.preventDefault();


        $('body').css('overflow', 'hidden');

        var dataId = $(this).data('video');
        var dataUrl = $(this).data('aws');
        var dataDomain = $(this).data('domain');
        var wpaper = $(this).data('wallpaper');
        var tituloVideo = $(this).data('titulo');

        var pos = $(this).offset();
        var sizeIw = $(this).outerWidth();
        var sizeIh = $(this).outerHeight();

        console.log('');
        if(tituloVideo) {
            gtag('config', 'UA-162179837-1', {'page_path': '/videos/'+tituloVideo});
        }

        $('body').append('<div class="popUp overlay"></div>' +
            '<div class="popUp container-p text-center" style="top: '+pos.top+'px; left: '+pos.left+'px; width: '+sizeIw+'px; height: '+sizeIh+'px;">' +
                '<div class="wallpaper" style="background-image: url('+wpaper+')"></div>'+
                '<a class="close-popUp close-popUpL">' +
                    '<i class="far fa-arrow-alt-circle-left"></i>' +
                '</a>' +
            '</div>');

        if(dataUrl != "" && dataUrl != undefined){
            console.log("awss ",dataDomain+dataUrl);
            $('.popUp.container-p').append('<video id="reproductor" allow="autoplay; fullscreen" allowfullscreen width="640" height="270"  controls  ></video>');



            var video = document.getElementById('reproductor');
            var videoSrc = dataDomain+dataUrl;
            var hls = new Hls();
            if (Hls.isSupported()) {
                console.log("comaws 1");
                hls.loadSource(videoSrc);
                hls.attachMedia(video);
                hls.on(Hls.Events.MANIFEST_PARSED, function() {
/*                    gtag('send', 'Play_Video', {
                        'event_category': 'Videos',
                        'event_label': 'play',
                        'value': dataId
                    });*/
                    video.play();
                    console.log("play video: ", dataId);
                });
            }else if (video.canPlayType('application/vnd.apple.mpegurl')) {
                console.log("comaws 2");
                video.src = videoSrc;
                video.addEventListener('loadedmetadata', function() {
                    video.play();
                    console.log("play video");
                });
            }



        }else if(dataId != ' ' || dataId == null){
            console.log('video id', dataId);
            $('.popUp.container-p').append('<iframe src="https://player.vimeo.com/video/'+dataId+'?color=09a6a7&title=0&byline=0&portrait&autoplay=1" width="640" height="270" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
        }else {
            alert('faltan atributos');
        }
        setTimeout(function(){
            $( ".container-p" ).animate({
                top: scrollPosition + 'px',
            }, 200);
            $('.popUp').addClass('view');

            $('.close-popUpL').click(function () {

                gtag('config', 'UA-162179837-1', {'page_path': location.pathname});
                // ga('set', 'page', location.href);
                // ga('send', 'pageview');

                $('.popUp').removeClass('view');
                setTimeout(function(){
                    $('.popUp').remove();
                    $('body').css('overflow', 'auto');
                }, 600);
            });
            setTimeout(function(){
                $('.wallpaper').addClass('hidden');
            }, 1000);

        }, 100);
    });
}


$(window).on("load", function() {

});


function aMobile() {
    return(/Android|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );
}

function aIpad(){
    return navigator.userAgent.match(/iPad/i);
}

